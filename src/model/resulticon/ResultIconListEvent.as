package model.resulticon
{
	import flash.events.Event;

	public class ResultIconListEvent extends Event
	{
		public static var ADD: String = "ResultIconListEvent.ADD";

		public static var REMOVE: String = "ResultIconListEvent.REMOVE";

		public static var CONTENTSCHANGE: String = "ResultIconListEvent.CONTENTSCHANGE";

		public static var CURRENTCHANGED: String = "ResultIconListEvent.CURRENTCHANGED";
		
		public static var SECONDCHANGED: String = "ResultIconListEvent.SECONDCHANGED";
		
		[Bindable]
		private var _resulticonlist: ResultIconList;

		[Bindable]
		private var _resulticon: ResultIcon;

		public function ResultIconListEvent(aType: String, aResultIconList: ResultIconList, aResultIcon: ResultIcon) {
			super(aType);

			resultIconList = aResultIconList;

			resultIcon = aResultIcon;
		}

		[Bindable]
		public function get resultIconList(): ResultIconList {
			return _resulticonlist;
		}

		public function set resultIconList(aResultIconList: ResultIconList): void {
			_resulticonlist = aResultIconList;
		}

		[Bindable]
		public function get resultIcon(): ResultIcon {
			return _resulticon;
		}

		public function set resultIcon(aResultIcon: ResultIcon): void {
			_resulticon = aResultIcon;
		}
	}
}
