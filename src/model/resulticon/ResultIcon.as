package model.resulticon
{
	import flash.events.Event;
	import flash.events.EventDispatcher;
	
	import model.ModelUtils;
	import model.resultconnection.ResultConnectionList;
	import model.resultconnection.ResultConnectionListEvent;
	
	import mx.collections.ArrayCollection;
	
	import session.Session;
	
	public class ResultIcon extends EventDispatcher
	{
		[Bindable]
		private var _id: String;

		[Bindable]
		private var _icon: String;
		
		[Bindable]
		private var _notes: String;
		
		[Bindable]
		private var _x: Number;
		
		[Bindable]
		private var _y: Number;
		
		[Bindable]
		private var _width: Number;
		
		[Bindable]
		private var _height: Number;
		
		[Bindable]
		private var _resultconnectionlist: ResultConnectionList;
		
		public function ResultIcon(aID: String, aIcon: String, aNotes: String) {
			super();
			
			id = aID;

			icon = aIcon;
			
			notes = aNotes;
		}
	
		public static function createResultIcon(aObject: Object): ResultIcon {
			var id: String = aObject.url;
			
			var icon: String = aObject.icon;
			
			var notes: String = aObject.comment;
			
			var resulticon: ResultIcon = new ResultIcon(id, icon, notes);
			
			// indicators -> ResultConnections
			var arr: Array = (aObject.indicators as Array);
			var ac: ArrayCollection = new ArrayCollection(arr);
			resulticon.resultConnectionList = ResultConnectionList.createResultConnectionList(ac);
			
			// add userIndicators to ResultConnections
			arr = (aObject.userIndicators as Array);
			ac  = new ArrayCollection(arr);
			var resultconnectionlist:ResultConnectionList = ResultConnectionList.createResultConnectionList(ac);
			
			for (var i:int=0; i < resultconnectionlist.numResultConnections(); i++)
				resulticon.resultConnectionList.addResultConnection(resultconnectionlist.getResultConnection(i));

			if (aObject.hasOwnProperty("xPos")
			&&  aObject.hasOwnProperty("yPos")
			&&  aObject.hasOwnProperty("width")
			&&	aObject.hasOwnProperty("height") 
			) {
				resulticon.x = parseFloat(String(aObject.xPos));
				resulticon.y = parseFloat(String(aObject.yPos));
				resulticon.width = parseFloat(String(aObject.width));
				resulticon.height= parseFloat(String(aObject.height));
			}
				
			return resulticon;
		}
		
		[Bindable]
		public function get id(): String {
			return _id;
		}

		public function set id(aID: String): void {
			_id = aID;
		}

		[Bindable]
		public function get icon(): String {
			return _icon;
		}
		
		public function set icon(aIcon: String): void {
			_icon = aIcon;
		}
	
		[Bindable]
		public function get x(): Number {
			return _x;
		}
		
		public function set x(aX: Number): void {
			_x = aX;
		}
		
		[Bindable]
		public function get y(): Number {
			return _y;
		}
		
		public function set y(aY: Number): void {
			_y = aY;
		}
		
		[Bindable]
		public function get width(): Number {
			return _width;
		}
		
		public function set width(aWidth: Number): void {
			_width = aWidth;
		}
		
		[Bindable]
		public function get height(): Number {
			return _height;
		}
		
		public function set height(aHeight: Number): void {
			_height = aHeight;
		}
		
		[Bindable]
		public function get notes(): String {
			return _notes;
		}
		
		public function set notes(aNotes: String): void {
			_notes = aNotes;
		}

		[Bindable]
		public function get resultConnectionList(): ResultConnectionList {
			return _resultconnectionlist;
		}
		
		public function set resultConnectionList(aResultConnectionList: ResultConnectionList): void {
			//trace("set resultConnectionList", id, icon, aResultConnectionList.numResultConnections());
			if (_resultconnectionlist != null) {
				_resultconnectionlist.removeEventListener(ResultConnectionListEvent.ADD,    handleResultConnectionListEventAdd);
				_resultconnectionlist.removeEventListener(ResultConnectionListEvent.REMOVE, handleResultConnectionListEventRemove);
			}

			_resultconnectionlist = aResultConnectionList;
			
			if (_resultconnectionlist != null) {
				_resultconnectionlist.addEventListener(ResultConnectionListEvent.ADD,    handleResultConnectionListEventAdd);
				_resultconnectionlist.addEventListener(ResultConnectionListEvent.REMOVE, handleResultConnectionListEventRemove);
			}
		}
		
		private function contentsChanged(): void {
			var event: ResultIconEvent = new ResultIconEvent(ResultIconEvent.CONTENTSCHANGE, this);
			
			dispatchEvent(event);
		}
		
		private function handleResultConnectionListEventAdd(event: ResultConnectionListEvent): void {
			var newevent: ResultIconEvent = new ResultIconEvent(ResultIconEvent.INDICATOR_ADD, this);
			
			dispatchEvent(newevent);
		}
		
		private function handleResultConnectionListEventRemove(event: ResultConnectionListEvent): void {
			var newevent: ResultIconEvent = new ResultIconEvent(ResultIconEvent.INDICATOR_REMOVE, this);
			
			dispatchEvent(newevent);
		}
	}
}