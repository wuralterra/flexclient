package model.resulticon
{
	import mx.collections.ArrayCollection;
	
	import session.Session;
	
	public class ResultIconList extends ArrayCollection
	{
		[Bindable]
		private var _resulticon: ResultIcon;
		
		[Bindable]
		private var _resulticon2: ResultIcon;
		
		public function ResultIconList() {
			super();
		}

		public static function createResultIconList(aAC: ArrayCollection): ResultIconList {
			var resulticonlist: ResultIconList = new ResultIconList();
			
			for (var i: int = 0; i < aAC.length; i ++) {
				resulticonlist.addResultIcon(ResultIcon.createResultIcon(aAC[i]));
			}
			
			return resulticonlist;
		}
		
		public function addResultIcon(aResultIcon: ResultIcon): void {
			addItem(aResultIcon);

			aResultIcon.addEventListener(ResultIconEvent.CONTENTSCHANGE, handleResultIconEventContentsChange);

			var event: ResultIconListEvent = new ResultIconListEvent(ResultIconListEvent.ADD, this, aResultIcon);

			dispatchEvent(event);
		}

		public function removeResultIcon(aResultIcon: ResultIcon): void {
			var index: int = getItemIndex(aResultIcon);

			if (index != -1) {
				aResultIcon.removeEventListener(ResultIconEvent.CONTENTSCHANGE, handleResultIconEventContentsChange);

				removeItemAt(index);

				var event: ResultIconListEvent = new ResultIconListEvent(ResultIconListEvent.REMOVE, this, aResultIcon);

				dispatchEvent(event);
			}
		}

		public function handleResultIconEventContentsChange(aEvent: ResultIconEvent): void {
			var event: ResultIconListEvent = new ResultIconListEvent(ResultIconListEvent.CONTENTSCHANGE, this, aEvent.resultIcon);

			dispatchEvent(event);
		}

		public function getResultIcon(i: int): ResultIcon {
			return getItemAt(i) as ResultIcon;
		}

		public function numResultIcons(): int {
			return length;
		}
		
		public function findResultIconByID(aID: String): ResultIcon {
			var result: ResultIcon = null;
			
			for (var i: int = 0; i < numResultIcons(); i ++) {
				var resulticon: ResultIcon = getResultIcon(i);
				
				if (resulticon.id == aID) {
					result = resulticon;
					
					break;
				}
			}
			
			return result;
		}
		
		[Bindable]
		public function get resultIcon(): ResultIcon {
			return _resulticon;
		}
		
		public function set resultIcon(aResultIcon: ResultIcon): void {
			_resulticon = aResultIcon;
			
			var event: ResultIconListEvent = new ResultIconListEvent(ResultIconListEvent.CURRENTCHANGED, this, aResultIcon);
			
			dispatchEvent(event);
		}

		[Bindable]
		public function get resultIcon2(): ResultIcon {
			return _resulticon2;
		}
		
		public function set resultIcon2(aResultIcon: ResultIcon): void {
			_resulticon2 = aResultIcon;
			
			var event: ResultIconListEvent = new ResultIconListEvent(ResultIconListEvent.CURRENTCHANGED, this, aResultIcon);
			
			dispatchEvent(event);
		}
	}
}
