package model.resulticon
{
	import flash.events.Event;

	public class ResultIconEvent extends Event
	{
		public static var CONTENTSCHANGE: String = "ResultIconEvent.CONTENTSCHANGE";
		public static var INDICATOR_ADD: String = "ResultIconEvent.INDICATOR_ADD";
		public static var INDICATOR_REMOVE: String = "ResultIconEvent.INDICATOR_REMOVE";

		[Bindable]
		private var _resulticon: ResultIcon;

		public function ResultIconEvent(aType: String, aResultIcon: ResultIcon) {
			super(aType);

			resultIcon = aResultIcon;
		}

		[Bindable]
		public function get resultIcon(): ResultIcon {
			return _resulticon;
		}

		public function set resultIcon(aResultIcon: ResultIcon): void {
			_resulticon = aResultIcon;
		}
	}
}
