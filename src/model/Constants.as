package model
{
	public class Constants
	{
		// profile fieldnames
		public static const PROFILELIST: Array = ["name","family","region","gender","age","urbanity","education","finance","coastal","mountainous"];

		[Bindable]
		public static var USERINDICATOR: String = "Your issue";
		
		[Bindable]
		public static var ADDINDICATOR: String = "Your issue    Click to type";
		
		[Bindable]
		public static var ADDINDICATORTIP: String = "Type issue and press Enter to create";
		
		[Bindable]
		public static var INDICATOR: String = "issue";
		
		[Bindable]
		public static var INDICATORHEADER: String = "Issues";

		[Bindable]
		public static var CONNECTOR: String = "connector";

		[Bindable]
		public static var CONNECTION: String = "link";

		[Bindable]
		public static var CONNECTIONTEXT: String = "type a comment";
		
		[Bindable]
		public static var ICON: String = "picture";
		
		[Bindable]
		public static var ICONHEADER: String = "Pictures";
		
		[Bindable]
		public static var ICONTEXT: String = "add text";

		[Bindable]
		public static var ICONTAG: String = "text";

		[Bindable]
		public static var TAG: String = "free text";
		
		[Bindable]
		public static var TAGTEXT: String = "add text";

		[Bindable]
		public static var THEMEHEADER: String = "Themes";
		
		[Bindable]
		public static var THEME: String = "theme";

		public static const DEFAULT_COLOR:uint = 0x5a62a6; 
		public static const CONNECTOR_SIZE: int = 14;
		public static const LINE_THICKNESS: int = 5;
		public static const RADIUS: int = 10;
		public static const DOT_SIZE: int = 8;
		public static const LINK_THICKNESS: int = 2;
		public static const INDICATOR_WIDTH: int = 140;
	}
}