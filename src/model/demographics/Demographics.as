package model.demographics
{
	import flash.events.EventDispatcher;
	
	import model.ModelUtils;
	import model.demographicsitem.DemographicsItemList;
	
	import mx.collections.ArrayCollection;
	
	public class Demographics extends EventDispatcher
	{
		[Bindable]
		private var _id: String;
		
		private var _title: String;
		
		[Bindable]
		private var _description: String;

		[Bindable]
		private var _demographicsitemlist: DemographicsItemList;
		
		public function Demographics(aID: String, aTitle: String, aDescription: String) {
			super();
			
			id = aID;

			title = aTitle;
			
			description = aDescription;
		}
		
		public static function createDemographics(aObject: Object): Demographics {
			var id: String = aObject.url;
			
			var title: String = aObject.title;
			
			var description: String = aObject.description;
			
			var demographics: Demographics = new Demographics(id, title, description);
			
			var arr: Array = (aObject.items as Array);
			var ac: ArrayCollection = new ArrayCollection(arr);
			
			demographics.demographicsItemList = DemographicsItemList.createDemographicsItemList(ac);
			
			return demographics;
		}
		
		public function createDemographicsObject(): Object {
			var demographicsobject: Object = new Object();
			
			demographicsobject["demographics"] = ModelUtils.getNumberFromId(id);
			
			return demographicsobject;
		}
		
		[Bindable]
		public function get id(): String {
			return _id;
		}

		public function set id(aID: String): void {
			_id = aID;

			contentsChanged();
		}

		[Bindable]
		public function get description(): String {
			return _description;
		}
		
		public function set description(aDescription: String): void {
			_description = aDescription;
			
			contentsChanged();
		}
		
		[Bindable]
		public function get title(): String {
			return _title;
		}
		
		public function set title(aTitle: String): void {
			_title = aTitle;
			
			contentsChanged();
		}
		
		[Bindable]
		public function get demographicsItemList(): DemographicsItemList {
			return _demographicsitemlist;
		}

		public function set demographicsItemList(aDemographicsItemList: DemographicsItemList): void {
			_demographicsitemlist = aDemographicsItemList;

			contentsChanged();
		}

		private function contentsChanged(): void {
			var event: DemographicsEvent = new DemographicsEvent(DemographicsEvent.CONTENTSCHANGE, this);

			dispatchEvent(event);
		}
	}
}