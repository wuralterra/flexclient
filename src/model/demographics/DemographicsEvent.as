package model.demographics
{
	import flash.events.Event;

	public class DemographicsEvent extends Event
	{
		public static var CONTENTSCHANGE: String = "DemographicsEvent.CONTENTSCHANGE";

		[Bindable]
		private var _demographics: Demographics;

		public function DemographicsEvent(aType: String, aDemographics: Demographics) {
			super(aType);

			demographics = aDemographics;
		}

		[Bindable]
		public function get demographics(): Demographics {
			return _demographics;
		}

		public function set demographics(aDemographics: Demographics): void {
			_demographics = aDemographics;
		}
	}
}
