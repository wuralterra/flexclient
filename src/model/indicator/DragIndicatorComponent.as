package model.indicator
{
	import flash.display.Sprite;
	import flash.events.MouseEvent;
	
	import mx.binding.utils.BindingUtils;
	import mx.core.Application;
	import mx.core.DragSource;
	import mx.core.UIComponent;
	import mx.events.DragEvent;
	import mx.events.MoveEvent;
	import mx.events.ResizeEvent;
	import mx.managers.DragManager;
	
	import spark.components.Button;
	import spark.components.Image;
	import spark.components.Label;
	
	import components.Connector;
	import session.Session;
	import model.Constants;
	import model.icon.Icon;
	import model.indicator.Indicator;
	
	public class DragIndicatorComponent extends UIComponent
	{
		/* components */
		private var _button: Button;

		private var _border: Sprite;
		
		private var _circle: Sprite;
		
		/* Start drag for resize */
		private var _startDragX: int;
		
		private var _startDragY: int;
		
		private var dX:Number, dY:Number;             

		[Bindable]
		public var dragged: Boolean;
		
		[Bindable]
		private var _indicator: Indicator;
		
		[Bindable]
		private var session:Session = Session.instance();
		
		public function DragIndicatorComponent() {
			super();
			
			addEventListener(ResizeEvent.RESIZE, adjustChildren);
		}
		
		override protected function createChildren(): void {
			super.createChildren();
			
			_button = new Button();
			_button.width  = this.width;
			_button.height = this.height;
			if (indicator.id == "0")
				_button.label = Constants.USERINDICATOR;
			else
				_button.label = indicator.title;
			_button.mouseEnabled = true;
			if (dragged)
				_button.styleName = "draggeddragindicator";
			else if (indicator.id == "0")
				_button.styleName = "adddragindicator";
			else
				_button.styleName = "dragindicator";
			//_button.toolTip= indicator.description;
			_button.addEventListener(ResizeEvent.RESIZE, adjustChildren);
			
			addChild(_button);

			//addEventListener(DragEvent.DRAG_ENTER, doDragEnter);
			//addEventListener(DragEvent.DRAG_DROP, doDragDrop);
		}
				
		/* Event listeners */
		// resize of component
		private function adjustChildren(event: ResizeEvent): void {
			//trace("drgindcmp adjustChildren", indicator.title, (event.target == this)?"indcmp":"button", event.oldHeight, height, unscaledHeight, _button.getLayoutBoundsHeight());
			
			if (_button) {
				height= _button.getLayoutBoundsHeight();	// height needs to be adjusted to accomodate indicator titles
				
				_button.width = unscaledWidth;
				_button.height= unscaledHeight;
			}
		}
		
/*		protected function rollOverHandler(event:MouseEvent):void {
			_button.styleName = "rolloverdragindicator";
		}
		
		protected function rollOutHandler(event:MouseEvent):void {
			_button.styleName = "dragindicator";
		}
*/		
		private function dragStart(event: MouseEvent): void {
			if (indicator != null) {
				//trace("dragStart");
				// Get the drag initiator component from the event object.
				var dragInitiator: Connector = event.currentTarget as Connector;
				
				// Create a DragSource object.
				var ds: DragSource = new DragSource();
				
				// Add the data to the object.
				ds.addData(indicator, "connect");
				
				var connectorProxy: Connector= new Connector();
				
				// Call the DragManager doDrag() method to start the drag. 
				DragManager.doDrag(dragInitiator, ds, event, connectorProxy);			
			}
		}
		
		private function doDragEnter(event: DragEvent): void {
			// Accept the drag only if the user is dragging Icon data 
			if (event.dragSource.hasFormat("connect")) {
				var data: Object = event.dragSource.dataForFormat("connect");
			
				if (data is Icon) {
					//trace("doDragEnter");
					DragManager.acceptDragDrop(DragIndicatorComponent(event.currentTarget));
				}
			}
		}
		
		// Called if the target accepts the dragged object 
		// and the user releases the mouse button. 
		private function doDragDrop(event: DragEvent): void {
			//trace("doDragDrop");
			if (event.dragSource.hasFormat("connect")) {
				var data: Object = event.dragSource.dataForFormat("connect");
			
				if (data is Icon) {
					//trace("doDragDrop Icon");
					// data as Icon can connect to this (Indicator)
					(data as Icon).connect(indicator);
				}
			}
		}

		/* Getters and setters */
		[Bindable]
		public function get indicator():Indicator {
			return _indicator;
		}
		
		public function set indicator(aIndicator: Indicator): void {
			_indicator = aIndicator;
			
			if (_indicator != null)
				this.toolTip= indicator.description;
		}
	}
}