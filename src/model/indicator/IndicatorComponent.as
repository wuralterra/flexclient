package model.indicator
{
	import flash.display.Sprite;
	import flash.events.MouseEvent;
	
	import mx.binding.utils.BindingUtils;
	import mx.core.Application;
	import mx.core.DragSource;
	import mx.core.UIComponent;
	import mx.events.DragEvent;
	import mx.events.MoveEvent;
	import mx.events.ResizeEvent;
	import mx.managers.DragManager;
	
	import spark.components.Button;
	import spark.components.Image;
	import spark.components.Label;
	
	import components.Connector;
	import session.Session;
	import model.Constants;
	import model.icon.Icon;
	import model.indicator.Indicator;
	
	public class IndicatorComponent extends UIComponent
	{
		[Embed(source="assets/images/close.gif")]
		[Bindable]
		private var _closeimgcls: Class;
		
		/* Close function */
		private var _onclose: Function = null;
		
		/* components */
		private var _button: Button;

		private var _connector: Connector;
		
		private var _border: Sprite;
		
		private var _circle: Sprite;
		
		private var _closeButton: Button = null;

		private var _connectHandle: Button = null;
		
		/* Start drag for resize */
		private var _startDragX: int;
		
		private var _startDragY: int;
		
		private var dX:Number, dY:Number;             

		[Bindable]
		private var _dragstarted: Boolean;
		
		[Bindable]
		private var _indicator: Indicator;
		
		[Bindable]
		private var session:Session = Session.instance();
		
		public function IndicatorComponent() {
			super();
			
			addEventListener(ResizeEvent.RESIZE, adjustChildren);
		}
		
		override protected function createChildren(): void {
			super.createChildren();
			
			_button = new Button();
			_button.width  = this.width;
			_button.height = this.height;
			_button.label  = indicator.title;
			_button.mouseEnabled = false;
			_button.styleName = "indicator";
			//_button.toolTip= indicator.description;
			_button.addEventListener(ResizeEvent.RESIZE, adjustChildren);
			
			addChild(_button);
			
			_connector = new Connector();
			addChild(_connector);
			_connector.x = 1;
			_connector.y = height/2;
			_connector.buttonMode = true;
			_connector.addEventListener(MouseEvent.MOUSE_DOWN, dragStart);

			// user indicator
			if (indicator.id == "0")
				createCloseButton();

			addEventListener(DragEvent.DRAG_ENTER, doDragEnter);
			addEventListener(DragEvent.DRAG_DROP, doDragDrop);
		}
				
		// resize of component
		private function adjustChildren(event: ResizeEvent): void {
			//trace("indcmp adjustChildren", indicator.title, (event.target == this)?"indcmp":"button", event.oldHeight, height, unscaledHeight, _button.getLayoutBoundsHeight());
			
			if (_button) {
				height= _button.getLayoutBoundsHeight();	// height needs to be adjusted to accomodate indicator titles
				
				_button.width = unscaledWidth;
				_button.height= unscaledHeight;
			}
			
			// connector
			if (_connector) {
				_connector.x = 1;
				_connector.y = unscaledHeight/2;
			}
			
			// buttons
			if (_closeButton) {
				_closeButton.x = unscaledWidth    - _closeButton.width;
				_closeButton.y = unscaledHeight/2 - _closeButton.height/2;
			}
		}
		
		public function get onClose(): Function {
			return _onclose;
		}
		
		public function set onClose(aOnClose: Function): void {
			_onclose = aOnClose;
		}

		private function createCloseButton(): void {
			if ( _closeButton )
				return;
			
			_closeButton = new Button();
			_closeButton.width = 15;
			_closeButton.height = 15;
			
			_closeButton.buttonMode = true;
			_closeButton.useHandCursor = true;
			_closeButton.setStyle("icon", _closeimgcls);
			_closeButton.addEventListener(MouseEvent.CLICK, closeButtonHandler);
			BindingUtils.bindProperty(_closeButton, "visible", session, "notPrinting");
			
			_closeButton.toolTip = "Close " + Constants.INDICATOR;
			
			addChild( _closeButton );

			_closeButton.x = unscaledWidth    - _closeButton.width;
			_closeButton.y = unscaledHeight/2 - _closeButton.height/2;
		}
		
		/* Event listeners */
		protected function closeButtonHandler(event: MouseEvent): void {
			this.parent.removeChild(this);
			
			if (onClose != null) {
				onClose(indicator);
			}
		}

		private function dragStart(event: MouseEvent): void {
			if (indicator != null) {
				//trace("dragStart");
				// Get the drag initiator component from the event object.
				var dragInitiator: Connector = event.currentTarget as Connector;
				
				// Create a DragSource object.
				var ds: DragSource = new DragSource();
				
				// Add the data to the object.
				ds.addData(indicator, "connect");
				
				var connectorProxy: Connector= new Connector();
				
				// Call the DragManager doDrag() method to start the drag. 
				DragManager.doDrag(dragInitiator, ds, event, connectorProxy);			
			}
		}
		
		private function doDragEnter(event: DragEvent): void {
			// Accept the drag only if the user is dragging Icon data 
			if (event.dragSource.hasFormat("connect")) {
				var data: Object = event.dragSource.dataForFormat("connect");
			
				if (data is Icon) {
					//trace("doDragEnter");
					DragManager.acceptDragDrop(IndicatorComponent(event.currentTarget));
				}
			}
		}
		
		// Called if the target accepts the dragged object 
		// and the user releases the mouse button. 
		private function doDragDrop(event: DragEvent): void {
			//trace("doDragDrop");
			if (event.dragSource.hasFormat("connect")) {
				var data: Object = event.dragSource.dataForFormat("connect");
			
				if (data is Icon) {
					//trace("doDragDrop Icon");
					// data as Icon can connect to this (Indicator)
					(data as Icon).connect(indicator);
				}
			}
		}

		/* Getters and setters */
		[Bindable]
		public function get indicator():Indicator {
			return _indicator;
		}
		
		public function set indicator(aIndicator: Indicator): void {
			_indicator = aIndicator;
			
			if (_indicator != null)
				this.toolTip= indicator.description;
		}
	}
}