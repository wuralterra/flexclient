package model.indicator
{
	import flash.events.EventDispatcher;
	
	import model.ModelUtils;
	
	import mx.collections.ArrayCollection;
	
	public class Indicator extends EventDispatcher
	{
		[Bindable]
		private var _id: String;

		[Bindable]
		private var _title: String;
		
		[Bindable]
		private var _description: String;
		
		//Result
		[Bindable]
		private var _x: Number;
		
		[Bindable]
		private var _y: Number;
		
		
		public function Indicator(aID: String, aTitle: String, aDescription: String) {
			super();
			
			id = aID;

			title = aTitle;
			
			description = aDescription;

			x = 0;
			
			y = 0;
		}
		
		public static function createIndicator(aObject: Object): Indicator {
			var id: String = aObject.url;
			
			var title: String = aObject.title;
			
			var description: String = aObject.description;
			
			var indicator: Indicator = new Indicator(id, title, description);
			
			if (aObject.hasOwnProperty("xPos")
				&&  aObject.hasOwnProperty("yPos")
			) {
				indicator.x = parseFloat(String(aObject.xPos));
				indicator.y = parseFloat(String(aObject.yPos));
			}

			return indicator;
		}
		
		public function createIndicatorObject(aId: String, aComment: String): Object {
			var indicatorobject: Object = new Object();
			var indicatorid: String = ModelUtils.getNumberFromId(id);
			
			if (indicatorid == "0") {
				indicatorobject["indicator"]= aId;
				indicatorobject["type"]     = "user";
			}
			else
				indicatorobject["indicator"]= indicatorid;
			
			if (aComment)
				indicatorobject["comment"]  = aComment;

			return indicatorobject;
		}
		
		public function createResultIndicatorObject(aId: String, aComment: String, asUser:Boolean): Object {
			var indicatorobject: Object = new Object();
			var indicatorid: String = ModelUtils.getNumberFromId(id);
			
			// @ToDo wait for change Dave, temp store as userindicator?
			if (indicatorid == "0" || asUser) {
				indicatorobject["type"]     = "user";
				indicatorobject["indicator"]= aId;
			}
			else {
				//indicatorobject["type"]     = "user";
				//indicatorobject["type"]     = "stored";
				indicatorobject["indicator"]= indicatorid;
				indicatorobject["id"]       = aId;
			}
			
			if (aComment)
				indicatorobject["comment"] = aComment;
			
			return indicatorobject;
		}
		
		public function createUserIndicatorObject(aId: String): Object {
			var userindicatorobject: Object = new Object();
			
			userindicatorobject["id"]         = aId;
			userindicatorobject["title"]      = title;
			userindicatorobject["description"]= description;

			return userindicatorobject;
		}
		
		[Bindable]
		public function get id(): String {
			return _id;
		}

		public function set id(aID: String): void {
			_id = aID;

			contentsChanged();
		}

		[Bindable]
		public function get description(): String {
			return _description;
		}

		public function set description(aDescription: String): void {
			_description = aDescription;

			contentsChanged();
		}
		
		[Bindable]
		public function get title(): String {
			return _title;
		}
		
		public function set title(aTitle: String): void {
			_title = aTitle;
			
			contentsChanged();
		}
		
		[Bindable]
		public function get x(): Number {
			return _x;
		}
		
		public function set x(aX: Number): void {
			_x = aX;
			
			//contentsChanged();
		}
		
		[Bindable]
		public function get y(): Number {
			return _y;
		}
		
		public function set y(aY: Number): void {
			_y = aY;
			
			//contentsChanged();
		}
		
		private function contentsChanged(): void {
			var event: IndicatorEvent = new IndicatorEvent(IndicatorEvent.CONTENTSCHANGE, this);

			dispatchEvent(event);
		}
	}
}