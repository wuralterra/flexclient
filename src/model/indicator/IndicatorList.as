package model.indicator
{
	import model.ModelUtils;
	
	import mx.collections.ArrayCollection;
	
	public class IndicatorList extends ArrayCollection
	{
		[Bindable]
		private var _indicator: Indicator;
		
		public function IndicatorList() {
			super();
		}

		public static function createIndicatorList(aAC: ArrayCollection): IndicatorList {
			var indicatorlist: IndicatorList = new IndicatorList();
			
			for (var i: int = 0; i < aAC.length; i ++) {
				indicatorlist.addIndicator(Indicator.createIndicator(aAC[i]));
			}
			
			return indicatorlist;
		}
		
		public function addIndicator(aIndicator: Indicator): void {
			addItem(aIndicator);

			aIndicator.addEventListener(IndicatorEvent.CONTENTSCHANGE, handleIndicatorEventContentsChange);

			var event: IndicatorListEvent = new IndicatorListEvent(IndicatorListEvent.ADD, this, aIndicator);

			dispatchEvent(event);
		}

		public function removeIndicator(aIndicator: Indicator): void {
			var index: int = getItemIndex(aIndicator);

			if (index != -1) {
				aIndicator.removeEventListener(IndicatorEvent.CONTENTSCHANGE, handleIndicatorEventContentsChange);

				removeItemAt(index);

				var event: IndicatorListEvent = new IndicatorListEvent(IndicatorListEvent.REMOVE, this, aIndicator);

				dispatchEvent(event);
			}
		}

		public function handleIndicatorEventContentsChange(aEvent: IndicatorEvent): void {
			var event: IndicatorListEvent = new IndicatorListEvent(IndicatorListEvent.CONTENTSCHANGE, this, aEvent.indicator);

			dispatchEvent(event);
		}

		public function getIndicator(i: int): Indicator {
			return getItemAt(i) as Indicator;
		}

		public function numIndicators(): int {
			return length;
		}
		
		public function findIndicatorByID(aID: String): Indicator {
			var result: Indicator = null;
			
			for (var i: int = 0; i < numIndicators(); i ++) {
				var indicator: Indicator = getIndicator(i);
				
				if (indicator.id == aID || ModelUtils.getNumberFromId(indicator.id) == aID) {
					result = indicator;
					
					break;
				}
			}
			
			return result;
		}
		
		[Bindable]
		public function get indicator(): Indicator {
			return _indicator;
		}
		
		public function set indicator(aIndicator: Indicator): void {
			_indicator = aIndicator;
			
			var event: IndicatorListEvent = new IndicatorListEvent(IndicatorListEvent.CURRENTCHANGED, this, aIndicator);
			
			dispatchEvent(event);
		}
		
	}
}
