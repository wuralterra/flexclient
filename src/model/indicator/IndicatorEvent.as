package model.indicator
{
	import flash.events.Event;

	public class IndicatorEvent extends Event
	{
		public static var CONTENTSCHANGE: String = "IndicatorEvent.CONTENTSCHANGE";

		[Bindable]
		private var _indicator: Indicator;

		public function IndicatorEvent(aType: String, aIndicator: Indicator) {
			super(aType);

			indicator = aIndicator;
		}

		[Bindable]
		public function get indicator(): Indicator {
			return _indicator;
		}

		public function set indicator(aIndicator: Indicator): void {
			_indicator = aIndicator;
		}
	}
}
