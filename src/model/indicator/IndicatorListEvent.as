package model.indicator
{
	import flash.events.Event;

	public class IndicatorListEvent extends Event
	{
		public static var ADD: String = "IndicatorListEvent.ADD";

		public static var REMOVE: String = "IndicatorListEvent.REMOVE";

		public static var CONTENTSCHANGE: String = "IndicatorListEvent.CONTENTSCHANGE";

		public static var CURRENTCHANGED: String = "IndicatorListEvent.CURRENTCHANGED";
		
		[Bindable]
		private var _indicatorlist: IndicatorList;

		[Bindable]
		private var _indicator: Indicator;

		public function IndicatorListEvent(aType: String, aIndicatorList: IndicatorList, aIndicator: Indicator) {
			super(aType);

			indicatorList = aIndicatorList;

			indicator = aIndicator;
		}

		[Bindable]
		public function get indicatorList(): IndicatorList {
			return _indicatorlist;
		}

		public function set indicatorList(aIndicatorList: IndicatorList): void {
			_indicatorlist = aIndicatorList;
		}

		[Bindable]
		public function get indicator(): Indicator {
			return _indicator;
		}

		public function set indicator(aIndicator: Indicator): void {
			_indicator = aIndicator;
		}
	}
}
