package model.indicator
{
	import components.Connector;
	
	import flash.events.MouseEvent;
	
	import model.Constants;
	
	import mx.core.UIComponent;
	import mx.events.FlexEvent;
	import mx.events.ResizeEvent;
	
	import spark.components.Button;
	import spark.components.TextInput;
	
	public class AddIndicatorComponent extends UIComponent
	{
		/* components */
		private var _button: Button;

		private var _connector: Connector;
		
		private var _textinput: TextInput = null;

		/* Enter function */
		private var _onenter: Function = null;
		
		public function AddIndicatorComponent() {
			super();
			
			addEventListener(ResizeEvent.RESIZE, adjustChildren);
		}
		
		override protected function createChildren(): void {
			super.createChildren();
			
			_button = new Button();
			_button.width  = this.width;
			_button.height= this.height;
			_button.label  = Constants.ADDINDICATOR;
			//_button.mouseEnabled = false;
			_button.styleName = "addindicator";
			//_button.toolTip= Constants.ADDINDICATOR;
			_button.addEventListener(ResizeEvent.RESIZE, adjustChildren);
			_button.addEventListener(MouseEvent.CLICK, handleClick);
			
			addChild(_button);
			
			_connector = new Connector(0xc8c9dc);
			addChild(_connector);
			_connector.x = 1;
			_connector.y = height/2;
		}
				
		private function createTextInput(): void {
			if (_textinput)
				return;
			
			_textinput = new TextInput();
			_textinput.width = this.width; 
			_textinput.height= this.height;
			_textinput.maxChars = 50;
			_textinput.toolTip = Constants.ADDINDICATORTIP;
			_textinput.addEventListener(FlexEvent.ENTER, handleEnter);
			
			addChild(_textinput);
			_textinput.setFocus();
		}

		/* Event listeners */
		// resize of component
		private function adjustChildren(event: ResizeEvent): void {
			if (_button) {
				height= _button.getLayoutBoundsHeight();	// height needs to be adjusted to accomodate indicator titles
				
				_button.width = unscaledWidth;
				_button.height= unscaledHeight;
			}
			
			// connector
			if (_connector) {
				_connector.x = 1;
				_connector.y = unscaledHeight/2;
			}
		}
		
		private function handleClick(event:MouseEvent): void {
			createTextInput();
		}

		private function handleEnter(event:FlexEvent): void {
			if (_textinput) { 
				if (_textinput.text != "") {
					var indicator: Indicator = new Indicator("0", _textinput.text, "User " + Constants.INDICATOR + " for " + _textinput.text);
				
					if (onEnter != null) {
						onEnter(indicator);
					}
				}
	
				this.removeChild(_textinput);
				_textinput = null;
			}
		}

		public function get onEnter(): Function {
			return _onenter;
		}
		
		public function set onEnter(aOnEnter: Function): void {
			_onenter = aOnEnter;
		}
	}
}