package model.resultcanvas
{
	import mx.collections.ArrayCollection;
	import model.resultcanvas.*;
	
	public class ResultCanvasList extends ArrayCollection
	{
		[Bindable]
		private var _resultcanvas: ResultCanvas;
		
		public function ResultCanvasList() {
			super();
		}

		public static function createResultCanvasList(aAC: ArrayCollection): ResultCanvasList {
			var resultcanvaslist: ResultCanvasList = new ResultCanvasList();
			
			for (var i: int = 0; i < aAC.length; i ++) {
				resultcanvaslist.addResultCanvas(ResultCanvas.createResultCanvas(aAC[i]));
			}
			
			return resultcanvaslist;
		}
		
		public function addResultCanvas(aResultCanvas: ResultCanvas): void {
			addItem(aResultCanvas);

			aResultCanvas.addEventListener(ResultCanvasEvent.CONTENTSCHANGE, handleResultCanvasEventContentsChange);

			var event: ResultCanvasListEvent = new ResultCanvasListEvent(ResultCanvasListEvent.ADD, this, aResultCanvas);

			dispatchEvent(event);
		}

		public function removeResultCanvas(aResultCanvas: ResultCanvas): void {
			var index: int = getItemIndex(aResultCanvas);

			if (index != -1) {
				aResultCanvas.removeEventListener(ResultCanvasEvent.CONTENTSCHANGE, handleResultCanvasEventContentsChange);

				removeItemAt(index);

				var event: ResultCanvasListEvent = new ResultCanvasListEvent(ResultCanvasListEvent.REMOVE, this, aResultCanvas);

				dispatchEvent(event);
			}
		}

		public function handleResultCanvasEventContentsChange(aEvent: ResultCanvasEvent): void {
			var event: ResultCanvasListEvent = new ResultCanvasListEvent(ResultCanvasListEvent.CONTENTSCHANGE, this, aEvent.resultCanvas);

			dispatchEvent(event);
		}

		public function getResultCanvas(i: int): ResultCanvas {
			return getItemAt(i) as ResultCanvas;
		}

		public function numResultCanvasses(): int {
			return length;
		}
		
		public function findCanvasByID(aID: String): ResultCanvas {
			var result: ResultCanvas = null;
			
			for (var i: int = 0; i < numResultCanvasses(); i ++) {
				var resultcanvas: ResultCanvas = getResultCanvas(i);
				
				if (resultcanvas.id == aID) {
					result = resultcanvas;
					
					break;
				}
			}
			
			return result;
		}

		[Bindable]
		public function get resultCanvas(): ResultCanvas {
			return _resultcanvas;
		}
		
		public function set resultCanvas(aResultCanvas: ResultCanvas): void {
			_resultcanvas = aResultCanvas;
			
			var event: ResultCanvasListEvent = new ResultCanvasListEvent(ResultCanvasListEvent.CURRENTCHANGED, this, aResultCanvas);
			
			dispatchEvent(event);
		}
	}
}
