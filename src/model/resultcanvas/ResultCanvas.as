package model.resultcanvas
{
	import flash.events.EventDispatcher;
	
	import model.ModelUtils;
	import model.background.Background;
	import model.connection.ConnectionList;
	import model.indicator.Indicator;
	import model.indicator.IndicatorList;
	import model.question.QuestionList;
	import model.resulticon.ResultIcon;
	import model.resulticon.ResultIconList;
	import model.resulttag.ResultTagList;
	import model.theme.ThemeList;
	
	import mx.collections.ArrayCollection;
	
	public class ResultCanvas extends EventDispatcher
	{
		[Bindable]
		private var _id: String;

		[Bindable]
		private var _canvas: String;
		
		[Bindable]
		private var _comment: String;
		
		[Bindable]
		private var _resulticonlist: ResultIconList;
		
		[Bindable]
		private var _background: Background;
		
/*		[Bindable]
		private var _resultconnectionlist: ResultConnectionList;
*/
		[Bindable]
		private var _userindicatorlist: IndicatorList;
		
		[Bindable]
		private var _storedindicatorlist: IndicatorList;
		
		[Bindable]
		private var _resulttaglist: ResultTagList;
		
		public function ResultCanvas(aID: String, aCanvas: String, aComment: String) {
			super();
			
			id = aID;

			canvas = aCanvas;
			
			comment = aComment;
		}
		
		public static function createResultCanvas(aObject: Object): ResultCanvas {
			var id: String = aObject.url;
			
			var canvas: String = aObject.canvas;
			
			var comment: String = aObject.comment;
			
			var resultcanvas: ResultCanvas = new ResultCanvas(id, canvas, comment);
			
			if (aObject.background)
				resultcanvas.background = Background.createBackground(aObject.background as Object);
			
			var arr: Array = (aObject.icons as Array);
			var ac: ArrayCollection = new ArrayCollection(arr);
			resultcanvas.resultIconList = ResultIconList.createResultIconList(ac);

			arr = (aObject.textfields as Array);
			ac  = new ArrayCollection(arr);
			resultcanvas.resultTagList = ResultTagList.createResultTagList(ac);
			
			arr = (aObject.userIndicators as Array);
			ac  = new ArrayCollection(arr);
			resultcanvas.userIndicatorList = IndicatorList.createIndicatorList(ac);
			
			arr = (aObject.storedIndicators as Array);
			ac  = new ArrayCollection(arr);
			resultcanvas.storedIndicatorList = IndicatorList.createIndicatorList(ac);
			
			return resultcanvas;
		}

		[Bindable]
		public function get id(): String {
			return _id;
		}

		public function set id(aID: String): void {
			_id = aID;

			contentsChanged();
		}

		[Bindable]
		public function get canvas(): String {
			return _canvas;
		}
		
		public function set canvas(aCanvas: String): void {
			_canvas = aCanvas;
			
			contentsChanged();
		}
		
		[Bindable]
		public function get comment(): String {
			return _comment;
		}
		
		public function set comment(aComment: String): void {
			_comment = aComment;
		}
		
		private function contentsChanged(): void {
			var event: ResultCanvasEvent = new ResultCanvasEvent(ResultCanvasEvent.CONTENTSCHANGE, this);
		}

		[Bindable]
		public function get background(): Background {
			return _background;
		}
		
		public function set background(aBackground: Background): void {
			_background = aBackground;
		}
		
		[Bindable]
		public function get resultIconList(): ResultIconList {
			return _resulticonlist;
		}
		
		public function set resultIconList(aResultIconList: ResultIconList): void {
			_resulticonlist = aResultIconList;
		}

		[Bindable]
		public function get userIndicatorList(): IndicatorList {
			return _userindicatorlist;
		}
		
		public function set userIndicatorList(aUserIndicatorList: IndicatorList): void {
			_userindicatorlist = aUserIndicatorList;
		}
		
		[Bindable]
		public function get storedIndicatorList(): IndicatorList {
			return _storedindicatorlist;
		}
		
		public function set storedIndicatorList(aIndicatorList: IndicatorList): void {
			_storedindicatorlist = aIndicatorList;
		}
		
/*		[Bindable]
		public function get resultConnectionList(): ResultConnectionList {
			return _resultconnectionlist;
		}
		
		public function set resultConnectionList(aResultConnectionList: ResultConnectionList): void {
			_resultconnectionlist = aResultConnectionList;
			
		}
*/
		[Bindable]
		public function get resultTagList(): ResultTagList {
			return _resulttaglist;
		}
		
		public function set resultTagList(aResultTagList: ResultTagList): void {
			_resulttaglist = aResultTagList;
		}
	}
}