package model.resultcanvas
{
	import flash.events.Event;

	public class ResultCanvasEvent extends Event
	{
		public static var CONTENTSCHANGE: String = "ResultCanvasEvent.CONTENTSCHANGE";

		[Bindable]
		private var _resultcanvas: ResultCanvas;

		public function ResultCanvasEvent(aType: String, aResultCanvas: ResultCanvas) {
			super(aType);

			resultCanvas = aResultCanvas;
		}

		[Bindable]
		public function get resultCanvas(): ResultCanvas {
			return _resultcanvas;
		}

		public function set resultCanvas(aResultCanvas: ResultCanvas): void {
			_resultcanvas = aResultCanvas;
		}
	}
}
