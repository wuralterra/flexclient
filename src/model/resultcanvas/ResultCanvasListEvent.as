package model.resultcanvas
{
	import flash.events.Event;

	public class ResultCanvasListEvent extends Event
	{
		public static var ADD: String = "ResultCanvasListEvent.ADD";

		public static var REMOVE: String = "ResultCanvasListEvent.REMOVE";

		public static var CONTENTSCHANGE: String = "ResultCanvasListEvent.CONTENTSCHANGE";

		public static var CURRENTCHANGED: String = "ResultCanvasListEvent.CURRENTCHANGED";
		
		[Bindable]
		private var _resultcanvaslist: ResultCanvasList;

		[Bindable]
		private var _resultcanvas: ResultCanvas;

		public function ResultCanvasListEvent(aType: String, aResultCanvasList: ResultCanvasList, aResultCanvas: ResultCanvas) {
			super(aType);

			resultCanvasList = aResultCanvasList;

			resultCanvas = aResultCanvas;
		}

		[Bindable]
		public function get resultCanvasList(): ResultCanvasList {
			return _resultcanvaslist;
		}

		public function set resultCanvasList(aResultCanvasList: ResultCanvasList): void {
			_resultcanvaslist = aResultCanvasList;
		}

		[Bindable]
		public function get resultCanvas(): ResultCanvas {
			return _resultcanvas;
		}

		public function set resultCanvas(aResultCanvas: ResultCanvas): void {
			_resultcanvas = aResultCanvas;
		}
	}
}
