package model.question
{
	import flash.events.Event;

	public class QuestionEvent extends Event
	{
		public static var CONTENTSCHANGE: String = "QuestionEvent.CONTENTSCHANGE";

		[Bindable]
		private var _question: Question;

		public function QuestionEvent(aType: String, aQuestion: Question) {
			super(aType);

			question = aQuestion;
		}

		[Bindable]
		public function get question(): Question {
			return _question;
		}

		public function set question(aQuestion: Question): void {
			_question = aQuestion;
		}
	}
}
