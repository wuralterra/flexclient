package model.question
{
	import flash.events.Event;

	public class QuestionListEvent extends Event
	{
		public static var ADD: String = "QuestionListEvent.ADD";

		public static var REMOVE: String = "QuestionListEvent.REMOVE";

		public static var CONTENTSCHANGE: String = "QuestionListEvent.CONTENTSCHANGE";

		[Bindable]
		private var _questionlist: QuestionList;

		[Bindable]
		private var _question: Question;

		public function QuestionListEvent(aType: String, aQuestionList: QuestionList, aQuestion: Question) {
			super(aType);

			questionList = aQuestionList;
			
			question = aQuestion;
		}

		[Bindable]
		public function get questionList(): QuestionList {
			return _questionlist;
		}

		public function set questionList(aQuestionList: QuestionList): void {
			_questionlist = aQuestionList;
		}

		[Bindable]
		public function get question(): Question {
			return _question;
		}

		public function set question(aQuestion: Question): void {
			_question = aQuestion;
		}
	}
}
