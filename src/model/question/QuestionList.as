package model.question
{
	import model.ModelUtils;
	
	import mx.collections.ArrayCollection;
	
	import session.Session;
	
	public class QuestionList extends ArrayCollection
	{
		public function QuestionList() {
			super();
		}

		public static function createQuestionList(aAC: ArrayCollection): QuestionList {
			var questionlist: QuestionList = new QuestionList();
			
			for (var i: int = 0; i < aAC.length; i ++) {
				questionlist.addQuestion(Question.createQuestion(aAC[i]));
			}
			
/*			if (questionlist.numQuestions() == 0) {
				questionlist.addQuestion(new Question("qId1", "qTitle1", "qDescription 1 that needs to be a bit longer to wrap?"));
				questionlist.addQuestion(new Question("qId2", "qTitle2", "qDescription 2?"));
			}
*/			
			return questionlist;
		}
		
		public function addQuestion(aQuestion: Question): void {
			addItem(aQuestion);

			aQuestion.addEventListener(QuestionEvent.CONTENTSCHANGE, handleQuestionEventContentsChange);

			var event: QuestionListEvent = new QuestionListEvent(QuestionListEvent.ADD, this, aQuestion);

			dispatchEvent(event);
		}

		public function removeQuestion(aQuestion: Question): void {
			var index: int = getItemIndex(aQuestion);

			if (index != -1) {
				aQuestion.removeEventListener(QuestionEvent.CONTENTSCHANGE, handleQuestionEventContentsChange);

				removeItemAt(index);

				var event: QuestionListEvent = new QuestionListEvent(QuestionListEvent.REMOVE, this, aQuestion);

				dispatchEvent(event);
			}
		}

		public function handleQuestionEventContentsChange(aEvent: QuestionEvent): void {
			var event: QuestionListEvent = new QuestionListEvent(QuestionListEvent.CONTENTSCHANGE, this, aEvent.question);

			dispatchEvent(event);
		}

		public function getQuestion(i: int): Question {
			return getItemAt(i) as Question;
		}

		public function numQuestions(): int {
			return length;
		}
		
		public function findQuestionByID(aID: String): Question {
			var result: Question = null;
			
			for (var i: int = 0; i < numQuestions(); i ++) {
				var question: Question = getQuestion(i);
				
				if (question.id == aID || ModelUtils.getNumberFromId(question.id) == aID) {
					result = question;
					
					break;
				}
			}
			
			return result;
		}
	}
}
