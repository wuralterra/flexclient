package model.question
{
	import flash.events.EventDispatcher;
	
	import mx.collections.ArrayCollection;
	
	public class Question extends EventDispatcher
	{
		[Bindable]
		private var _id: String;

		[Bindable]
		private var _title: String;
		
		[Bindable]
		private var _description: String;
		
		[Bindable]
		private var _bulletlist: ArrayCollection;
		
		public function Question(aID: String, aTitle: String, aDescription: String) {
			super();
			
			id = aID;

			title = aTitle;
			
			description = aDescription;
			
			bulletList = new ArrayCollection();
		}
		
		public static function createQuestion(aObject: Object): Question {
			var id: String = aObject.url;
			
			var title: String = aObject.title;
			
			var description: String = aObject.description;
			
			var question: Question = new Question(id, title, description);
			
			var arr: Array = (aObject.bullets as Array);
			
			if (arr != null) {
				var ac: ArrayCollection = new ArrayCollection(arr);

				question.bulletList = ac;
			}

			return question;
		}
		
		[Bindable]
		public function get id(): String {
			return _id;
		}

		public function set id(aID: String): void {
			_id = aID;

			contentsChanged();
		}

		[Bindable]
		public function get description(): String {
			return _description;
		}

		public function set description(aDescription: String): void {
			_description = aDescription;

			contentsChanged();
		}
		
		[Bindable]
		public function get title(): String {
			return _title;
		}
		
		public function set title(aTitle: String): void {
			_title = aTitle;
			
			contentsChanged();
		}
		
		[Bindable]
		public function get bulletList(): ArrayCollection {
			return _bulletlist;
		}
		
		public function set bulletList(aBulletList: ArrayCollection): void {
			trace("set bulletList", id, title, description, aBulletList.length);
			_bulletlist = aBulletList;
			
			contentsChanged();
		}
		
		private function contentsChanged(): void {
			var event: QuestionEvent = new QuestionEvent(QuestionEvent.CONTENTSCHANGE, this);

			dispatchEvent(event);
		}
	}
}