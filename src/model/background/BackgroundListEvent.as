package model.background
{
	import flash.events.Event;

	public class BackgroundListEvent extends Event
	{
		public static var ADD: String = "BackgroundListEvent.ADD";

		public static var REMOVE: String = "BackgroundListEvent.REMOVE";

		public static var CONTENTSCHANGE: String = "BackgroundListEvent.CONTENTSCHANGE";

		public static var CURRENTCHANGED: String = "BackgroundListEvent.CURRENTCHANGED";
		
		[Bindable]
		private var _backgroundlist: BackgroundList;

		[Bindable]
		private var _background: Background;

		public function BackgroundListEvent(aType: String, aBackgroundList: BackgroundList, aBackground: Background) {
			super(aType);

			backgroundList = aBackgroundList;

			background = aBackground;
		}

		[Bindable]
		public function get backgroundList(): BackgroundList {
			return _backgroundlist;
		}

		public function set backgroundList(aBackgroundList: BackgroundList): void {
			_backgroundlist = aBackgroundList;
		}

		[Bindable]
		public function get background(): Background {
			return _background;
		}

		public function set background(aBackground: Background): void {
			_background = aBackground;
		}
	}
}
