package model.background
{
	import flash.events.EventDispatcher;
	
	import mx.collections.ArrayCollection;
	import mx.utils.StringUtil;
	
	import session.Session;
	
	public class Background extends EventDispatcher
	{
		[Bindable]
		private var _id: String;

		[Bindable]
		private var _title: String;
		
		[Bindable]
		private var _description: String;
		
		[Bindable]
		private var _imageurl: String;
		
		public function Background(aID: String, aTitle: String, aDescription: String, aImageURL: String) {
			super();
			
			id = aID;

			title = aTitle;
			
			description = aDescription;

			imageURL = aImageURL;
		}
		
		public static function createBackground(aObject: Object): Background {
			var id: String = aObject.url;
			
			var title: String = aObject.title;
			
			var description: String = aObject.description;
			
			var imageurl: String = aObject.imageURL;

			var background: Background = new Background(id, title, description, imageurl);
			
			Session.instance().cache.load(background.fullImageURL);
			
			return background;
		}
		
		[Bindable]
		public function get id(): String {
			return _id;
		}

		public function set id(aID: String): void {
			_id = aID;

			contentsChanged();
		}

		[Bindable]
		public function get description(): String {
			return _description;
		}

		public function set description(aDescription: String): void {
			_description = aDescription;

			contentsChanged();
		}
		
		[Bindable]
		public function get title(): String {
			return _title;
		}
		
		public function set title(aTitle: String): void {
			_title = aTitle;
			
			contentsChanged();
		}
	
		[Bindable]
		public function get fullImageURL(): String {
			if (imageURL.indexOf("http:") != -1)
				return imageURL;
			else
				return Session.instance().assetsURL + imageURL;
		}
		
		public function set fullImageURL(aImageURL: String): void {
			//trace("mkdir", "." + (aImageURL.substring(0, aImageURL.lastIndexOf("/"))).replace(/\//g, "\\"));
			//trace("wget -nH \"" + Session.instance().assetsURL + aImageURL + "\"","-O \"." + aImageURL + "\"");
			trace("wget -nH \"" + Session.instance().assetsURL + aImageURL + "\"","-O \"." + (aImageURL.substring(0, aImageURL.lastIndexOf("/"))).replace(/\//g, "\\") + "_" + (aImageURL.substring(aImageURL.lastIndexOf("/")+1)) + "\"");
		}
		
		[Bindable]
		public function get imageURL(): String {
			return _imageurl;
		}
		
		public function set imageURL(aImageURL: String): void {
			_imageurl = aImageURL;
			
			fullImageURL = aImageURL; 
			
			contentsChanged();
		}

		private function contentsChanged(): void {
			var event: BackgroundEvent = new BackgroundEvent(BackgroundEvent.CONTENTSCHANGE, this);
			
			dispatchEvent(event);
		}
	}
}