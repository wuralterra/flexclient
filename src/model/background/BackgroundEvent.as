package model.background
{
	import flash.events.Event;

	public class BackgroundEvent extends Event
	{
		public static var CONTENTSCHANGE: String = "BackgroundEvent.CONTENTSCHANGE";

		[Bindable]
		private var _background: Background;

		public function BackgroundEvent(aType: String, aBackground: Background) {
			super(aType);

			background = aBackground;
		}

		[Bindable]
		public function get background(): Background {
			return _background;
		}

		public function set background(aBackground: Background): void {
			_background = aBackground;
		}
	}
}
