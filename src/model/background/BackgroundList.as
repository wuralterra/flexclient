package model.background
{
	import model.ModelUtils;
	
	import mx.collections.ArrayCollection;
	
	import session.Session;
	
	public class BackgroundList extends ArrayCollection
	{
		[Bindable]
		private var _background: Background;
		
		public function BackgroundList() {
			super();
		}

		public static function createBackgroundList(aAC: ArrayCollection): BackgroundList {
			var backgroundlist: BackgroundList = new BackgroundList();
			
			// empty background
			backgroundlist.addBackground(null);
			
			for (var i: int = 0; i < aAC.length; i ++) {
				backgroundlist.addBackground(Background.createBackground(aAC[i]));
			}
			
			return backgroundlist;
		}
		
		public function addBackground(aBackground: Background): void {
			addItem(aBackground);

			if (aBackground)
				aBackground.addEventListener(BackgroundEvent.CONTENTSCHANGE, handleBackgroundEventContentsChange);

			var event: BackgroundListEvent = new BackgroundListEvent(BackgroundListEvent.ADD, this, aBackground);

			dispatchEvent(event);
		}

		public function removeBackground(aBackground: Background): void {
			var index: int = getItemIndex(aBackground);

			if (index != -1) {
				aBackground.removeEventListener(BackgroundEvent.CONTENTSCHANGE, handleBackgroundEventContentsChange);

				removeItemAt(index);

				var event: BackgroundListEvent = new BackgroundListEvent(BackgroundListEvent.REMOVE, this, aBackground);

				dispatchEvent(event);
			}
		}

		public function handleBackgroundEventContentsChange(aEvent: BackgroundEvent): void {
			var event: BackgroundListEvent = new BackgroundListEvent(BackgroundListEvent.CONTENTSCHANGE, this, aEvent.background);

			dispatchEvent(event);
		}

		public function getBackground(i: int): Background {
			return getItemAt(i) as Background;
		}

		public function numBackgrounds(): int {
			return length;
		}
		
		public function findBackgroundByID(aID: String): Background {
			var result: Background = null;
			
			for (var i: int = 0; i < numBackgrounds(); i ++) {
				var background: Background = getBackground(i);
				
				if (background.id == aID || ModelUtils.getNumberFromId(background.id) == aID) {
					result = background;
					
					break;
				}
			}
			
			return result;
		}
		
		public function findBackgroundByURL(aURL: String): Background {
			var result: Background = null;
			
			for (var i: int = 0; i < numBackgrounds(); i ++) {
				var background: Background = getBackground(i);
				
				if (background != null && (background.imageURL == aURL || background.fullImageURL == aURL)) {
					result = background;
					
					break;
				}
			}
			
			return result;
		}
		
		[Bindable]
		public function get background(): Background {
			return _background;
		}
		
		public function set background(aBackground: Background): void {
			_background = aBackground;
			
			var event: BackgroundListEvent = new BackgroundListEvent(BackgroundListEvent.CURRENTCHANGED, this, aBackground);
			
			dispatchEvent(event);
		}
	}
}
