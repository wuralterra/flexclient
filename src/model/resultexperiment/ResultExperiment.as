package model.resultexperiment
{
	import flash.events.EventDispatcher;
	
	import model.ModelUtils;
	import model.resultcanvas.ResultCanvasList;
	
	import mx.collections.ArrayCollection;
	
	public class ResultExperiment extends EventDispatcher
	{
		[Bindable]
		private var _id: String;
		
		private var _experiment: String;
		
		[Bindable]
		private var _comment: String;
		
		[Bindable]
		private var _userprofile: Object;
		
		[Bindable]
		private var _demographics: Object;
		
		[Bindable]
		private var _resultcanvaslist: ResultCanvasList;
		
		public function ResultExperiment(aID: String, aExperiment: String, aComment: String) {
			super();
			
			id = aID;

			experiment = aExperiment;
			
			comment = aComment;
		}
		
		public static function createResultExperiment(aObject: Object): ResultExperiment {
			var id: String = aObject.url;
			
			var experiment: String = aObject.experiment;
			
			var comment: String = aObject.comment;
			
			var resultexperiment: ResultExperiment = new ResultExperiment(id, experiment, comment);
			
			resultexperiment.userProfile = aObject.userProfile as Object;

			resultexperiment.demographics = aObject.demographics as Object;

			var arr: Array = (aObject.canvasses as Array);
			var ac: ArrayCollection = new ArrayCollection(arr);
			
			resultexperiment.resultCanvasList = ResultCanvasList.createResultCanvasList(ac);
			
			return resultexperiment;
		}
		
		[Bindable]
		public function get id(): String {
			return _id;
		}

		public function set id(aID: String): void {
			_id = aID;

			contentsChanged();
		}

		[Bindable]
		public function get comment(): String {
			return _comment;
		}
		
		public function set comment(aComment: String): void {
			_comment = aComment;
			
			contentsChanged();
		}
		
		[Bindable]
		public function get experiment(): String {
			return _experiment;
		}
		
		public function set experiment(aExperiment: String): void {
			_experiment = aExperiment;
			
			contentsChanged();
		}
		
		[Bindable]
		public function get userProfile(): Object {
			return _userprofile;
		}
		
		public function set userProfile(aUserProfile: Object): void {
			_userprofile = aUserProfile;
			
			contentsChanged();
		}
		
		[Bindable]
		public function get demographics(): Object {
			return _demographics;
		}
		
		public function set demographics(aDemographics: Object): void {
			_demographics = aDemographics;
			
			contentsChanged();
		}
		
		[Bindable]
		public function get resultCanvasList(): ResultCanvasList {
			return _resultcanvaslist;
		}

		public function set resultCanvasList(aResultCanvasList: ResultCanvasList): void {
			trace("set resultCanvasList", id, aResultCanvasList.numResultCanvasses());
			_resultcanvaslist = aResultCanvasList;

			contentsChanged();
		}

		private function contentsChanged(): void {
			var event: ResultExperimentEvent = new ResultExperimentEvent(ResultExperimentEvent.CONTENTSCHANGE, this);

			dispatchEvent(event);
		}
	}
}