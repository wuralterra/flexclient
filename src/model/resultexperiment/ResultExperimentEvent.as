package model.resultexperiment
{
	import flash.events.Event;

	public class ResultExperimentEvent extends Event
	{
		public static var CONTENTSCHANGE: String = "ResultExperimentEvent.CONTENTSCHANGE";

		[Bindable]
		private var _resultexperiment: ResultExperiment;

		public function ResultExperimentEvent(aType: String, aResultExperiment: ResultExperiment) {
			super(aType);

			resultExperiment = aResultExperiment;
		}

		[Bindable]
		public function get resultExperiment(): ResultExperiment {
			return _resultexperiment;
		}

		public function set resultExperiment(aResultExperiment: ResultExperiment): void {
			_resultexperiment = aResultExperiment;
		}
	}
}
