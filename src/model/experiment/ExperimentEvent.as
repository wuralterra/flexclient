package model.experiment
{
	import flash.events.Event;

	public class ExperimentEvent extends Event
	{
		public static var CONTENTSCHANGE: String = "ExperimentEvent.CONTENTSCHANGE";

		[Bindable]
		private var _experiment: Experiment;

		public function ExperimentEvent(aType: String, aExperiment: Experiment) {
			super(aType);

			experiment = aExperiment;
		}

		[Bindable]
		public function get experiment(): Experiment {
			return _experiment;
		}

		public function set experiment(aExperiment: Experiment): void {
			_experiment = aExperiment;
		}
	}
}
