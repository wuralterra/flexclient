package model.experiment
{
	import flash.events.EventDispatcher;
	
	import model.ModelUtils;
	import model.canvas.CanvasList;
	import model.demographics.Demographics;
	import model.language.LanguageList;
	import model.statement.StatementList;
	import model.textfragment.TextFragmentList;
	
	import mx.collections.ArrayCollection;
	
	public class Experiment extends EventDispatcher
	{
		[Bindable]
		private var _id: String;
		
		private var _title: String;
		
		[Bindable]
		private var _description: String;
		
		[Bindable]
		private var _completed: Boolean;

		[Bindable]
		private var _canvaslist: CanvasList;
		
		[Bindable]
		private var _demographics: Demographics;
		
		[Bindable]
		public var statements: StatementList;
		
		[Bindable]
		public var resultStatements: StatementList;
		
		[Bindable]
		public var resultStatements2: StatementList;
		
		[Bindable]
		public var textFragments: TextFragmentList;
		
		// Result
		//UserInfo;
		
		public function Experiment(aID: String, aTitle: String, aDescription: String, aCompleted: Boolean = false) {
			super();
			
			id = aID;

			title = aTitle;
			
			description = aDescription;

			completed = aCompleted;
		}
		
		public static function createExperiment(aObject: Object): Experiment {
			var id: String = aObject.url;
			
			var title: String = aObject.title;
			
			var description: String = aObject.description;
			
			var experiment: Experiment = new Experiment(id, title, description);
			
			if (aObject.hasOwnProperty("demographics"))
				experiment.demographics = Demographics.createDemographics(aObject.demographics);
			
			
			var arr: Array = (aObject.canvasses as Array);
			var ac: ArrayCollection = new ArrayCollection(arr);
			
			experiment.canvasList = CanvasList.createCanvasList(ac);
			
			if (aObject.hasOwnProperty("text_fragments")) {
				trace(aObject.text_fragments);
				arr = (aObject.text_fragments as Array);
				ac = new ArrayCollection(arr);
				
				experiment.textFragments = TextFragmentList.createTextFragmentList(ac);
			}
			
			if (aObject.hasOwnProperty("statements")) {
				trace(aObject.statements);
				arr = (aObject.statements as Array);
				ac = new ArrayCollection(arr);
				
				experiment.statements = StatementList.createStatementList(ac);
				experiment.resultStatements  = new StatementList();
				experiment.resultStatements2 = new StatementList();
			}
			
			return experiment;
		}
		
		public function createExperimentObject(): Object {
			var experimentobject: Object = new Object();
			
			experimentobject["experiment"] = ModelUtils.getNumberFromId(id);
			
			return experimentobject;
		}
		
		[Bindable]
		public function get id(): String {
			return _id;
		}

		public function set id(aID: String): void {
			_id = aID;

			contentsChanged();
		}

		[Bindable]
		public function get description(): String {
			return _description;
		}
		
		public function set description(aDescription: String): void {
			_description = aDescription;
			
			contentsChanged();
		}
		
		[Bindable]
		public function get title(): String {
			return _title;
		}
		
		public function set title(aTitle: String): void {
			_title = aTitle;
			
			contentsChanged();
		}
		
		[Bindable]
		public function get completed(): Boolean {
			return _completed;
		}

		public function set completed(aCompleted: Boolean): void {
			_completed = aCompleted;

			contentsChanged();
		}
		
		[Bindable]
		public function get canvasList(): CanvasList {
			return _canvaslist;
		}

		public function set canvasList(aCanvasList: CanvasList): void {
			_canvaslist = aCanvasList;

			contentsChanged();
		}

		[Bindable]
		public function get demographics(): Demographics {
			return _demographics;
		}
		
		public function set demographics(aDemographics: Demographics): void {
			_demographics = aDemographics;
			
			contentsChanged();
		}
		
		private function contentsChanged(): void {
			var event: ExperimentEvent = new ExperimentEvent(ExperimentEvent.CONTENTSCHANGE, this);

			dispatchEvent(event);
		}
	}
}