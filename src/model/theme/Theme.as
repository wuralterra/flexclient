package model.theme
{
	import flash.events.EventDispatcher;
	
	import model.icon.IconList;
	import model.question.QuestionList;
	
	import mx.collections.ArrayCollection;
	
	import session.Session;
	
	public class Theme extends EventDispatcher
	{
		public static const ThemeTypen: ArrayCollection = new ArrayCollection(['Individual', 'Sectoral']);

		public static const typeIndividual: int = 1;
		
		public static const typeSectoral: int = 2;
		
		public static const defaultThemeType: int = typeIndividual;

		[Bindable]
		private var _id: String;

		[Bindable]
		private var _title: String;
		
		[Bindable]
		private var _description: String;
		
		[Bindable]
		private var _iconlist: IconList;
		
		//Result
		[Bindable]
		private var _notes: String;
		
		[Bindable]
		private var _resulticonlist: IconList;
		
		
		public function Theme(aID: String, aTitle: String, aDescription: String) {
			super();
			
			id = aID;

			title = aTitle;
			
			description = aDescription;
			
			//Result
			//if (Session.instance().type == Session.typeCrowdSourcing)
			//	notes = aDescription;
			//else
				notes = "";
			
			resultIconList = new IconList();
		}
		
		public static function createTheme(aObject: Object): Theme {
			var id: String = aObject.url;
			
			var title: String = aObject.title;
			
			var description: String = aObject.description;
			
			var theme: Theme = new Theme(id, title, description);
			
			var arr: Array = (aObject.icons as Array);
			var ac: ArrayCollection = new ArrayCollection(arr);
			
			theme.iconList = IconList.createIconList(ac);
			
			return theme;
		}
		
		[Bindable]
		public function get id(): String {
			return _id;
		}

		public function set id(aID: String): void {
			_id = aID;

			contentsChanged();
		}

		[Bindable]
		public function get description(): String {
			return _description;
		}

		public function set description(aDescription: String): void {
			_description = aDescription;

			contentsChanged();
		}
		
		[Bindable]
		public function get title(): String {
			return _title;
		}
		
		public function set title(aTitle: String): void {
			_title = aTitle;
			
			contentsChanged();
		}
		
		[Bindable]
		public function get iconList(): IconList {
			return _iconlist;
		}

		public function set iconList(aIconList: IconList): void {
			//trace("set iconList", id, title, description, aIconList.numIcons());
			_iconlist = aIconList;

			contentsChanged();
		}

		private function contentsChanged(): void {
			var event: ThemeEvent = new ThemeEvent(ThemeEvent.CONTENTSCHANGE, this);

			dispatchEvent(event);
		}

		// Result
		[Bindable]
		public function get notes(): String {
			return _notes;
		}
		
		public function set notes(aNotes: String): void {
			_notes = aNotes;
			
			//contentsChanged();
		}

		[Bindable]
		public function get resultIconList(): IconList {
			return _resulticonlist;
		}
		
		public function set resultIconList(aIconList: IconList): void {
			//trace("set resultIconList", id, title, description, aIconList.numIcons());
			_resulticonlist = aIconList;
			
			//contentsChanged();
		}
	}
}