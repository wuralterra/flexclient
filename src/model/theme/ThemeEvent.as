package model.theme
{
	import flash.events.Event;

	public class ThemeEvent extends Event
	{
		public static var CONTENTSCHANGE: String = "ThemeEvent.CONTENTSCHANGE";

		[Bindable]
		private var _theme: Theme;

		public function ThemeEvent(aType: String, aTheme: Theme) {
			super(aType);

			theme = aTheme;
		}

		[Bindable]
		public function get theme(): Theme {
			return _theme;
		}

		public function set theme(aTheme: Theme): void {
			_theme = aTheme;
		}
	}
}
