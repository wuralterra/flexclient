package model.theme
{
	import flash.events.Event;

	public class ThemeListEvent extends Event
	{
		public static var ADD: String = "ThemeListEvent.ADD";

		public static var REMOVE: String = "ThemeListEvent.REMOVE";

		public static var CONTENTSCHANGE: String = "ThemeListEvent.CONTENTSCHANGE";

		public static var CURRENTCHANGED: String = "ThemeListEvent.CURRENTCHANGED";
		
		[Bindable]
		private var _themelist: ThemeList;

		[Bindable]
		private var _theme: Theme;

		public function ThemeListEvent(aType: String, aThemeList: ThemeList, aTheme: Theme) {
			super(aType);

			themeList = aThemeList;

			theme = aTheme;
		}

		[Bindable]
		public function get themeList(): ThemeList {
			return _themelist;
		}

		public function set themeList(aThemeList: ThemeList): void {
			_themelist = aThemeList;
		}

		[Bindable]
		public function get theme(): Theme {
			return _theme;
		}

		public function set theme(aTheme: Theme): void {
			_theme = aTheme;
		}
	}
}
