package model.theme
{
	import model.ModelUtils;
	
	import mx.collections.ArrayCollection;
	
	import session.Session;
	
	public class ThemeList extends ArrayCollection
	{
		[Bindable]
		private var _theme: Theme;
		
		public function ThemeList() {
			super();
		}

		public static function createThemeList(aAC: ArrayCollection): ThemeList {
			var themelist: ThemeList = new ThemeList();
			
			for (var i: int = 0; i < aAC.length; i ++) {
				themelist.addTheme(Theme.createTheme(aAC[i]));
			}
			
			return themelist;
		}
		
		public function addTheme(aTheme: Theme): void {
			addItem(aTheme);

			aTheme.addEventListener(ThemeEvent.CONTENTSCHANGE, handleThemeEventContentsChange);

			var event: ThemeListEvent = new ThemeListEvent(ThemeListEvent.ADD, this, aTheme);

			dispatchEvent(event);
		}

		public function removeTheme(aTheme: Theme): void {
			var index: int = getItemIndex(aTheme);

			if (index != -1) {
				aTheme.removeEventListener(ThemeEvent.CONTENTSCHANGE, handleThemeEventContentsChange);

				removeItemAt(index);

				var event: ThemeListEvent = new ThemeListEvent(ThemeListEvent.REMOVE, this, aTheme);

				dispatchEvent(event);
			}
		}

		public function handleThemeEventContentsChange(aEvent: ThemeEvent): void {
			var event: ThemeListEvent = new ThemeListEvent(ThemeListEvent.CONTENTSCHANGE, this, aEvent.theme);

			dispatchEvent(event);
		}

		public function getTheme(i: int): Theme {
			if (i < numThemes())
				return getItemAt(i) as Theme;
			else
				return null;
		}

		public function numThemes(): int {
			return length;
		}
		
		public function findThemeByID(aID: String): Theme {
			var result: Theme = null;
			
			for (var i: int = 0; i < numThemes(); i ++) {
				var theme: Theme = getTheme(i);
				
				if (theme.id == aID || ModelUtils.getNumberFromId(theme.id) == aID) {
					result = theme;
					
					break;
				}
			}
			
			return result;
		}

		[Bindable]
		public function get theme(): Theme {
			return _theme;
		}
		
		public function set theme(aTheme: Theme): void {
			_theme = aTheme;
			
			var event: ThemeListEvent = new ThemeListEvent(ThemeListEvent.CURRENTCHANGED, this, aTheme);
			
			dispatchEvent(event);
		}
	}
}
