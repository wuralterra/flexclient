package model.demographicsitem
{
	import model.ModelUtils;
	
	import mx.collections.ArrayCollection;
	
	public class DemographicsItemList extends ArrayCollection
	{
		[Bindable]
		private var _demographicsitem: DemographicsItem;
		
		public function DemographicsItemList() {
			super();
		}

		public static function createDemographicsItemList(aAC: ArrayCollection): DemographicsItemList {
			var demographicsitemlist: DemographicsItemList = new DemographicsItemList();
			
			for (var i: int = 0; i < aAC.length; i ++) {
				demographicsitemlist.addDemographicItem(DemographicsItem.createDemographicsItem(aAC[i]));
			}
			
			return demographicsitemlist;
		}
		
		public function addDemographicItem(aDemographicsItem: DemographicsItem): void {
			addItem(aDemographicsItem);

			aDemographicsItem.addEventListener(DemographicsItemEvent.CONTENTSCHANGE, handleDemographicsItemEventContentsChange);

			var event: DemographicsItemListEvent = new DemographicsItemListEvent(DemographicsItemListEvent.ADD, this, aDemographicsItem);

			dispatchEvent(event);
		}

		public function removeDemographicsItem(aDemographicsItem: DemographicsItem): void {
			var index: int = getItemIndex(aDemographicsItem);

			if (index != -1) {
				aDemographicsItem.removeEventListener(DemographicsItemEvent.CONTENTSCHANGE, handleDemographicsItemEventContentsChange);

				removeItemAt(index);

				var event: DemographicsItemListEvent = new DemographicsItemListEvent(DemographicsItemListEvent.REMOVE, this, aDemographicsItem);

				dispatchEvent(event);
			}
		}

		public function handleDemographicsItemEventContentsChange(aEvent: DemographicsItemEvent): void {
			var event: DemographicsItemListEvent = new DemographicsItemListEvent(DemographicsItemListEvent.CONTENTSCHANGE, this, aEvent.demographicsItem);

			dispatchEvent(event);
		}

		public function getDemographicsItem(i: int): DemographicsItem {
			return getItemAt(i) as DemographicsItem;
		}

		public function numDemographicsItems(): int {
			return length;
		}
		
		public function findDemographicsItemByID(aID: String): DemographicsItem {
			var result: DemographicsItem = null;
			
			for (var i: int = 0; i < numDemographicsItems(); i ++) {
				var demographicsitem: DemographicsItem = getDemographicsItem(i);
				
				if (demographicsitem.id == aID || ModelUtils.getNumberFromId(demographicsitem.id) == aID) {
					result = demographicsitem;
					
					break;
				}
			}
			
			return result;
		}
		
		public function findDemographicsItemByTitle(aTitle: String): DemographicsItem {
			var result: DemographicsItem = null;
			
			for (var i: int = 0; i < numDemographicsItems(); i ++) {
				var demographicsitem: DemographicsItem = getDemographicsItem(i);
				
				if (demographicsitem.title == aTitle) {
					result = demographicsitem;
					
					break;
				}
			}
			
			return result;
		}
		
		[Bindable]
		public function get demographicsItem(): DemographicsItem {
			return _demographicsitem;
		}
		
		public function set demographicsItem(aDemographicsItem: DemographicsItem): void {
			_demographicsitem = aDemographicsItem;
			
			var event: DemographicsItemListEvent = new DemographicsItemListEvent(DemographicsItemListEvent.CURRENTCHANGED, this, aDemographicsItem);
			
			dispatchEvent(event);
		}
		
	}
}
