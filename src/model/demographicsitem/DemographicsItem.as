package model.demographicsitem
{
	import flash.events.EventDispatcher;
	
	import model.ModelUtils;
	import model.demographicsoption.DemographicsOptionList;
	
	import mx.collections.ArrayCollection;
	
	public class DemographicsItem extends EventDispatcher
	{
		[Bindable]
		private var _id: String;

		[Bindable]
		private var _title: String;
		
		[Bindable]
		private var _description: String;
		
		[Bindable]
		private var _number: String;
		
		[Bindable]
		private var _demographicsoptionlist: DemographicsOptionList;
		
		//Result
		[Bindable]
		private var _lat: String = "";

		[Bindable]
		private var _lon: String = "";

		public function DemographicsItem(aID: String, aTitle: String, aDescription: String, aNumber: String) {
			super();
			
			id = aID;

			title = aTitle;
			
			description = aDescription;
			
			number = aNumber;
		}
		
		public static function createDemographicsItem(aObject: Object): DemographicsItem {
			var id: String = aObject.url;
			
			var title: String = aObject.title;
			
			var description: String = aObject.description;
			
			var number: String = aObject.number;

			var demographicsitem: DemographicsItem = new DemographicsItem(id, title, description, number);
			
			var arr: Array = (aObject.options as Array);
			var ac: ArrayCollection = new ArrayCollection(arr);
			
			demographicsitem.demographicsOptionList = DemographicsOptionList.createDemographicsOptionList(ac);

			return demographicsitem;
		}
		
		public function createDemographicsItemObject(): Object {
			var demographicsitemobject: Object = new Object();
			var demographicsitemid: String = ModelUtils.getNumberFromId(id);
			
			demographicsitemobject["demographicsitem"]= demographicsitemid;
			
			return demographicsitemobject;
		}
		
		[Bindable]
		public function get id(): String {
			return _id;
		}

		public function set id(aID: String): void {
			_id = aID;

			contentsChanged();
		}

		[Bindable]
		public function get number(): String {
			return _number;
		}
		
		public function set number(aNumber: String): void {
			_number = aNumber;
			
			contentsChanged();
		}
		
		[Bindable]
		public function get lat(): String {
			return _lat;
		}
		
		public function set lat(aLat: String): void {
			_lat = aLat;
			
			contentsChanged();
		}
		
		[Bindable]
		public function get lon(): String {
			return _lon;
		}
		
		public function set lon(aLon: String): void {
			_lon = aLon;
			
			contentsChanged();
		}
		
		[Bindable]
		public function get description(): String {
			return _description;
		}

		public function set description(aDescription: String): void {
			_description = aDescription;

			contentsChanged();
		}
		
		[Bindable]
		public function get title(): String {
			return _title;
		}
		
		public function set title(aTitle: String): void {
			_title = aTitle;
			
			contentsChanged();
		}
		
		[Bindable]
		public function get demographicsOptionList(): DemographicsOptionList {
			return _demographicsoptionlist;
		}
		
		public function set demographicsOptionList(aDemographicsOptionList: DemographicsOptionList): void {
			_demographicsoptionlist = aDemographicsOptionList;
			
			contentsChanged();
		}
		
		private function contentsChanged(): void {
			var event: DemographicsItemEvent = new DemographicsItemEvent(DemographicsItemEvent.CONTENTSCHANGE, this);

			dispatchEvent(event);
		}
	}
}