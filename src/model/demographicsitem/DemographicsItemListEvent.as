package model.demographicsitem
{
	import flash.events.Event;

	public class DemographicsItemListEvent extends Event
	{
		public static var ADD: String = "DemographicsItemListEvent.ADD";

		public static var REMOVE: String = "DemographicsItemListEvent.REMOVE";

		public static var CONTENTSCHANGE: String = "DemographicsItemListEvent.CONTENTSCHANGE";

		public static var CURRENTCHANGED: String = "DemographicsItemListEvent.CURRENTCHANGED";
		
		[Bindable]
		private var _demographicsitemlist: DemographicsItemList;

		[Bindable]
		private var _demographicsitem: DemographicsItem;

		public function DemographicsItemListEvent(aType: String, aDemographicsItemList: DemographicsItemList, aDemographicsItem: DemographicsItem) {
			super(aType);

			demographicsItemList = aDemographicsItemList;

			demographicsItem = aDemographicsItem;
		}

		[Bindable]
		public function get demographicsItemList(): DemographicsItemList {
			return _demographicsitemlist;
		}

		public function set demographicsItemList(aDemographicsItemList: DemographicsItemList): void {
			_demographicsitemlist = aDemographicsItemList;
		}

		[Bindable]
		public function get demographicsItem(): DemographicsItem {
			return _demographicsitem;
		}

		public function set demographicsItem(aDemographicsItem: DemographicsItem): void {
			_demographicsitem = aDemographicsItem;
		}
	}
}
