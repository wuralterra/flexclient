package model.demographicsitem
{
	import flash.events.Event;

	public class DemographicsItemEvent extends Event
	{
		public static var CONTENTSCHANGE: String = "DemographicsItemEvent.CONTENTSCHANGE";

		[Bindable]
		private var _demographicsitem: DemographicsItem;

		public function DemographicsItemEvent(aType: String, aDemographicsItem: DemographicsItem) {
			super(aType);

			demographicsItem = aDemographicsItem;
		}

		[Bindable]
		public function get demographicsItem(): DemographicsItem {
			return _demographicsitem;
		}

		public function set demographicsItem(aDemographicsItem: DemographicsItem): void {
			_demographicsitem = aDemographicsItem;
		}
	}
}
