package model.textfragment
{
	import model.ModelUtils;
	
	import mx.collections.ArrayCollection;
	
	import session.Session;
	
	public class TextFragmentList extends ArrayCollection
	{
		public function TextFragmentList() {
			super();
		}

		public static function createTextFragmentList(aAC: ArrayCollection): TextFragmentList {
			var textFragmentlist: TextFragmentList = new TextFragmentList();
			
			for (var i: int = 0; i < aAC.length; i ++) {
				textFragmentlist.addTextFragment(TextFragment.createTextFragment(aAC[i]));
			}
			
			return textFragmentlist;
		}
		
		public function addTextFragments(aAC: ArrayCollection): void {
			for (var i: int = 0; i < aAC.length; i ++) {
				addTextFragment(TextFragment.createTextFragment(aAC[i]));
			}
		}
		
		public function addTextFragment(aTextFragment: TextFragment): void {
			addItem(aTextFragment);

			aTextFragment.addEventListener(TextFragmentEvent.CONTENTSCHANGE, handleTextFragmentEventContentsChange);

			var event: TextFragmentListEvent = new TextFragmentListEvent(TextFragmentListEvent.ADD, this, aTextFragment);

			dispatchEvent(event);
		}

		public function removeTextFragment(aTextFragment: TextFragment): void {
			var index: int = getItemIndex(aTextFragment);

			if (index != -1) {
				aTextFragment.removeEventListener(TextFragmentEvent.CONTENTSCHANGE, handleTextFragmentEventContentsChange);

				removeItemAt(index);

				var event: TextFragmentListEvent = new TextFragmentListEvent(TextFragmentListEvent.REMOVE, this, aTextFragment);

				dispatchEvent(event);
			}
		}

		public function handleTextFragmentEventContentsChange(aEvent: TextFragmentEvent): void {
			var event: TextFragmentListEvent = new TextFragmentListEvent(TextFragmentListEvent.CONTENTSCHANGE, this, aEvent.textFragment);

			dispatchEvent(event);
		}

		public function getTextFragment(i: int): TextFragment {
			return getItemAt(i) as TextFragment;
		}

		public function numTextFragments(): int {
			return length;
		}
		
		public function findTextFragmentByTitle(aTitle: String): TextFragment {
			var result: TextFragment = null;
			
			for (var i: int = 0; i < numTextFragments(); i ++) {
				var textFragment: TextFragment = getTextFragment(i);
				
				if (textFragment.title == aTitle) {
					result = textFragment;
					
					break;
				}
			}
			
			return result;
		}
	}
}
