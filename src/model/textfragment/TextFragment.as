package model.textfragment
{
	import flash.events.EventDispatcher;
	
	import mx.collections.ArrayCollection;
	
	public class TextFragment extends EventDispatcher
	{
		[Bindable]
		private var _id: String;
		
		[Bindable]
		private var _title: String;
		
		[Bindable]
		private var _description: String;
		
		public function TextFragment(aID: String, aTitle: String, aDescription: String) {
			super();
			
			id = aID;
			
			title = aTitle;
			
			description = aDescription;
		}
		
		public static function createTextFragment(aObject: Object): TextFragment {
			var id: String = aObject.url;

			var title: String = aObject.title;
			
			var description: String = aObject.description;
			
			var fragment: TextFragment = new TextFragment(id, title, description);
			
			return fragment;
		}
		
		[Bindable]
		public function get id(): String {
			return _id;
		}
		
		public function set id(aID: String): void {
			_id = aID;
			
			contentsChanged();
		}
		
		[Bindable]
		public function get description(): String {
			return _description;
		}

		public function set description(aDescription: String): void {
			_description = aDescription;

			contentsChanged();
		}
		
		[Bindable]
		public function get title(): String {
			return _title;
		}
		
		public function set title(aTitle: String): void {
			_title = aTitle;
			
			contentsChanged();
		}
		
		private function contentsChanged(): void {
			var event: TextFragmentEvent = new TextFragmentEvent(TextFragmentEvent.CONTENTSCHANGE, this);

			dispatchEvent(event);
		}
	}
}