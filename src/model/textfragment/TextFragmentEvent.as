package model.textfragment
{
	import flash.events.Event;

	public class TextFragmentEvent extends Event
	{
		public static var CONTENTSCHANGE: String = "TextFragmentEvent.CONTENTSCHANGE";

		[Bindable]
		private var _textfragment: TextFragment;

		public function TextFragmentEvent(aType: String, aTextFragment: TextFragment) {
			super(aType);

			textFragment = aTextFragment;
		}

		[Bindable]
		public function get textFragment(): TextFragment {
			return _textfragment;
		}

		public function set textFragment(aTextFragment: TextFragment): void {
			_textfragment = aTextFragment;
		}
	}
}
