package model.textfragment
{
	import flash.events.Event;

	public class TextFragmentListEvent extends Event
	{
		public static var ADD: String = "TextFragmentListEvent.ADD";

		public static var REMOVE: String = "TextFragmentListEvent.REMOVE";

		public static var CONTENTSCHANGE: String = "TextFragmentListEvent.CONTENTSCHANGE";
		
		[Bindable]
		private var _textfragmentlist: TextFragmentList;

		[Bindable]
		private var _textfragment: TextFragment;

		public function TextFragmentListEvent(aType: String, aTextFragmentList: TextFragmentList, aTextFragment: TextFragment) {
			super(aType);

			textFragmentList = aTextFragmentList;

			textFragment = aTextFragment;
		}

		[Bindable]
		public function get textFragmentList(): TextFragmentList {
			return _textfragmentlist;
		}

		public function set textFragmentList(aTextFragmentList: TextFragmentList): void {
			_textfragmentlist = aTextFragmentList;
		}

		[Bindable]
		public function get textFragment(): TextFragment {
			return _textfragment;
		}

		public function set textFragment(aTextFragment: TextFragment): void {
			_textfragment = aTextFragment;
		}
	}
}
