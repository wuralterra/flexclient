package model.icon
{
	import flash.events.Event;

	public class IconListEvent extends Event
	{
		public static var ADD: String = "IconListEvent.ADD";

		public static var REMOVE: String = "IconListEvent.REMOVE";

		public static var CONTENTSCHANGE: String = "IconListEvent.CONTENTSCHANGE";

		public static var CURRENTCHANGED: String = "IconListEvent.CURRENTCHANGED";
		
		public static var SECONDCHANGED: String = "IconListEvent.SECONDCHANGED";
		
		[Bindable]
		private var _iconlist: IconList;

		[Bindable]
		private var _icon: Icon;

		public function IconListEvent(aType: String, aIconList: IconList, aIcon: Icon) {
			super(aType);

			iconList = aIconList;

			icon = aIcon;
		}

		[Bindable]
		public function get iconList(): IconList {
			return _iconlist;
		}

		public function set iconList(aIconList: IconList): void {
			_iconlist = aIconList;
		}

		[Bindable]
		public function get icon(): Icon {
			return _icon;
		}

		public function set icon(aIcon: Icon): void {
			_icon = aIcon;
		}
	}
}
