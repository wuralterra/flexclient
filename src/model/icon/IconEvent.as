package model.icon
{
	import flash.events.Event;

	public class IconEvent extends Event
	{
		public static var CONTENTSCHANGE: String = "IconEvent.CONTENTSCHANGE";
		public static var INDICATOR_ADD: String = "IconEvent.INDICATOR_ADD";
		public static var INDICATOR_REMOVE: String = "IconEvent.INDICATOR_REMOVE";

		[Bindable]
		private var _icon: Icon;

		public function IconEvent(aType: String, aIcon: Icon) {
			super(aType);

			icon = aIcon;
		}

		[Bindable]
		public function get icon(): Icon {
			return _icon;
		}

		public function set icon(aIcon: Icon): void {
			_icon = aIcon;
		}
	}
}
