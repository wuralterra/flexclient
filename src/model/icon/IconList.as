package model.icon
{
	import model.ModelUtils;
	
	import mx.collections.ArrayCollection;
	
	public class IconList extends ArrayCollection
	{
		[Bindable]
		private var _icon: Icon;
		
		[Bindable]
		private var _icon2: Icon;
		
		public function IconList() {
			super();
		}

		public static function createIconList(aAC: ArrayCollection): IconList {
			var iconlist: IconList = new IconList();
			
			for (var i: int = 0; i < aAC.length; i ++) {
				iconlist.addIcon(Icon.createIcon(aAC[i]));
			}
			
			return iconlist;
		}
		
		public function addIcon(aIcon: Icon): void {
			addItem(aIcon);

			aIcon.addEventListener(IconEvent.CONTENTSCHANGE, handleIconEventContentsChange);

			var event: IconListEvent = new IconListEvent(IconListEvent.ADD, this, aIcon);

			dispatchEvent(event);
		}

		public function removeIcon(aIcon: Icon): void {
			var index: int = getItemIndex(aIcon);

			if (index != -1) {
				aIcon.removeEventListener(IconEvent.CONTENTSCHANGE, handleIconEventContentsChange);

				removeItemAt(index);

				var event: IconListEvent = new IconListEvent(IconListEvent.REMOVE, this, aIcon);

				dispatchEvent(event);
			}
		}

		public function handleIconEventContentsChange(aEvent: IconEvent): void {
			var event: IconListEvent = new IconListEvent(IconListEvent.CONTENTSCHANGE, this, aEvent.icon);

			dispatchEvent(event);
		}

		public function getIcon(i: int): Icon {
			return getItemAt(i) as Icon;
		}

		public function numIcons(): int {
			return length;
		}
		
		public function findIconByID(aID: String): Icon {
			var result: Icon = null;
			
			for (var i: int = 0; i < numIcons(); i ++) {
				var icon: Icon = getIcon(i);
				
				if (icon.id == aID || ModelUtils.getNumberFromId(icon.id) == aID) {
					result = icon;
					
					break;
				}
			}
			
			return result;
		}
		
		public function findIconByURL(aURL: String): Icon {
			var result: Icon = null;
			
			for (var i: int = 0; i < numIcons(); i ++) {
				var icon: Icon = getIcon(i);
				
				if (icon.imageURL == aURL || icon.fullImageURL == aURL) {
					result = icon;
					
					break;
				}
			}
			
			return result;
		}
		
		[Bindable]
		public function get icon(): Icon {
			return _icon;
		}
		
		public function set icon(aIcon: Icon): void {
			_icon = aIcon;
			
			var event: IconListEvent = new IconListEvent(IconListEvent.CURRENTCHANGED, this, aIcon);
			
			dispatchEvent(event);
		}

		[Bindable]
		public function get icon2(): Icon {
			return _icon2;
		}
		
		public function set icon2(aIcon: Icon): void {
			_icon2 = aIcon;
			
			var event: IconListEvent = new IconListEvent(IconListEvent.CURRENTCHANGED, this, aIcon);
			
			dispatchEvent(event);
		}
	}
}
