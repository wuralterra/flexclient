package model.icon
{
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.display.Loader;
	import flash.display.LoaderInfo;
	import flash.events.Event;
	import flash.events.EventDispatcher;
	import flash.net.URLLoader;
	import flash.net.URLRequest;
	
	import model.ModelUtils;
	import model.indicator.Indicator;
	import model.indicator.IndicatorList;
	import model.indicator.IndicatorListEvent;
	
	import mx.collections.ArrayCollection;
	
	import session.Session;
	
	public class Icon extends EventDispatcher
	{
		[Bindable]
		private var _id: String;

		[Bindable]
		private var _title: String;
		
		[Bindable]
		private var _description: String;
		
		[Bindable]
		private var _imageurl: String;
		
/*		[Bindable]
		private var _bitmapdata: BitmapData;
*/		
		// Result
		[Bindable]
		private var _notes: String;
		
		[Bindable]
		private var _x: Number;
		
		[Bindable]
		private var _y: Number;
		
		[Bindable]
		private var _width: Number;
		
		[Bindable]
		private var _height: Number;
		
		[Bindable]
		private var _indicatorlist: IndicatorList;
		
		public function Icon(aID: String, aTitle: String, aDescription: String, aImageURL: String) {
			super();
			
			id = aID;

			title = aTitle;
			
			description = aDescription;

			imageURL = aImageURL;
			
			indicatorList = new IndicatorList();
			
			x = 0;
			
			y = 0;
			
			width = 0;
			
			height = 0;
		}
	
		public static function createIcon(aObject: Object): Icon {
			var id: String = aObject.url;
			
			var title: String = aObject.title;
			
			var description: String = aObject.description;
			
			var imageurl: String = aObject.imageURL;

			var icon: Icon = new Icon(id, title, description, imageurl);

			Session.instance().cache.load(icon.fullImageURL);
			
			return icon;
		}
		
		public function createIconObject(): Object {
			var iconobject: Object = new Object();
			
			iconobject["icon"] = ModelUtils.getNumberFromId(id);
			
			if (notes)
				iconobject["comment"] = notes;
			
			return iconobject;
		}
		
		public function connect(aIndicator: Indicator): void {
			indicatorList.addIndicator(aIndicator);
		}
		
		[Bindable]
		public function get id(): String {
			return _id;
		}

		public function set id(aID: String): void {
			_id = aID;

			contentsChanged();
		}

		[Bindable]
		public function get description(): String {
			return _description;
		}

		public function set description(aDescription: String): void {
			_description = aDescription;

			contentsChanged();
		}
		
		[Bindable]
		public function get title(): String {
			return _title;
		}
		
		public function set title(aTitle: String): void {
			_title = aTitle;
			
			contentsChanged();
		}
	
		[Bindable]
		public function get fullImageURL(): String {
			if (imageURL.indexOf("http:") != -1)
				return imageURL;
			else
				return Session.instance().assetsURL + imageURL;
		}
		
		public function set fullImageURL(aImageURL: String): void {
			//trace("mkdir", "." + (aImageURL.substring(0, aImageURL.lastIndexOf("/"))).replace(/\//g, "\\"));
			//trace("wget -nH \"" + Session.instance().assetsURL + aImageURL + "\"","-O \"." + aImageURL + "\"");
			trace("wget -nH \"" + Session.instance().assetsURL + aImageURL + "\"","-O \"." + (aImageURL.substring(0, aImageURL.lastIndexOf("/"))).replace(/\//g, "\\") + "_" + (aImageURL.substring(aImageURL.lastIndexOf("/")+1)) + "\"");
		}
		
		[Bindable]
		public function get imageURL(): String {
			return _imageurl;
		}
		
		public function set imageURL(aImageURL: String): void {
			_imageurl = aImageURL;
			
			fullImageURL = aImageURL; 
			
			contentsChanged();
		}

		// Result
		[Bindable]
		public function get x(): Number {
			return _x;
		}
		
		public function set x(aX: Number): void {
			_x = aX;
			
			//contentsChanged();
		}
		
		[Bindable]
		public function get y(): Number {
			return _y;
		}
		
		public function set y(aY: Number): void {
			_y = aY;
			
			//contentsChanged();
		}
		
		[Bindable]
		public function get width(): Number {
			return _width;
		}
		
		public function set width(aWidth: Number): void {
			_width = aWidth;
		}
		
		[Bindable]
		public function get height(): Number {
			return _height;
		}
		
		public function set height(aHeight: Number): void {
			_height = aHeight;
		}
		
		[Bindable]
		public function get notes(): String {
			return _notes;
		}
		
		public function set notes(aNotes: String): void {
			_notes = aNotes;
			
			//contentsChanged();
		}

		[Bindable]
		public function get indicatorList(): IndicatorList {
			return _indicatorlist;
		}
		
		public function set indicatorList(aIndicatorList: IndicatorList): void {
			if (_indicatorlist != null) {
				_indicatorlist.removeEventListener(IndicatorListEvent.ADD,    handleIndicatorListEventAdd);
				_indicatorlist.removeEventListener(IndicatorListEvent.REMOVE, handleIndicatorListEventRemove);
			}

			_indicatorlist = aIndicatorList;
			
			if (_indicatorlist != null) {
				_indicatorlist.addEventListener(IndicatorListEvent.ADD,    handleIndicatorListEventAdd);
				_indicatorlist.addEventListener(IndicatorListEvent.REMOVE, handleIndicatorListEventRemove);
			}
		}
		
		private function contentsChanged(): void {
			var event: IconEvent = new IconEvent(IconEvent.CONTENTSCHANGE, this);
			
			dispatchEvent(event);
		}
		
		private function handleIndicatorListEventAdd(event: IndicatorListEvent): void {
			var newevent: IconEvent = new IconEvent(IconEvent.INDICATOR_ADD, this);
			
			dispatchEvent(newevent);
		}
		
		private function handleIndicatorListEventRemove(event: IndicatorListEvent): void {
			var newevent: IconEvent = new IconEvent(IconEvent.INDICATOR_REMOVE, this);
			
			dispatchEvent(newevent);
		}
	}
}