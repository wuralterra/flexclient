package model.icon
{
	import components.Connector;
	
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.text.TextField;
	import flash.text.TextFieldType;
	import flash.text.TextFormat;
	
	import flashx.textLayout.formats.TextAlign;
	
	import model.Constants;
	import model.indicator.Indicator;
	
	import mx.binding.utils.BindingUtils;
	import mx.binding.utils.ChangeWatcher;
	import mx.controls.Button;
	import mx.core.Application;
	import mx.core.DragSource;
	import mx.core.UIComponent;
	import mx.events.DragEvent;
	import mx.events.MoveEvent;
	import mx.events.ResizeEvent;
	import mx.graphics.BitmapFillMode;
	import mx.graphics.BitmapScaleMode;
	import mx.managers.DragManager;
	
	import session.Session;
	
	import spark.components.BorderContainer;
	import spark.components.Image;
	
	public class IconComponent extends UIComponent
	{
		[Embed(source="assets/images/close.gif")]
		[Bindable]
		private var _closeimgcls: Class;
		
		[Embed(source="assets/images/resize.gif")]
		[Bindable]
		private var _resizeimgcls: Class;
		
		/* Close function */
		private var _onclose: Function = null;
		
		/* Original dimensions */
		private var _originalHeight:Number;
		private var _originalWidth: Number;
		
		private var _originalX: Number;
		private var _originalY: Number;
		
		/* components */
		private var _image: Image;

		private var _connector: Connector;
		
		private var _closeButton: Button = null;

		private var _resizeHandle: Button = null;
		
		private var _textfield: TextField = null;

		private var _tagCloseButton: Button = null;
		
		/* Start drag for resize */
		private var _startDragX: Number;
		private var _startDragY: Number;
		
		private var dX:Number, dY:Number;             

		[Bindable]
		private var _dragstarted: Boolean;
		
		[Bindable]
		private var _icon: Icon;
		
		[Bindable]
		private var session:Session = Session.instance();
		
		private var connectorChangeWatcher:ChangeWatcher;
		
		public function IconComponent() {
			super();
			minWidth = 40;
			minHeight= 40;
			
			toolTip = Constants.ICON;
			
			addEventListener(ResizeEvent.RESIZE, adjustChildren);
			
			addEventListener(MoveEvent.MOVE, handleMoveEvent);
			addEventListener(MouseEvent.MOUSE_DOWN,  mouseDownHandler);
			addEventListener(DragEvent.DRAG_ENTER, doDragEnter);
			addEventListener(DragEvent.DRAG_DROP,  doDragDrop);
			//_connector.addEventListener(DragEvent.DRAG_EXIT,    connectorDragExit);
		}

		protected function handleMoveEvent(event: MoveEvent): void {
			var textfieldheight: int = 0;
			if (_textfield)
				textfieldheight = _textfield.height
			// prevent getting outside parent
			if (y < 0) y = 0;
			if (y+height+textfieldheight > parent.height) y=parent.height - height - textfieldheight;
			if (x < 0) x = 0;
			if (x+width  > parent.width - Constants.INDICATOR_WIDTH)  x=parent.width  - width - Constants.INDICATOR_WIDTH;
		}
				
		protected function handleMouseMove(event: MouseEvent): void {
			trace("handleMouseMove");
		}
		
		override protected function createChildren(): void {
			super.createChildren();
			
			_image = new Image();
			_image.contentLoader = Session.instance().cache;
			_image.width = this.width;
			_image.height= this.height;
			_image.fillMode = BitmapFillMode.SCALE;
			_image.scaleMode= BitmapScaleMode.ZOOM;
/*			_image.horizontalCenter= 0;
			_image.verticalCenter  = 0;
*/			_image.source = icon.fullImageURL;
			_image.toolTip= icon.description;
			
			addChild(_image);

			// connector
			_connector = new Connector();
			addChild(_connector);
			_connector.buttonMode = true;
			_connector.addEventListener(MouseEvent.MOUSE_DOWN,  connectorMouseDown);
			_connector.addEventListener(DragEvent.DRAG_COMPLETE,connectorDragComplete);
			connectorChangeWatcher = BindingUtils.bindProperty(_connector, "visible", session, "notPrinting");
			
			// buttons
			createCloseButton();
			createResizeHandle();
			
			adjustChildren(null);
		}
				
		private function mouseDownHandler(event:MouseEvent):void {                 
			if (!dragStarted) {
				dX = x - stage.mouseX;                 
				dY = y - stage.mouseY;             
			
				//startDrag();                 
				stage.addEventListener(MouseEvent.MOUSE_MOVE, handleDrag);                 
				stage.addEventListener(MouseEvent.MOUSE_UP, handleUp);
			}
		}
		
		private function handleDrag(event:MouseEvent):void {                 
			x = stage.mouseX + dX;                 
			y = stage.mouseY + dY;
			
			event.updateAfterEvent();             
		}
		
		private function handleUp(event:MouseEvent):void {                 
			//stopDrag();                 
			stage.removeEventListener(MouseEvent.MOUSE_MOVE, handleDrag);                 
			stage.removeEventListener(MouseEvent.MOUSE_UP, handleUp);	
		}
			
		private function adjustChildren(event: ResizeEvent): void {
			if (_image) {
				_image.width = unscaledWidth;
				_image.height= unscaledHeight;
			}

			// connector
			if (_connector) {
				_connector.x = unscaledWidth - 1;
				_connector.y = unscaledHeight/2;
			}
			
			// textfield
			if (_textfield) {
				if (_image) {
					var textformat:TextFormat = new TextFormat("Arial");
					textformat.align = TextAlign.CENTER;
					_textfield.width = _image.width;
						
					_textfield.text = _textfield.text;
					_textfield.setTextFormat(textformat);
					_textfield.height = 15 * _textfield.numLines + 5;
				}
				
				_textfield.x = 0;
				_textfield.y = unscaledHeight;

				addChild(_textfield);
			}
			
			// buttons
			if (_closeButton) {
				_closeButton.x = unscaledWidth - _closeButton.width - 1;
				_closeButton.y = 1;
			}
			
			if (_resizeHandle) {
				_resizeHandle.x = unscaledWidth  - _resizeHandle.width  - 1;
				_resizeHandle.y = unscaledHeight - _resizeHandle.height - 1;
			}

			if (_tagCloseButton) {
				_tagCloseButton.x = unscaledWidth - _tagCloseButton.width - 1;
				_tagCloseButton.y = unscaledHeight + _textfield.height - _tagCloseButton.height - 1;

				addChild(_tagCloseButton);
			}
		}
		
		public function get onClose(): Function {
			return _onclose;
		}
		
		public function set onClose(aOnClose: Function): void {
			_onclose = aOnClose;
		}

		/* Functions to create buttons */
		private function createCloseButton(): void {
			if ( _closeButton )
				return;
			
			_closeButton = new Button();
			_closeButton.width = 10;
			_closeButton.height = 10;
			
			_closeButton.buttonMode = true;
			_closeButton.useHandCursor = true;
			_closeButton.setStyle("icon", _closeimgcls);
			_closeButton.addEventListener(MouseEvent.CLICK, closeButtonHandler);
			BindingUtils.bindProperty(_closeButton, "visible", session, "notPrinting");
			
			_closeButton.toolTip = "Close " + Constants.ICON;
			
			addChild( _closeButton );
		}

		private function createResizeHandle(): void {
			if ( _resizeHandle )
				return;
			
			_resizeHandle = new Button();
			_resizeHandle.width = 11;
			_resizeHandle.height= 11;
			
			_resizeHandle.buttonMode = true;
			_resizeHandle.useHandCursor = true;
			_resizeHandle.setStyle("icon", _resizeimgcls);
			_resizeHandle.addEventListener(MouseEvent.MOUSE_DOWN, resizeHandleMouseDown);
			BindingUtils.bindProperty(_resizeHandle, "visible", session, "notPrinting");
			
			_resizeHandle.toolTip = "Resize " + Constants.ICON;
			
			addChild( _resizeHandle );
		}
		
		private function createTagCloseButton(): void {
			if ( _tagCloseButton )
				return;
			
			_tagCloseButton = new Button();
			_tagCloseButton.width = 10;
			_tagCloseButton.height= 10;
			
			_tagCloseButton.buttonMode = true;
			_tagCloseButton.useHandCursor = true;
			_tagCloseButton.setStyle("icon", _closeimgcls);
			_tagCloseButton.addEventListener(MouseEvent.CLICK, tagCloseButtonHandler);
			BindingUtils.bindProperty(_tagCloseButton, "visible", session, "notPrinting");
			
			_tagCloseButton.toolTip = "Remove " + Constants.ICONTAG + " from " + Constants.ICON;
			
			addChild(_tagCloseButton);

			adjustChildren(null);
		}
		
		private function createTextField(aString: String): void {
			if (_textfield)
				return;
			
			var textformat:TextFormat = new TextFormat("Arial");
			textformat.align = TextAlign.CENTER;
			
			_textfield = new TextField();
			_textfield.type = TextFieldType.INPUT;
			_textfield.width = 135;
			_textfield.height = 20; 
			_textfield.text = aString;
			_textfield.setTextFormat(textformat);
			_textfield.background = true; 
			_textfield.backgroundColor = 0xffffff; 
			_textfield.border = false;
			_textfield.addEventListener(Event.CHANGE, handleChange);
			_textfield.addEventListener(MouseEvent.MOUSE_DOWN, tagMouseDown);
			_textfield.addEventListener(MouseEvent.CLICK, handleClick);
			_textfield.wordWrap = true;
			_textfield.multiline= true;
			
			addChild(_textfield);
			
			toolTip = Constants.ICON + " with " + Constants.ICONTAG;

			adjustTextfieldHeight();
		}
		
		/* Event listeners */
		protected function closeButtonHandler(event: MouseEvent): void {
			this.parent.removeChild(this);
			
			if (onClose != null) {
				onClose(icon);
			}
		}
		
		protected function tagCloseButtonHandler(event: MouseEvent): void {
			removeChild(_textfield);
			_textfield = null;
			
			toolTip = Constants.ICON;
			
			removeChild(_tagCloseButton);
			_tagCloseButton = null;
		}
		
		private function connectorMouseDown(event: MouseEvent): void {
			if (icon != null) {
				//trace("connectorMouseDown");
				// Get the drag initiator component from the event object.
				var dragInitiator: Connector = event.currentTarget as Connector;
				
				// DragSource object for adding data to drag
				var ds: DragSource = new DragSource();
				ds.addData(icon, "connect");
				
				var connectorProxy: Connector= new Connector();
				
				// Call the DragManager doDrag() method to start the drag. 
				DragManager.doDrag(dragInitiator, ds, event, connectorProxy);
				
				event.stopPropagation();
			}
		}
		
		private function tagMouseDown(event: MouseEvent): void {
			event.stopPropagation();
		}
		
		private function connectorDragComplete(event: DragEvent): void {
			// Do nothing (yet)
		}
		
		private function doDragEnter(event: DragEvent): void {
			if (event.dragSource.hasFormat("connect")) {
				var data: Object = event.dragSource.dataForFormat("connect");
				
				// Accept the drag only if the user is dragging Indicator data 
				if (data is Indicator) {
					DragManager.acceptDragDrop(IconComponent(event.currentTarget));
				}
			}
			if (event.dragSource.hasFormat("tag") && !_textfield) {
				var str: Object = event.dragSource.dataForFormat("tag");
				
				if (str is String) {
					DragManager.acceptDragDrop(IconComponent(event.currentTarget));
				}
			}
		}
		
		private function doDragExit(event: DragEvent): void {
			// Do nothing (yet)
		}
		
		// Called if the target accepts the dragged object and the user 
		// releases the mouse button while over the canvas. 
		private function doDragDrop(event: DragEvent): void {
			if (event.dragSource.hasFormat("connect")) {
				var data: Object = event.dragSource.dataForFormat("connect");
				
				if (data is Indicator) {
					// this (Icon) can connect to data as Indicator
					icon.connect(data as Indicator);
				}
			}
			if (event.dragSource.hasFormat("tag")) {
				var str: Object = event.dragSource.dataForFormat("tag");
				
				if (str is String) {
					createTextField(str as String);

					createTagCloseButton();
				}
			}
		}
		
		private function handleConnect(event: IconEvent): void {
			trace("handleConnect", connectorChangeWatcher.isWatching(), icon.indicatorList.numIndicators()); 
			if (connectorChangeWatcher.isWatching())
				connectorChangeWatcher.unwatch();
		}
		
		private function handleUnConnect(event: IconEvent): void {
			trace("handleUnConnect", connectorChangeWatcher.isWatching(), icon.indicatorList.numIndicators()); 
			if (!connectorChangeWatcher.isWatching() && icon.indicatorList.numIndicators()==0)
				connectorChangeWatcher.reset(session);
		}
		
		private function handleClick(event: MouseEvent): void {
			if (_textfield.text == Constants.ICONTEXT)
				_textfield.setSelection(0, Constants.ICONTEXT.length);
		}
		
		private function handleChange(event: Event): void {
			if (icon != null)
				icon.notes = _textfield.text; 

			adjustTextfieldHeight();
		}
		
		private function adjustTextfieldHeight(): void {
			_textfield.height = 15 * _textfield.numLines + 5;
			
			adjustChildren(null);
		}
		
		protected function resizeHandleMouseDown(event: MouseEvent): void {
			dragStarted = true;
			
			stage.addEventListener(MouseEvent.MOUSE_MOVE, resizeHandleMouseMove);
			stage.addEventListener(MouseEvent.MOUSE_UP,   resizeHandleMouseUp);
			
			_startDragX = this.parent.mouseX;
			_startDragY = this.parent.mouseY;
			
			saveOriginalDimensions();
		}
		
		protected function resizeHandleMouseMove(event: MouseEvent): void {
			if (dragStarted) {
				var currentX: Number = this.parent.mouseX;
				var currentY: Number = this.parent.mouseY;
				
				var deltaX: Number = currentX - _startDragX;
				var deltaY: Number = currentY - _startDragY;
				
				width = _originalWidth + deltaX;
				height= _originalHeight + deltaY;
				
				// prevent getting outside parent
				if (y+height > parent.height) y=parent.height - height;
				if (x+width  > parent.width)  x=parent.width  - width;
				// prevent getting negative size
				if (height < minHeight) height= minHeight;
				if (width  < minWidth)  width = minWidth;
				
				adjustChildren(null);
			}
		}
		
		protected function resizeHandleMouseUp(event: MouseEvent): void {
			stage.removeEventListener(MouseEvent.MOUSE_UP, resizeHandleMouseUp);
			stage.removeEventListener(MouseEvent.MOUSE_MOVE, resizeHandleMouseMove);
			
			dragStarted = false;
		}
		
		/* Functions to save and restore dimensions */
		private function saveOriginalDimensions(): void {
			_originalHeight= height;
			_originalWidth = width;
			
			_originalX = x;
			_originalY = y;
		}
		
		private function restoreOriginalDimensions(): void {
			height= _originalHeight;
			width = _originalWidth;
			
			x = _originalX;
			y = _originalY;
		}
		
		/* Getters and setters */
		[Bindable]
		protected function get dragStarted(): Boolean {
			return _dragstarted;
		}
		
		protected function set dragStarted(aDragStarted: Boolean): void {
			_dragstarted = aDragStarted;
		}

		[Bindable]
		public function get icon(): Icon {
			return _icon;
		}
		
		public function set icon(aIcon: Icon): void {
			_icon = aIcon;

			if (aIcon != null) {
				//trace("addEventlisteners");
				aIcon.addEventListener(IconEvent.INDICATOR_ADD,    handleConnect);
				aIcon.addEventListener(IconEvent.INDICATOR_REMOVE, handleUnConnect);
				
				if (aIcon.notes != null && aIcon.notes != "") {
					createTextField(aIcon.notes);
					
					createTagCloseButton();
				}
			}
		}
	}
}