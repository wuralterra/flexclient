package model.statement
{
	import flash.events.Event;

	public class StatementEvent extends Event
	{
		public static var CONTENTSCHANGE: String = "StatementEvent.CONTENTSCHANGE";

		[Bindable]
		private var _statement: Statement;

		public function StatementEvent(aType: String, aStatement: Statement) {
			super(aType);

			statement = aStatement;
		}

		[Bindable]
		public function get statement(): Statement {
			return _statement;
		}

		public function set statement(aStatement: Statement): void {
			_statement = aStatement;
		}
	}
}
