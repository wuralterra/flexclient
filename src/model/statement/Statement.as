package model.statement
{
	import flash.events.EventDispatcher;
	
	import mx.collections.ArrayCollection;
	
	public class Statement extends EventDispatcher
	{
		[Bindable]
		private var _id: String;
		
		[Bindable]
		private var _title: String;

		[Bindable]
		private var _description: String;
		
		public function Statement(aID: String, aTitle: String, aDescription: String) {
			super();
			
			id = aID;
			
			title = aTitle;

			description = aDescription;
		}
		
		public static function createStatement(aObject: Object): Statement {
			var id: String = aObject.url;

			var title: String = aObject.title;

			var description: String = aObject.description;
			
			var statement: Statement = new Statement(id, title, description);
			
			return statement;
		}
		
		[Bindable]
		public function get id(): String {
			return _id;
		}
		
		public function set id(aID: String): void {
			_id = aID;
			
			contentsChanged();
		}
		
		[Bindable]
		public function get title(): String {
			return _title;
		}
		
		public function set title(aTitle: String): void {
			_title = aTitle;
			
			contentsChanged();
		}
		
		[Bindable]
		public function get description(): String {
			return _description;
		}

		public function set description(aDescription: String): void {
			_description = aDescription;

			contentsChanged();
		}
		
		private function contentsChanged(): void {
			var event: StatementEvent = new StatementEvent(StatementEvent.CONTENTSCHANGE, this);

			dispatchEvent(event);
		}
	}
}