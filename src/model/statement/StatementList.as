package model.statement
{
	import model.ModelUtils;
	
	import mx.collections.ArrayCollection;
	
	import session.Session;
	
	public class StatementList extends ArrayCollection
	{
		public function StatementList() {
			super();
		}

		public static function createStatementList(aAC: ArrayCollection): StatementList {
			var statementlist: StatementList = new StatementList();
			
			for (var i: int = 0; i < aAC.length; i ++) {
				statementlist.addStatement(Statement.createStatement(aAC[i]));
			}
			
			return statementlist;
		}
		
		public function addStatements(aAC: ArrayCollection): void {
			for (var i: int = 0; i < aAC.length; i ++) {
				addStatement(Statement.createStatement(aAC[i]));
			}
		}
		
		public function addStatement(aStatement: Statement): void {
			addItem(aStatement);

			aStatement.addEventListener(StatementEvent.CONTENTSCHANGE, handleStatementEventContentsChange);

			var event: StatementListEvent = new StatementListEvent(StatementListEvent.ADD, this, aStatement);

			dispatchEvent(event);
		}

		public function removeStatement(aStatement: Statement): void {
			var index: int = getItemIndex(aStatement);

			if (index != -1) {
				aStatement.removeEventListener(StatementEvent.CONTENTSCHANGE, handleStatementEventContentsChange);

				removeItemAt(index);

				var event: StatementListEvent = new StatementListEvent(StatementListEvent.REMOVE, this, aStatement);

				dispatchEvent(event);
			}
		}

		public function handleStatementEventContentsChange(aEvent: StatementEvent): void {
			var event: StatementListEvent = new StatementListEvent(StatementListEvent.CONTENTSCHANGE, this, aEvent.statement);

			dispatchEvent(event);
		}

		public function getStatement(i: int): Statement {
			return getItemAt(i) as Statement;
		}

		public function numStatements(): int {
			return length;
		}
		
		public function findStatementByID(aID: String): Statement {
			var result: Statement = null;
			
			for (var i: int = 0; i < numStatements(); i ++) {
				var statement: Statement = getStatement(i);
				
				if (statement.id == aID) {
					result = statement;
					
					break;
				}
			}
			
			return result;
		}
	}
}
