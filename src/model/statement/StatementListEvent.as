package model.statement
{
	import flash.events.Event;

	public class StatementListEvent extends Event
	{
		public static var ADD: String = "StatementListEvent.ADD";

		public static var REMOVE: String = "StatementListEvent.REMOVE";

		public static var CONTENTSCHANGE: String = "StatementListEvent.CONTENTSCHANGE";

		[Bindable]
		private var _statementlist: StatementList;

		[Bindable]
		private var _statement: Statement;

		public function StatementListEvent(aType: String, aStatementList: StatementList, aStatement: Statement) {
			super(aType);

			statementList = aStatementList;

			statement = aStatement;
		}

		[Bindable]
		public function get statementList(): StatementList {
			return _statementlist;
		}

		public function set statementList(aStatementList: StatementList): void {
			_statementlist = aStatementList;
		}

		[Bindable]
		public function get statement(): Statement {
			return _statement;
		}

		public function set statement(aStatement: Statement): void {
			_statement = aStatement;
		}
	}
}
