package model.language
{
	import flash.events.Event;

	public class LanguageEvent extends Event
	{
		public static var CONTENTSCHANGE: String = "LanguageEvent.CONTENTSCHANGE";

		[Bindable]
		private var _language: Language;

		public function LanguageEvent(aType: String, aLanguage: Language) {
			super(aType);

			language = aLanguage;
		}

		[Bindable]
		public function get language(): Language {
			return _language;
		}

		public function set language(aLanguage: Language): void {
			_language = aLanguage;
		}
	}
}
