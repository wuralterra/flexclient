package model.language
{
	import flash.events.Event;

	public class LanguageListEvent extends Event
	{
		public static var ADD: String = "LanguageListEvent.ADD";

		public static var REMOVE: String = "LanguageListEvent.REMOVE";

		public static var CONTENTSCHANGE: String = "LanguageListEvent.CONTENTSCHANGE";

		public static var CURRENTCHANGED: String = "LanguageListEvent.CURRENTCHANGED";
		
		[Bindable]
		private var _languagelist: LanguageList;

		[Bindable]
		private var _language: Language;

		public function LanguageListEvent(aType: String, aLanguageList: LanguageList, aLanguage: Language) {
			super(aType);

			languageList = aLanguageList;

			language = aLanguage;
		}

		[Bindable]
		public function get languageList(): LanguageList {
			return _languagelist;
		}

		public function set languageList(aLanguageList: LanguageList): void {
			_languagelist = aLanguageList;
		}

		[Bindable]
		public function get language(): Language {
			return _language;
		}

		public function set language(aLanguage: Language): void {
			_language = aLanguage;
		}
	}
}
