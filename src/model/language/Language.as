package model.language
{
	import flash.events.EventDispatcher;
	
	import mx.collections.ArrayCollection;
	
	public class Language extends EventDispatcher
	{
		[Bindable]
		private var _title: String;
		
		[Bindable]
		private var _description: String;
		
		public function Language(aTitle: String, aDescription: String) {
			super();
			
			title = aTitle;
			
			description = aDescription;
		}
		
		public static function createLanguage(aObject: Object): Language {
			var title: String = aObject.language;
			
			var description: String = aObject.description;
			
			if (description == "")
				description = "No description for: " + title;
			
			var language: Language = new Language(title, description);
			
			return language;
		}
		
		[Bindable]
		public function get description(): String {
			return _description;
		}

		public function set description(aDescription: String): void {
			_description = aDescription;

			contentsChanged();
		}
		
		[Bindable]
		public function get title(): String {
			return _title;
		}
		
		public function set title(aTitle: String): void {
			_title = aTitle;
			
			icon = aTitle;
			
			contentsChanged();
		}
		
		[Bindable]
		public function get icon(): String {
			return "assets\\images\\" + _title + ".png";
		}
		
		public function set icon(aTitle: String): void {
			//dummy
		}
		
		private function contentsChanged(): void {
			var event: LanguageEvent = new LanguageEvent(LanguageEvent.CONTENTSCHANGE, this);

			dispatchEvent(event);
		}
	}
}