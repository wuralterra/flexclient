package model.language
{
	import model.ModelUtils;
	
	import mx.collections.ArrayCollection;
	
	import session.Session;
	
	public class LanguageList extends ArrayCollection
	{
		[Bindable]
		private var _language: Language;
		
		public function LanguageList() {
			super();
		}

		public static function createLanguageList(aAC: ArrayCollection): LanguageList {
			var languagelist: LanguageList = new LanguageList();
			
			for (var i: int = 0; i < aAC.length; i ++) {
				languagelist.addLanguage(Language.createLanguage(aAC[i]));
			}
			
			return languagelist;
		}
		
		public function addLanguages(aAC: ArrayCollection): void {
			for (var i: int = 0; i < aAC.length; i ++) {
				addLanguage(Language.createLanguage(aAC[i]));
			}
			
			if (numLanguages() == 1) {
				language = getLanguage(0);
			}				
		}
		
		public function addLanguage(aLanguage: Language): void {
			addItem(aLanguage);

			aLanguage.addEventListener(LanguageEvent.CONTENTSCHANGE, handleLanguageEventContentsChange);

			var event: LanguageListEvent = new LanguageListEvent(LanguageListEvent.ADD, this, aLanguage);

			dispatchEvent(event);
		}

		public function removeLanguage(aLanguage: Language): void {
			var index: int = getItemIndex(aLanguage);

			if (index != -1) {
				aLanguage.removeEventListener(LanguageEvent.CONTENTSCHANGE, handleLanguageEventContentsChange);

				removeItemAt(index);

				var event: LanguageListEvent = new LanguageListEvent(LanguageListEvent.REMOVE, this, aLanguage);

				dispatchEvent(event);
			}
		}

		public function handleLanguageEventContentsChange(aEvent: LanguageEvent): void {
			var event: LanguageListEvent = new LanguageListEvent(LanguageListEvent.CONTENTSCHANGE, this, aEvent.language);

			dispatchEvent(event);
		}

		public function getLanguage(i: int): Language {
			return getItemAt(i) as Language;
		}

		public function numLanguages(): int {
			return length;
		}
		
		public function findLanguageByTitle(aTitle: String): Language {
			var result: Language = null;
			
			for (var i: int = 0; i < numLanguages(); i ++) {
				var language: Language = getLanguage(i);
				
				if (language.title == aTitle) {
					result = language;
					
					break;
				}
			}
			
			return result;
		}

		[Bindable]
		public function get language(): Language {
			return _language;
		}
		
		public function set language(aLanguage: Language): void {
			_language = aLanguage;
			
			var event: LanguageListEvent = new LanguageListEvent(LanguageListEvent.CURRENTCHANGED, this, aLanguage);
			
			dispatchEvent(event);
		}
	}
}
