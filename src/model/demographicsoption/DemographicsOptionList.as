package model.demographicsoption
{
	import model.ModelUtils;
	
	import mx.collections.ArrayCollection;
	
	public class DemographicsOptionList extends ArrayCollection
	{
		[Bindable]
		private var _demographicsOption: DemographicsOption;
		
		public function DemographicsOptionList() {
			super();
		}

		public static function createDemographicsOptionList(aAC: ArrayCollection): DemographicsOptionList {
			var demographicsdemographicsOptionlist: DemographicsOptionList = new DemographicsOptionList();
			
			for (var i: int = 0; i < aAC.length; i ++) {
				demographicsdemographicsOptionlist.addDemographicsOption(DemographicsOption.createDemographicsOption(aAC[i]));
			}
			
			return demographicsdemographicsOptionlist;
		}
		
		public function addDemographicsOption(aDemographicsOption: DemographicsOption): void {
			if (aDemographicsOption != null) {
				addItem(aDemographicsOption);
	
				aDemographicsOption.addEventListener(DemographicsOptionEvent.CONTENTSCHANGE, handleDemographicsOptionEventContentsChange);
	
				var event: DemographicsOptionListEvent = new DemographicsOptionListEvent(DemographicsOptionListEvent.ADD, this, aDemographicsOption);
	
				dispatchEvent(event);
			}
		}

		public function removeDemographicsOption(aDemographicsOption: DemographicsOption): void {
			var index: int = getItemIndex(aDemographicsOption);

			if (index != -1) {
				aDemographicsOption.removeEventListener(DemographicsOptionEvent.CONTENTSCHANGE, handleDemographicsOptionEventContentsChange);

				removeItemAt(index);

				var event: DemographicsOptionListEvent = new DemographicsOptionListEvent(DemographicsOptionListEvent.REMOVE, this, aDemographicsOption);

				dispatchEvent(event);
			}
		}

		public function handleDemographicsOptionEventContentsChange(aEvent: DemographicsOptionEvent): void {
			var event: DemographicsOptionListEvent = new DemographicsOptionListEvent(DemographicsOptionListEvent.CONTENTSCHANGE, this, aEvent.demographicsOption);

			dispatchEvent(event);
		}

		public function getDemographicsOption(i: int): DemographicsOption {
			return getItemAt(i) as DemographicsOption;
		}

		public function numDemographicsOptions(): int {
			return length;
		}
		
		public function findDemographicsOptionByID(aID: String): DemographicsOption {
			var result: DemographicsOption = null;
			
			for (var i: int = 0; i < numDemographicsOptions(); i ++) {
				var demographicsOption: DemographicsOption = getDemographicsOption(i);
				
				if (demographicsOption.id == aID || ModelUtils.getNumberFromId(demographicsOption.id) == aID) {
					result = demographicsOption;
					
					break;
				}
			}
			
			return result;
		}
		
		public function findDemographicsOptionByURL(aURL: String): DemographicsOption {
			var result: DemographicsOption = null;
			
			for (var i: int = 0; i < numDemographicsOptions(); i ++) {
				var demographicsOption: DemographicsOption = getDemographicsOption(i);
				
				if (demographicsOption.imageURL == aURL || demographicsOption.fullImageURL == aURL) {
					result = demographicsOption;
					
					break;
				}
			}
			
			return result;
		}
		
		[Bindable]
		public function get demographicsOption(): DemographicsOption {
			return _demographicsOption;
		}
		
		public function set demographicsOption(aDemographicsOption: DemographicsOption): void {
			_demographicsOption = aDemographicsOption;
			
			var event: DemographicsOptionListEvent = new DemographicsOptionListEvent(DemographicsOptionListEvent.CURRENTCHANGED, this, aDemographicsOption);
			
			dispatchEvent(event);
		}
		
	}
}
