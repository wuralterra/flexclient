package model.demographicsoption
{
	import flash.events.EventDispatcher;
	
	import model.ModelUtils;
	
	import mx.collections.ArrayCollection;
	
	import session.Session;
	
	public class DemographicsOption extends EventDispatcher
	{
		[Bindable]
		private var _id: String;

		[Bindable]
		private var _title: String;
		
		[Bindable]
		private var _description: String;
		
		[Bindable]
		private var _number: String;
		
		[Bindable]
		private var _imageurl: String;
		
		//Result
		
		public function DemographicsOption(aID: String, aTitle: String, aDescription: String, aNumber: String, aImageURL: String) {
			super();
			
			id = aID;

			title = aTitle;
			
			description = aDescription;

			number = aNumber;

			imageURL = aImageURL;
		}
		
		public static function createDemographicsOption(aObject: Object): DemographicsOption {
			var demographicsoption: DemographicsOption;
			
			var id: String = aObject.url;
			
			var title: String = aObject.title;
			
			var description: String = aObject.description;
			
			var number: String = aObject.number;
			
			var imageurl: String = aObject.imageURL;

			if (title == "" && imageurl == "")
				trace("createDemographicsOption", id, title, description, number, imageurl);
			else {
				demographicsoption = new DemographicsOption(id, title, description, number, imageurl);
				
				Session.instance().cache.load(demographicsoption.fullImageURL);
			}
				
			return demographicsoption;
		}
		
		public function createOptionObject(): Object {
			var demographicsoptionobject: Object = new Object();
			var demographicsoptionid: String = ModelUtils.getNumberFromId(id);
			
			demographicsoptionobject["demographicsoption"]= demographicsoptionid;
			
			return demographicsoptionobject;
		}
		
		[Bindable]
		public function get id(): String {
			return _id;
		}

		public function set id(aID: String): void {
			_id = aID;

			contentsChanged();
		}

		[Bindable]
		public function get number(): String {
			return _number;
		}
		
		public function set number(aNumber: String): void {
			_number = aNumber;
			
			contentsChanged();
		}
		
		[Bindable]
		public function get description(): String {
			return _description;
		}

		public function set description(aDescription: String): void {
			_description = aDescription;

			contentsChanged();
		}
		
		[Bindable]
		public function get title(): String {
			return _title;
		}
		
		public function set title(aTitle: String): void {
			_title = aTitle;
			
			contentsChanged();
		}
		
		[Bindable]
		public function get fullImageURL(): String {
			if (imageURL.indexOf("http:") != -1)
				return imageURL;
			else
				return Session.instance().assetsURL + imageURL;
		}
		
		public function set fullImageURL(aImageURL: String): void {
			if (aImageURL != "") {
				//trace("mkdir", "." + (aImageURL.substring(0, aImageURL.lastIndexOf("/"))).replace(/\//g, "\\"));
				//trace("wget -nH \"" + Session.instance().assetsURL + aImageURL + "\"","-O \"." + aImageURL + "\"");
			}
		}
		
		[Bindable]
		public function get imageURL(): String {
			return _imageurl;
		}
		
		public function set imageURL(aImageURL: String): void {
			_imageurl = aImageURL;
			
			fullImageURL = aImageURL; 
			
			contentsChanged();
		}
		
		// Result
		
		private function contentsChanged(): void {
			var event: DemographicsOptionEvent = new DemographicsOptionEvent(DemographicsOptionEvent.CONTENTSCHANGE, this);

			dispatchEvent(event);
		}
	}
}