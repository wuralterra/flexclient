package model.demographicsoption
{
	import flash.events.Event;

	public class DemographicsOptionEvent extends Event
	{
		public static var CONTENTSCHANGE: String = "DemographicsOptionEvent.CONTENTSCHANGE";

		[Bindable]
		private var _demographicsOption: DemographicsOption;

		public function DemographicsOptionEvent(aType: String, aDemographicsOption: DemographicsOption) {
			super(aType);

			demographicsOption = aDemographicsOption;
		}

		[Bindable]
		public function get demographicsOption(): DemographicsOption {
			return _demographicsOption;
		}

		public function set demographicsOption(aDemographicsOption: DemographicsOption): void {
			_demographicsOption = aDemographicsOption;
		}
	}
}
