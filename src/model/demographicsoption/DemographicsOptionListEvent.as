package model.demographicsoption
{
	import flash.events.Event;

	public class DemographicsOptionListEvent extends Event
	{
		public static var ADD: String = "OptionListEvent.ADD";

		public static var REMOVE: String = "OptionListEvent.REMOVE";

		public static var CONTENTSCHANGE: String = "OptionListEvent.CONTENTSCHANGE";

		public static var CURRENTCHANGED: String = "OptionListEvent.CURRENTCHANGED";
		
		[Bindable]
		private var _demographicsoptionlist: DemographicsOptionList;

		[Bindable]
		private var _demographicsoption: DemographicsOption;

		public function DemographicsOptionListEvent(aType: String, aDemographicsOptionList: DemographicsOptionList, aDemographicsOption: DemographicsOption) {
			super(aType);

			demographicsOptionList = aDemographicsOptionList;

			demographicsOption = aDemographicsOption;
		}

		[Bindable]
		public function get demographicsOptionList(): DemographicsOptionList {
			return _demographicsoptionlist;
		}

		public function set demographicsOptionList(aDemographicsOptionList: DemographicsOptionList): void {
			_demographicsoptionlist = aDemographicsOptionList;
		}

		[Bindable]
		public function get demographicsOption(): DemographicsOption {
			return _demographicsoption;
		}

		public function set demographicsOption(aDemographicsOption: DemographicsOption): void {
			_demographicsoption = aDemographicsOption;
		}
	}
}
