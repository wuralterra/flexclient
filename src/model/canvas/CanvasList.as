package model.canvas
{
	import model.ModelUtils;
	import model.canvas.*;
	
	import mx.collections.ArrayCollection;
	
	public class CanvasList extends ArrayCollection
	{
		[Bindable]
		private var _canvas: Canvas;
		
		public function CanvasList() {
			super();
		}

		public static function createCanvasList(aAC: ArrayCollection): CanvasList {
			var canvaslist: CanvasList = new CanvasList();
			
			for (var i: int = 0; i < aAC.length; i ++) {
				var canvas:Canvas = Canvas.createCanvas(aAC[i]);
				
				if (canvas.themeList.numThemes() > 0) // leave out empty canvas
					canvaslist.addCanvas(canvas);
			}
			
			return canvaslist;
		}
		
		public function addCanvas(aCanvas: Canvas): void {
			addItem(aCanvas);

			aCanvas.addEventListener(CanvasEvent.CONTENTSCHANGE, handleCanvasEventContentsChange);

			var event: CanvasListEvent = new CanvasListEvent(CanvasListEvent.ADD, this, aCanvas);

			dispatchEvent(event);
		}

		public function removeCanvas(aCanvas: Canvas): void {
			var index: int = getItemIndex(aCanvas);

			if (index != -1) {
				aCanvas.removeEventListener(CanvasEvent.CONTENTSCHANGE, handleCanvasEventContentsChange);

				removeItemAt(index);

				var event: CanvasListEvent = new CanvasListEvent(CanvasListEvent.REMOVE, this, aCanvas);

				dispatchEvent(event);
			}
		}

		public function handleCanvasEventContentsChange(aEvent: CanvasEvent): void {
			var event: CanvasListEvent = new CanvasListEvent(CanvasListEvent.CONTENTSCHANGE, this, aEvent.canvas);

			dispatchEvent(event);
		}

		public function getCanvas(i: int): Canvas {
			return getItemAt(i) as Canvas;
		}

		public function numCanvasses(): int {
			return length;
		}
		
		public function findCanvasByID(aID: String): Canvas {
			var result: Canvas = null;
			
			for (var i: int = 0; i < numCanvasses(); i ++) {
				var canvas: Canvas = getCanvas(i);
				
				if (canvas.id == aID || ModelUtils.getNumberFromId(canvas.id) == aID) {
					result = canvas;
					
					break;
				}
			}
			
			return result;
		}

		[Bindable]
		public function get canvas(): Canvas {
			return _canvas;
		}
		
		public function set canvas(aCanvas: Canvas): void {
			_canvas = aCanvas;
			
			var event: CanvasListEvent = new CanvasListEvent(CanvasListEvent.CURRENTCHANGED, this, aCanvas);
			
			dispatchEvent(event);
		}
	}
}
