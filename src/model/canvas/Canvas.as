package model.canvas
{
	import flash.events.EventDispatcher;
	
	import model.ModelUtils;
	import model.background.BackgroundList;
	import model.connection.ConnectionList;
	import model.icon.IconList;
	import model.indicator.IndicatorList;
	import model.question.QuestionList;
	import model.tag.TagList;
	import model.theme.ThemeList;
	
	import mx.collections.ArrayCollection;
	
	public class Canvas extends EventDispatcher
	{
		public static const CanvasTypen: ArrayCollection = new ArrayCollection(['Individual', 'Sectoral']);

		public static const typeIndividual: int = 1;
		
		public static const typeSectoral: int = 2;
		
		public static const defaultCanvasType: int = typeIndividual;

		[Bindable]
		private var _id: String;

		[Bindable]
		private var _title: String;
		
		[Bindable]
		private var _description: String;
		
		[Bindable]
		private var _intro: String;
		
		[Bindable]
		private var _outro: String;
		
		[Bindable]
		private var _themelist: ThemeList;
		
		[Bindable]
		private var _questionlist: QuestionList;
		
		[Bindable]
		private var _indicatorlist: IndicatorList;
		
		[Bindable]
		private var _backgroundlist: BackgroundList;

		// Result
		[Bindable]
		private var _iconlist: IconList;
		
		[Bindable]
		private var _connectionlist: ConnectionList;

		[Bindable]
		private var _taglist: TagList;
		
		[Bindable]
		private var _resultindicatorlist: IndicatorList;
		
		
		public function Canvas(aID: String, aTitle: String, aDescription: String, aIntro: String, aOutro: String) {
			super();
			
			id = aID;

			title = aTitle;
			
			description = aDescription;
			
			intro = aIntro;
			
			outro = aOutro;
			
			iconList = new IconList();

			tagList = new TagList();

			connectionList = new ConnectionList();
			
			resultIndicatorList = new IndicatorList();
		}
		
		public static function createCanvas(aObject: Object): Canvas {
			var id: String = aObject.url;
			
			var title: String = aObject.title;
			
			var description: String = aObject.description;
			
			var intro: String = aObject.intro;
			
			var outro: String = aObject.outro;
			
			var canvas: Canvas = new Canvas(id, title, description, intro, outro);
			
			var arr: Array = (aObject.themes as Array);
			var ac: ArrayCollection = new ArrayCollection(arr);
			canvas.themeList = ThemeList.createThemeList(ac);

			arr = (aObject.indicators as Array);
			ac  = new ArrayCollection(arr);
			canvas.indicatorList = IndicatorList.createIndicatorList(ac);
			
			arr = (aObject.backgrounds as Array);
			ac  = new ArrayCollection(arr);
			canvas.backgroundList = BackgroundList.createBackgroundList(ac);
			
			// moet bij theme komen?
			arr = (aObject.questions as Array);
			ac  = new ArrayCollection(arr);
			canvas.questionList = QuestionList.createQuestionList(ac);
			
			return canvas;
		}

		public function createCanvasObject(aBackground: String): Object {
			var canvasobject: Object = new Object();
			
			canvasobject["canvas"] = ModelUtils.getNumberFromId(id);
			
			if (aBackground)
				canvasobject["background"] = aBackground;
			
			return canvasobject;
		}
		
		[Bindable]
		public function get id(): String {
			return _id;
		}

		public function set id(aID: String): void {
			_id = aID;

			contentsChanged();
		}

		[Bindable]
		public function get title(): String {
			return _title;
		}
		
		public function set title(aTitle: String): void {
			_title = aTitle;
			
			contentsChanged();
		}
		
		[Bindable]
		public function get description(): String {
			return _description;
		}
		
		public function set description(aDescription: String): void {
			_description = aDescription;
			
			contentsChanged();
		}
		
		[Bindable]
		public function get intro(): String {
			return _intro;
		}
		
		public function set intro(aIntro: String): void {
			_intro = aIntro;
			
			contentsChanged();
		}
		
		[Bindable]
		public function get outro(): String {
			return _outro;
		}
		
		public function set outro(aOutro: String): void {
			_outro = aOutro;
			
			contentsChanged();
		}
		
		[Bindable]
		public function get themeList(): ThemeList {
			return _themelist;
		}

		public function set themeList(aThemeList: ThemeList): void {
			//trace("set themeList", id, title, description, aThemeList.numThemes());
			_themelist = aThemeList;

			contentsChanged();
		}

		[Bindable]
		public function get indicatorList(): IndicatorList {
			return _indicatorlist;
		}
		
		public function set indicatorList(aIndicatorList: IndicatorList): void {
			//trace("set indicatorList", id, title, description, aIndicatorList.numIndicators());
			_indicatorlist = aIndicatorList;
			
			contentsChanged();
		}
		
		[Bindable]
		public function get questionList(): QuestionList {
			return _questionlist;
		}
		
		public function set questionList(aQuestionList: QuestionList): void {
			//trace("set questionList", id, title, description, aQuestionList.numQuestions());
			_questionlist = aQuestionList;
			
			contentsChanged();
		}
		
		[Bindable]
		public function get backgroundList(): BackgroundList {
			return _backgroundlist;
		}
		
		public function set backgroundList(aBackgroundList: BackgroundList): void {
			//trace("set backgroundList", id, title, description, aBackgroundList.numBackgrounds());
			_backgroundlist = aBackgroundList;
			
			contentsChanged();
		}
		
		private function contentsChanged(): void {
			var event: CanvasEvent = new CanvasEvent(CanvasEvent.CONTENTSCHANGE, this);
			
			dispatchEvent(event);
		}

		// Result
		[Bindable]
		public function get iconList(): IconList {
			return _iconlist;
		}
		
		public function set iconList(aIconList: IconList): void {
			_iconlist = aIconList;
			
			//contentsChanged();
		}

		[Bindable]
		public function get connectionList(): ConnectionList {
			return _connectionlist;
		}
		
		public function set connectionList(aConnectionList: ConnectionList): void {
			_connectionlist = aConnectionList;
			
			//contentsChanged();
		}

		[Bindable]
		public function get tagList(): TagList {
			return _taglist;
		}
		
		public function set tagList(aTagList: TagList): void {
			//trace("set tagList", id, title, description, aTagList.numTags());
			_taglist = aTagList;
			
			//contentsChanged();
		}

		[Bindable]
		public function get resultIndicatorList(): IndicatorList {
			return _resultindicatorlist;
		}
		
		public function set resultIndicatorList(aIndicatorList: IndicatorList): void {
			_resultindicatorlist = aIndicatorList;
			
			//contentsChanged();
		}
	}
}