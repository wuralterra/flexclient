package model.canvas
{
	import flash.events.Event;

	public class CanvasListEvent extends Event
	{
		public static var ADD: String = "CanvasListEvent.ADD";

		public static var REMOVE: String = "CanvasListEvent.REMOVE";

		public static var CONTENTSCHANGE: String = "CanvasListEvent.CONTENTSCHANGE";

		public static var CURRENTCHANGED: String = "CanvasListEvent.CURRENTCHANGED";
		
		[Bindable]
		private var _canvaslist: CanvasList;

		[Bindable]
		private var _canvas: Canvas;

		public function CanvasListEvent(aType: String, aCanvasList: CanvasList, aCanvas: Canvas) {
			super(aType);

			canvasList = aCanvasList;

			canvas = aCanvas;
		}

		[Bindable]
		public function get canvasList(): CanvasList {
			return _canvaslist;
		}

		public function set canvasList(aCanvasList: CanvasList): void {
			_canvaslist = aCanvasList;
		}

		[Bindable]
		public function get canvas(): Canvas {
			return _canvas;
		}

		public function set canvas(aCanvas: Canvas): void {
			_canvas = aCanvas;
		}
	}
}
