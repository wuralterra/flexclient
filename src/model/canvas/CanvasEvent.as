package model.canvas
{
	import flash.events.Event;

	public class CanvasEvent extends Event
	{
		public static var CONTENTSCHANGE: String = "CanvasEvent.CONTENTSCHANGE";

		[Bindable]
		private var _canvas: Canvas;

		public function CanvasEvent(aType: String, aCanvas: Canvas) {
			super(aType);

			canvas = aCanvas;
		}

		[Bindable]
		public function get canvas(): Canvas {
			return _canvas;
		}

		public function set canvas(aCanvas: Canvas): void {
			_canvas = aCanvas;
		}
	}
}
