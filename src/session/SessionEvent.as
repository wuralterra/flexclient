package session
{
	import flash.events.Event;

	public class SessionEvent extends Event
	{
		public static var ENABLEDCHANGED: String = "SessionEvent.ENABLEDCHANGED";
		
		public static var PROFILECHANGED: String = "SessionEvent.PROFILECHANGED";
		
		public static var PRINTCHANGED: String = "SessionEvent.PRINTCHANGED";
		
		public static var RESULTCHANGED: String = "SessionEvent.RESULTCHANGED";
		
		public static var TYPECHANGED: String = "SessionEvent.TYPECHANGED";
		
		public static var APIURLCHANGED: String = "SessionEvent.APIURLCHANGED";

		public static var EXPERIMENTCHANGED: String = "SessionEvent.EXPERIMENTCHANGED";

		public static var EXPERIMENTIDCHANGED: String = "SessionEvent.EXPERIMENTIDCHANGED";
		
		public static var RESULTEXPERIMENTCHANGED: String = "SessionEvent.RESULTEXPERIMENTCHANGED";
		
		public static var RESULTEXPERIMENTIDCHANGED: String = "SessionEvent.RESULTEXPERIMENTIDCHANGED";
		
		public static var SERVERACCESSCHANGED: String = "SessionEvent.SERVERACCESSCHANGED";

		public static var EXPERIMENTCONTENTSCHANGED: String = "SessionEvent.EXPERIMENTCONTENTSCHANGED";
		
		public static var RESULTEXPERIMENTCONTENTSCHANGED: String = "SessionEvent.RESULTEXPERIMENTCONTENTSCHANGED";
		
		public static var MODECHANGED: String = "SessionEvent.MODECHANGED";

		public static var EXPERIMENTSAVED: String = "SessionEvent.EXPERIMENTSAVED";
		
		public static var SEARCHMODECHANGED: String = "SessionEvent.SEARCHMODECHANGED";
		
		public static var SEARCHRESULTSCHANGED: String = "SessionEvent.SEARCHRESULTSCHANGED";
		
		[Bindable]
		private var _session: Session;
		
		public function SessionEvent(aType: String, aSession: Session) {
			super(aType);
			
			session = aSession;
		}
		
		[Bindable]
		public function get session(): Session {
			return _session;
		}
		
		public function set session(aSession: Session): void {
			_session = aSession;
		}
	}
}