package session
{
	import flash.events.Event;
	
	public class ServerAccessEvent extends Event
	{
		public static const COMPLETE: String = "ServerAccessEvent.COMPLETE";
		
		public static const INSERTPARCELCOMPLETE: String = "ServerAccessEvent.INSERTPARCELCOMPLETE";
		
		[Bindable]
		private var _serveraccess: ServerAccess;
		
		public function ServerAccessEvent(aType: String, aServerAccess: ServerAccess) {
			super(aType);
			
			serverAccess = aServerAccess;
		}
		
		[Bindable]
		public function get serverAccess(): ServerAccess {
			return _serveraccess;
		}
		
		public function set serverAccess(aServerAccess: ServerAccess): void {
			_serveraccess = aServerAccess;
		}
	}
}