package session
{
/*	CONFIG::Flex {
		import com.adobe.net.URI; 
	}
*/	
	import flash.display.DisplayObject;
	import flash.events.*;
	import flash.net.*;
	import flash.utils.ByteArray;
	import flash.utils.getDefinitionByName;
	import flash.utils.getQualifiedClassName;
	
	import model.Constants;
	import model.ModelUtils;
	import model.canvas.Canvas;
	import model.connection.Connection;
	import model.demographicsitem.DemographicsItem;
	import model.demographicsoption.DemographicsOption;
	import model.experiment.Experiment;
	import model.icon.Icon;
	import model.icon.IconComponent;
	import model.indicator.Indicator;
	import model.resultexperiment.ResultExperiment;
	import model.statement.Statement;
	import model.tag.Tag;
	import model.tag.TagComponent;
	import model.theme.Theme;
	
	import mx.collections.ArrayCollection;
	import mx.containers.Canvas;
	import mx.containers.ViewStack;
	import mx.controls.Alert;
	import mx.core.FlexGlobals;
	import mx.events.CloseEvent;
	import mx.managers.BrowserManager;
	import mx.managers.IBrowserManager;
	import mx.rpc.events.FaultEvent;
	import mx.rpc.events.ResultEvent;
	import mx.rpc.http.HTTPService;
	import mx.utils.URLUtil;
	
	import spark.components.TextInput;
	
	import view.components.Board2Component;
	import view.components.BoardComponent;
	import view.components.ProfileComponent;
	
	public class ServerAccess extends EventDispatcher
	{
		private var api: HTTPService;
		
		private var apidone: HTTPService;
		
		private var apisearch: HTTPService;
		
		// api names
		private static const experimentDoneApiName: String = "doneExperiment";
		private static const experimentSearchApiName: String = "search";

		// api done/search result
		private var apiDoneResult: String;
		private var apiSearch: String;
		
		[Bindable]
		private var _loading: Boolean;

		/**
		 * Constructor 
		 * 
		 */
		public function ServerAccess() {
			super();
			
			resetLoading();
		}

		private function init():void {
		}
		
		public function handleFault(event:FaultEvent):void
		{
			api.removeEventListener(FaultEvent.FAULT, handleFault);
						
			api = null;
			
			Alert.show(event.fault.faultString, "Error api");

			resetLoading();
		}
		
		public function loadExperiment(aID: String): void {
			api = new HTTPService();
			
			updateLoadingStatus();
			
			api.addEventListener(ResultEvent.RESULT, handleExperimentResult);
			api.addEventListener(FaultEvent.FAULT, handleFault);
			
			api.resultFormat = "text";
			
			if (Session.instance().languages.language != null)
				api.url = Session.instance().apiURL + aID + "/" + Session.instance().languages.language.title + ".json";
			else
				api.url = Session.instance().apiURL + aID + ".json";
			trace("apiurl:", api.url);

			api.send(); 
        }
		
		public function handleExperimentResult(event:ResultEvent):void
		{
			api.removeEventListener(ResultEvent.RESULT, handleExperimentResult);
			
			var rawData: String = String(event.result);

			trace("experiment\n================");
			trace(rawData);

			var obj: Object = JSON.parse(rawData);
			
			Session.instance().experiment = Experiment.createExperiment(obj);
			
			resetLoading();
		}

		public function loadExperimentLanguages(aID: String): void {
			api = new HTTPService();
			
			updateLoadingStatus();
			
			api.addEventListener(ResultEvent.RESULT, handleExperimentLanguagesResult);
			api.addEventListener(FaultEvent.FAULT, handleExperimentLanguagesFault);
			
			api.resultFormat = "text";
			
			api.url = Session.instance().apiURL + aID + "/available.json";
			trace("apiurl Languages:", api.url);
			
			api.send(); 
		}
		
		public function handleExperimentLanguagesResult(event:ResultEvent):void
		{
			api.removeEventListener(ResultEvent.RESULT, handleExperimentLanguagesResult);
			
			var rawData: String = String(event.result);
			
			trace("languages\n================");
			trace(rawData);
			
			var obj: Object = JSON.parse(rawData);
			var arr: Array = (obj as Array);
			var ac: ArrayCollection = new ArrayCollection(arr);
			
			if (arr.length > 0)
				resetLoading();

			Session.instance().languages.addLanguages(ac);
		}
		
		public function handleExperimentLanguagesFault(event:FaultEvent):void
		{
			api.removeEventListener(FaultEvent.FAULT, handleExperimentLanguagesFault);
			
			api = null;
			
			Alert.show(event.fault.faultString, "Error api");
			
			resetLoading();
		}
		
		public function loadResultExperiment(aID: String): void {
			api = new HTTPService();
			
			updateLoadingStatus();
			
			api.addEventListener(ResultEvent.RESULT, handleResultExperimentResult);
			api.addEventListener(FaultEvent.FAULT, handleFault);
			
			api.resultFormat = "text";
			
			api.url = Session.instance().apiURL + aID + ".json";
			
			api.send(); 
		}
		
		public function handleResultExperimentResult(event:ResultEvent):void
		{
			api.removeEventListener(ResultEvent.RESULT, handleResultExperimentResult);
			
			var rawData: String = String(event.result);
			
			trace("resultExperiment\n================");
			trace(rawData);
			
			var obj: Object = JSON.parse(rawData);
			
			Session.instance().resultExperiment = ResultExperiment.createResultExperiment(obj);

			resetLoading();
		}
		
		public function createExperimentDone(aExperiment: Experiment, aProfile: ProfileComponent, aViewStack: ViewStack): Object {
			var experimentDone: Object = aExperiment.createExperimentObject();
			
			experimentDone["moodboard"] = (Session.instance().type == Session.typeSectoral).toString();
			
			// profile
			if (aProfile) {
				experimentDone["profile"] = aProfile.createProfileObject();
			}
			
			// canvas
			var canvasses: Array = new Array();
			for (var j: int = 0; j < aExperiment.canvasList.numCanvasses(); j++) {
				var canvas: model.canvas.Canvas = aExperiment.canvasList.getCanvas(j);
				// optional background
				var canvasobject: Object = canvas.createCanvasObject((canvas.backgroundList.background)?ModelUtils.getNumberFromId(canvas.backgroundList.background.id):null);
				
				var icons: Array = new Array();
				
				if (Session.instance().type == Session.typeIndividual || Session.instance().type == Session.typeCrowdSourcing) {
					// theme-icon with notes
					for (var k: int = 0; k < canvas.themeList.numThemes(); k++) {
						var theme: Theme = canvas.themeList.getTheme(k);
						
						for (var t: int = 0; t < theme.resultIconList.numIcons(); t++) {
							var iconobj: Object= theme.resultIconList.getIcon(t).createIconObject();
							
							if (theme.notes)
								iconobj["comment"] = theme.notes;

							icons.push(iconobj);
						}
/*						if (theme.iconList.icon) {
							icons.push(theme.iconList.icon.createIconObject());
						}
*/					}
				}
				else if (Session.instance().type == Session.typeSectoral) {
					// moodboard
					// @ToDo get appropiate canvas from application VolanteMoodboard
					var diff: int = aViewStack.numChildren - aExperiment.canvasList.numCanvasses();
					
					trace(aViewStack.numChildren, aExperiment.canvasList.numCanvasses(), j, diff);
					var moveable: Boolean = false;
					var moodboard: mx.containers.Canvas;
					// find another solution to solve different versions...
					trace("application", Session.instance().application);
					if (Session.instance().application == "VolanteMoodboard" || Session.instance().application == "VisionCanvas") { 
						moodboard= (aViewStack.getChildAt(j+diff) as BoardComponent).moodboard;
						moveable = true;
					}
					else
						moodboard= (aViewStack.getChildAt(j+diff) as Board2Component).moodboard;
					
					// canvas iconlist icon with notes
					for (var l: int = 0; l < canvas.iconList.numIcons(); l++) {
						var icon:Icon = canvas.iconList.getIcon(l);
						
						var iconobject: Object = icon.createIconObject();
						
						// @ToDo get appropiate iconcomponent from BoardComponent
						for (var p: int = 0; p < moodboard.numElements; p++) {
							if (moodboard.getElementAt(p) is IconComponent) {
								var iconcomponent: IconComponent = moodboard.getElementAt(p) as IconComponent;
								
								if (iconcomponent.icon == icon) {
									iconobject["xPos"]  = iconcomponent.x / (moodboard.width-Constants.INDICATOR_WIDTH);
									iconobject["yPos"]  = iconcomponent.y / moodboard.height;
									iconobject["width"] = iconcomponent.width/(moodboard.width-Constants.INDICATOR_WIDTH);
									iconobject["height"]= iconcomponent.height/moodboard.height;
									break;
								}
							}
						}

						var indicators: Array = new Array();

						// connection with comment
						for (var m: int = 0; m < canvas.connectionList.numConnections(); m++) {
							var connection: Connection = canvas.connectionList.getConnection(m);
							
							if (connection.icon == icon) {
								var indicator:Indicator = connection.indicator;
								var itemindex:int = canvas.indicatorList.getItemIndex(indicator);
								
								// create any unique number for possible user indicator
								if (itemindex == -1) {
									itemindex = canvas.resultIndicatorList.getItemIndex(indicator);
									
									if (itemindex == -1) {
										indicators.push(indicator.createIndicatorObject("?" + (j+diff) + '-1', connection.comment));
										resultindicatorobject["type"]  = "unknown";
									}
									else
										indicators.push(indicator.createResultIndicatorObject("s" + (j+diff) + itemindex, connection.comment, moveable));
								}
								else
									indicators.push(indicator.createIndicatorObject("u" + (j+diff) + itemindex, connection.comment));
							}
						}
						
						iconobject["indicators"] = indicators;
						
						icons.push(iconobject);
					}
				}

				canvasobject["icons"] = icons;
			
				// tag/textfields
				var textfields: Array = new Array();
				
				for (var n: int = 0; n < canvas.tagList.numTags(); n++) {
					var tag: Tag = canvas.tagList.getTag(n);
					var tagobject: Object = tag.createTagObject();
						
					// @ToDo get appropiate tagcomponent from BoardComponent
					for (var o: int = 0; o < moodboard.numElements; o++) {
						if (moodboard.getElementAt(o) is TagComponent) {
							var tagcomponent: TagComponent = moodboard.getElementAt(o) as TagComponent;
							
							if (tagcomponent.tag == tag) {
								tagobject["xPos"]  = tagcomponent.x / (moodboard.width-Constants.INDICATOR_WIDTH);
								tagobject["yPos"]  = tagcomponent.y / moodboard.height;
								break;
							}
						}
					}
					
					textfields.push(tagobject);
				}
				
				canvasobject["textfields"] = textfields;

				// user indicators
				var userindicators: Array = new Array();
				var userindicator: Indicator;
				var userindicatorid: String;
				
				for (var q: int = 0; q < canvas.indicatorList.numIndicators(); q++) {
					userindicator  = canvas.indicatorList.getIndicator(q);
					userindicatorid= ModelUtils.getNumberFromId(userindicator.id);
					
					// is it a user indicator
					if (userindicatorid == "0") {
						// create same unique number as before
						userindicators.push(userindicator.createUserIndicatorObject("u" + (j+diff) + q));
					}
				}
				
				for (var r: int = 0; r < canvas.resultIndicatorList.numIndicators(); r++) {
					userindicator  = canvas.resultIndicatorList.getIndicator(r);
					userindicatorid= ModelUtils.getNumberFromId(userindicator.id);
					
					// is it a user indicator?
					if (userindicatorid == "0" && userindicator.title != "") {
						// create same unique number as before
						var userindicatorobject: Object = userindicator.createUserIndicatorObject("s" + (j+diff) + r); 
						userindicatorobject["xPos"]  = userindicator.x / (moodboard.width-Constants.INDICATOR_WIDTH);
						userindicatorobject["yPos"]  = userindicator.y / moodboard.height;
						userindicators.push(userindicatorobject);
					}
				}
				
				// try saving stored indicators as user indicators?
				for (var s: int = 0; s < canvas.resultIndicatorList.numIndicators(); s++) {
					var resultindicator: Indicator = canvas.resultIndicatorList.getIndicator(s);
					var resultindicatorid: String = ModelUtils.getNumberFromId(resultindicator.id);
					
					// is it not a user indicator
					if (resultindicatorid != "0") {
						// create same unique number as before
						var resultindicatorobject: Object = new Object();
						
						// @ToDo wait for change Dave, also createResultIndicatorObject
						resultindicatorobject["id"]         = "s" + (j+diff) + s; 
						resultindicatorobject["title"]      = resultindicator.title;
						resultindicatorobject["description"]= resultindicator.description;
						//resultindicatorobject["indicator"]  = resultindicatorid;
						resultindicatorobject["xPos"]       = resultindicator.x / (moodboard.width-Constants.INDICATOR_WIDTH);
						resultindicatorobject["yPos"]       = resultindicator.y / moodboard.height;
						userindicators.push(resultindicatorobject);
					}
				}

				canvasobject["userIndicators"] = userindicators;
				
				// stored indicators
/*				var storedindicators: Array = new Array();
				
				for (var s: int = 0; s < canvas.resultIndicatorList.numIndicators(); s++) {
					var resultindicator: Indicator = canvas.resultIndicatorList.getIndicator(s);
					var resultindicatorid: String = ModelUtils.getNumberFromId(resultindicator.id);
					
					// is it not a user indicator
					if (resultindicatorid != "0") {
						// create same unique number as before
						var resultindicatorobject: Object = new Object();
						
						// @ToDo wait for change Dave, also createResultIndicatorObject
						//resultindicatorobject["id"]       = "s" + (j+diff) + s; 
						resultindicatorobject["indicator"]= resultindicatorid;
						resultindicatorobject["xPos"]     = resultindicator.x / (moodboard.width-Constants.INDICATOR_WIDTH);
						resultindicatorobject["yPos"]     = resultindicator.y / moodboard.height;
						storedindicators.push(resultindicatorobject);
					}
				}
				
				canvasobject["storedIndicators"] = storedindicators;
*/				
				canvasses.push(canvasobject);
			}
			
			experimentDone["canvasses"] = canvasses;

			if (Session.instance().application == "VolanteCrowdSourcing") { 
				/*"demographics": {
				"items": [ 
				{ "item_id":"3", "option_id":"8" },
				{ "item_id":"4", "option_id":"12" } 
				],
				"lat": "40.5",
				"lon": "30.2"
				},*/
				var demographics: Object = new Object();
				var items: Array = new Array();
				for (var d:int = 0; d < aExperiment.demographics.demographicsItemList.numDemographicsItems(); d++) {
					var item: DemographicsItem= aExperiment.demographics.demographicsItemList.getDemographicsItem(d);
					if (item.demographicsOptionList.numDemographicsOptions() > 0) {
						var option: DemographicsOption = item.demographicsOptionList.demographicsOption;
					
						if (option != null) {
							var itemobject: Object = new Object();
						
							itemobject["item_id"]  = ModelUtils.getNumberFromId(item.id); 
							itemobject["option_id"]= ModelUtils.getNumberFromId(option.id);
							items.push(itemobject);
						}
					}
					else // map
						if (item.lat !="" && item.lon != "") {
							demographics["lat"] = item.lat;
							demographics["lon"] = item.lon;
						}
				}
				
				demographics["items"] = items;
				experimentDone["demographics"] = demographics;
				
			/*"statements" : [
				{ "statement" : "3", "response": "1" },
				{ "statement" : "4", "response": "0" }
			],*/
				var statements: Array = new Array();
				var statement: Statement;
				var statementobject: Object;
				for (var w:int = 0; w < aExperiment.resultStatements.numStatements(); w++) {
					statement      = aExperiment.resultStatements.getStatement(w);
					statementobject= new Object();
					
					statementobject["statement"]= ModelUtils.getNumberFromId(statement.id); 
					statementobject["response"] = (1 + w).toString();
					statements.push(statementobject);
				}
				for (var v:int = 0; v < aExperiment.resultStatements2.numStatements(); v++) {
					statement      = aExperiment.resultStatements2.getStatement(v);
					statementobject= new Object();
					
					statementobject["statement"]= ModelUtils.getNumberFromId(statement.id); 
					statementobject["response"] = (-1 - v).toString();
					statements.push(statementobject);
				}
				for (var u:int = 0; u < aExperiment.statements.numStatements(); u++) {
					statement      = aExperiment.statements.getStatement(u);
					statementobject= new Object();
					
					statementobject["statement"]= ModelUtils.getNumberFromId(statement.id); 
					statementobject["response"] = "0";
					statements.push(statementobject);
				}
				experimentDone["statements"] = statements;
			}
			
			return experimentDone;
		}
		
		public function sendExperimentDone(aExperimentDone: Object): void {
			apiDoneResult = JSON.stringify(aExperimentDone);
			
			trace("experimentResult", apiDoneResult.toString(), aExperimentDone.experiment);
			
			apidone = new HTTPService();
			
			apidone.resultFormat = "text";
			apidone.addEventListener(ResultEvent.RESULT, handleDoneExperimentResult);
			apidone.addEventListener(FaultEvent.FAULT, handleDoneFault);
			apidone.url = Session.instance().apiURL + "api/" + experimentDoneApiName + "/" + aExperimentDone.experiment + ".json";
			trace("apiDone url", apidone.url);
			
			apidone.method = "POST";
			apidone.contentType= "text/json";
			apidone.request = apiDoneResult;
			apidone.send();

			updateLoadingStatus();

/*	WORKING PUT SOLUTION -> is for update, POST for create
			var client:HttpClient = new HttpClient();
			client.addEventListener("COMPLETE", handleComplete);
			client.listener.onStatus = function(event:HttpStatusEvent):void {
				// Notified of response (with headers but not content)
			};
			
			client.listener.onData = function(event:HttpDataEvent):void {
				// For string data
				var stringData:String = event.readUTFBytes();
				trace("HttpDataEvent s", stringData);
				
				// For data
				var data:ByteArray = event.bytes;    
				trace("HttpDataEvent b", data);
			};
		
			client.listener.onComplete = function(event:HttpResponseEvent):void {
				// Notified when complete (after status and data)
				trace("HttpResponseEvent", event);
			};
			
			client.listener.onError = function(event:ErrorEvent):void {
				var errorMessage:String = event.text;
				trace("ErrorEvent", event.text);
			};      
			var uri:URI= new URI(Session.instance().apiURL + "/api/" + experimentDoneApiName + "/" + getNumberFromId(aExperiment.id) + ".json");
			var jsonData:ByteArray = new ByteArray();
			jsonData.writeUTFBytes(result);
			jsonData.position = 0;
			client.put(uri, jsonData, "text/json");
*/			
		}
		
		public function handleDoneFault(event:FaultEvent):void {
			apidone.removeEventListener(FaultEvent.FAULT, handleDoneFault);
			
			resetLoading();
			
			Alert.show(event.fault.faultString + ". You can manually copy the JSON result:\n" + apiDoneResult, "Error apidone");
		}
		
		public function handleDoneExperimentResult(event:ResultEvent):void
		{
			apidone.removeEventListener(ResultEvent.RESULT, handleDoneExperimentResult);
			
			var rawData: String = String(event.result);
			
			trace("handleDoneExperimentResult\n================");
			trace(rawData);
			
			var obj: Object = JSON.parse(rawData);
			
			if (obj.hasOwnProperty("url")) {
				if (Session.instance().type == Session.typeCrowdSourcing) {
					if (Session.instance().apiURL == "" || Session.instance().apiURL.charAt() == '/') {
						var browser:IBrowserManager = BrowserManager.getInstance(); 
						browser.init();
						var base: String  = browser.base;			// before #
						
						Session.instance().posterURL = URLUtil.getProtocol(base) + "://" + URLUtil.getServerName(base) + '/poster/' + ModelUtils.getNumberFromId(String(obj.url)) + '/MyPoster.pdf';
					}
					else
						Session.instance().posterURL = Session.instance().apiURL + 'poster/' + ModelUtils.getNumberFromId(String(obj.url)) + '/MyPoster.pdf';
				}
				else // no alert for Crowd Sourcing
					Alert.show("Experiment saved as result: " + ModelUtils.getNumberFromId(String(obj.url)), "Success", 4, null, alertClickHandler);
				
				Session.instance().experimentSaved = true;
			}
			else
				Alert.show("Save of Experiment failed: " + rawData + "\You can manually copy the JSON result:\n" + apiDoneResult, "Failure apidone");
			
			resetLoading();
		}

		private function alertClickHandler(event: CloseEvent):void {
			if (event.detail==Alert.NO) {
				Session.closeWindow();
			}
		}

		public function sendSearchExperiment(aExperimentSearch: Object): void {
			apiSearch = JSON.stringify(aExperimentSearch);
			
			trace("experimentSearch", apiSearch.toString(), Session.instance().experimentId);
			
			apisearch = new HTTPService();
			
			apisearch.resultFormat = "text";
			apisearch.addEventListener(ResultEvent.RESULT, handleSearchExperimentResult);
			apisearch.addEventListener(FaultEvent.FAULT, handleSearchFault);
			apisearch.url = Session.instance().apiURL + "api/" + experimentSearchApiName + "/" + ModelUtils.getNumberFromId(Session.instance().experimentId) + ".json";
			trace("apiSearch url", apisearch.url);
			
			apisearch.method = "POST";
			apisearch.contentType= "text/json";
			apisearch.request = apiSearch;
			apisearch.send();
			
			updateLoadingStatus();
		}
		
		public function handleSearchFault(event:FaultEvent):void {
			apisearch.removeEventListener(FaultEvent.FAULT, handleSearchFault);
			
			resetLoading();
			
			Alert.show(event.fault.faultString + ". Caused by JSON. You can manually copy the JSON:\n" + apiSearch, "Error apisearch");
		}
		
		public function handleSearchExperimentResult(event:ResultEvent):void
		{
			apisearch.removeEventListener(ResultEvent.RESULT, handleSearchExperimentResult);
			
			var rawData: String = String(event.result);
			
			trace("handleSearchExperimentResult\n================");
			trace(rawData);
			
			Session.instance().searchResults = JSON.parse(rawData);
			
			resetLoading();
		}
		
/*		private function configureListeners(dispatcher:IEventDispatcher):void {
			dispatcher.addEventListener(Event.COMPLETE, completeHandler);
			dispatcher.addEventListener(Event.OPEN, openHandler);
			dispatcher.addEventListener(ProgressEvent.PROGRESS, progressHandler);
			dispatcher.addEventListener(SecurityErrorEvent.SECURITY_ERROR, securityErrorHandler);
			dispatcher.addEventListener(HTTPStatusEvent.HTTP_STATUS, httpStatusHandler);
			dispatcher.addEventListener(IOErrorEvent.IO_ERROR, ioErrorHandler);
		}
		
		private function handleComplete(event:HttpResponseEvent):void {
			trace("handleComplete: " + event);
		}
		
		private function completeHandler(event:Event):void {
			var loader:URLLoader = URLLoader(event.target);
			trace("completeHandler: " + loader.data);
		}
		
		private function openHandler(event:Event):void {
			trace("openHandler: " + event);
		}
		
		private function progressHandler(event:ProgressEvent):void {
			trace("progressHandler loaded:" + event.bytesLoaded + " total: " + event.bytesTotal);
		}
		
		private function securityErrorHandler(event:SecurityErrorEvent):void {
			trace("securityErrorHandler: " + event);
		}
		
		private function httpStatusHandler(event:HTTPStatusEvent):void {
			trace("httpStatusHandler: " + event);
		}
		
		private function ioErrorHandler(event:IOErrorEvent):void {
			trace("ioErrorHandler: " + event);
		}
*/		
		private function handleServiceException(): void {
			Session.instance().messages.addItem("ServiceException");
			
			resetLoading();
		}
			
		private function resetLoading(): void {
			loading = false;
		}
		
		private function updateLoadingStatus(): void {
			loading = true;
		}
		
		[Bindable]
		public function get loading(): Boolean {
			return _loading;
		}
		
		public function set loading(aLoading: Boolean): void {
			_loading = aLoading;  
		}
	}
}