package session
{
	import flash.events.EventDispatcher;
	import flash.external.ExternalInterface;
	
	import flashx.textLayout.conversion.TextConverter;
	import flashx.textLayout.elements.TextFlow;
	
	import model.ModelUtils;
	import model.canvas.Canvas;
	import model.canvas.CanvasList;
	import model.experiment.Experiment;
	import model.experiment.ExperimentEvent;
	import model.icon.Icon;
	import model.icon.IconList;
	import model.language.Language;
	import model.language.LanguageList;
	import model.language.LanguageListEvent;
	import model.resultexperiment.ResultExperiment;
	import model.resultexperiment.ResultExperimentEvent;
	import model.textfragment.TextFragment;
	import model.theme.Theme;
	import model.theme.ThemeList;
	
	import mx.collections.ArrayCollection;
	import mx.controls.Alert;
	import mx.events.CloseEvent;
	import mx.events.CollectionEvent;
	import mx.managers.BrowserManager;
	import mx.managers.IBrowserManager;
	import mx.rpc.events.FaultEvent;
	import mx.rpc.events.ResultEvent;
	import mx.rpc.http.HTTPService;
	import mx.utils.ObjectUtil;
	import mx.utils.StringUtil;
	import mx.utils.URLUtil;
	
	import session.*;
	
	import spark.components.Application;
	import spark.core.ContentCache;
	
	import view.components.ProfileComponent;

	public class Session extends EventDispatcher
	{
		// Instance variable
		private static var _session: Session;
		
		// Configuration filename
		private static const _configurationfilename: String = "assets/conf/local_configuration.json";

		[Bindable]
		private var _configurationurl: String;
		
		[Bindable]
		private var _mode: int;
		
		[Bindable]
		private var _typepages: int = 0;
		
		[Bindable]
		private var _apiurl: String;
		
		[Bindable]
		private var _assetsurl: String;
		
		[Bindable]
		private var _experimentid: String;
		
		[Bindable]
		private var _resultexperimentid: String;

		[Bindable]
		private var _application: String;
		
		[Bindable]
		public var cache: ContentCache = new ContentCache();
		
		[Bindable]
		public var profilecomponent: ProfileComponent;
		
		[Bindable]
		private var _type: int;
		
		// Type constants
		public static const typeNone: int = -1;
		
		public static const typeIndividual: int = 0;

		public static const typeSectoral: int = 1;
		
		public static const typeCrowdSourcing: int = 2;
		
		[Bindable]
		private var _searchmode: int;
		
		// Type constants
		public static const searchYou: int = 0;
		
		public static const searchChoices: int = 1;
		
		[Bindable]
		private var _searchresults: Object;
		
		[Bindable]
		private var _experiment: Experiment;

		[Bindable]
		private var _resultexperiment: ResultExperiment;
		
		[Bindable]
		private var _profile: Boolean;
		
		[Bindable]
		private var _print: Boolean;
		
		[Bindable]
		private var _result: Boolean;
		
		[Bindable]
		private var _enabled: Boolean;
		
		[Bindable]
		private var _serveracces: ServerAccess;
		
		[Bindable]
		private var _messages: ArrayCollection;
		
		[Bindable]
		private var _printing: Boolean;
		
		[Bindable]
		public var notPrinting: Boolean;
		
		[Bindable]
		public var languageSelected: Boolean;
		
		[Bindable]
		public var selectLanguage: Boolean;
		
		[Bindable]
		public var welcomeShown: Boolean;
		
		[Bindable]
		public var languages: LanguageList;
		
		[Bindable]
		private var _experimentsaved: Boolean;
		
		[Bindable]
		public var posterURL: String = "";
		
		public function Session() {
			super();
			
			messages = new ArrayCollection();
			
			serverAccess = new ServerAccess();
			
			type = typeNone;
			
			mode = -1;
			
			maxMode = -1;
			
			profile = true;
			
			printing = false;
			
			notPrinting = true;
			
			result = false;
			
			welcomeShown = false;
			
			_experimentsaved = false;

			languageSelected = false;
			
			selectLanguage = false;
			
			languages = new LanguageList();
			languages.addLanguage(new Language("EN", "English"));
			
			// configure the image cache
			//cache.enableQueueing = true;
			//cache.maxActiveRequests = 2;
			cache.maxCacheEntries = 200;
		}

		public static function instance(): Session {
			if (_session == null) {
				_session = new Session();
			}

			return _session;
		}

		public static function closeWindow():void {
			if (ExternalInterface.available) {
				ExternalInterface.call('function ExitApp(){window.opener=null; window.close();}');
			}
		}
		
		public function getTextFragment(aKey:String):String {
			var result:String = "";
			var textf:TextFragment = null;
			
			if (experiment != null && experiment.textFragments != null) {
				textf = experiment.textFragments.findTextFragmentByTitle(aKey);
			}
			
			if (textf != null) {
				if (textf.description != null && textf.description != "") 
					result = textf.description;
				else
					result = "empty textFragment found for key: " + aKey;
			}
			else if (aKey == "language") {
				result = "Seleccione su idioma      Kies je taal\n\nWähl deine Sprache      ???? ?? ?????      Choisis ta langue";
			}
			else {
				result = "textFragment not found for key: " + aKey;
			}
			
			return result;
		}
		public function getTextFragmentAsTextFlow(aKey:String):TextFlow {
			return TextConverter.importToFlow(getTextFragment(aKey).replace(/\n/g,"<br/>"), TextConverter.TEXT_FIELD_HTML_FORMAT);
		}
		public function isTextFragment(aKey:String):Boolean {
			return (getTextFragment(aKey).indexOf("textFragment") == -1);	// empty and absent textFragments will result in false
		}
		
		public function parseBrowserURL():Boolean
		{
			var browser:IBrowserManager = BrowserManager.getInstance(); 
			browser.init();
			
			var base: String  = browser.base;			// before #
			var fragment:String= browser.fragment;		// after #, empty or (only) a number
			
			//if (URLUtil.getProtocol(base) == "file") // local run
				//fragment= "experiment=13&profile=false"
				//fragment= "experiment=339&result=true&profile=false"
			
			trace("parseBrowserURL fragment", fragment, "title", title, base);
			if (fragment == "" || !isNaN(parseInt(fragment)))
				return false;

			var title: String = browser.title;
			
			if (URLUtil.getProtocol(base) == "file")	// local run
				this.assetsURL = "http://test.volante.cloudbees.net"; 
			else
				this.assetsURL = URLUtil.getProtocol(base) + "://" + URLUtil.getServerName(base);
				
			var obj: Object = URLUtil.stringToObject(fragment, "&");
			this.apiURL = this.assetsURL + "/";

			if (obj.hasOwnProperty("language")) {
				this.selectLanguage = obj.language;

				//if (!selectLanguage)					// behave as if selected
				//	Session.instance().languageSelected = true;

				if (selectLanguage)
					languages.addEventListener(LanguageListEvent.CURRENTCHANGED, handleLanguageListEventCurrentChanged);
				else if (type == Session.typeCrowdSourcing)
					languages.language = languages.getLanguage(0);
			}
			trace(selectLanguage, languageSelected);

			if (obj.hasOwnProperty("profile"))
				this.profile = obj.profile;
			
			if (obj.hasOwnProperty("print"))
				this.print = obj.print;
					
			if (obj.hasOwnProperty("result"))
				this.result = obj.result;
						
			if (this.result) {
				if (obj.hasOwnProperty("experiment"))
					this.resultExperimentId = "/api/resultexperiment/" + obj.experiment;
				else
					this.resultExperimentId = "dummy";	// force fault
			}
			else if (obj.hasOwnProperty("experiment"))
				this.experimentId = "/api/experiment/" + obj.experiment;
			else
				this.experimentId = "dummy";			// force fault
			
			return true;
		}
		
		private function loadConfiguration(aURL: String): void {
			var httpservice: HTTPService = new HTTPService();

			httpservice.addEventListener(ResultEvent.RESULT, handleConfigurationResult);
			
			httpservice.addEventListener(FaultEvent.FAULT, handleFault);
			
			httpservice.resultFormat = "text";
			
			//httpservice.useProxy = false;

			httpservice.url = aURL;
			trace("configurl:", httpservice.url);
			
			httpservice.send(); 
		}
		
		public function handleFault(event: FaultEvent):void {
			Alert.show(event.fault.faultString, "Error config");
		}
		
		public function handleConfigurationResult(event:ResultEvent):void
		{
			//event.removeEventListener(ResultEvent.RESULT, handleConfigurationResult);
			
			var rawData: String = String(event.result);
			try
			{	var obj: Object = JSON.parse(rawData);
			} catch (err:Error)
			{	// code to react to the error
				Alert.show("JSON parse error", "Error", 3, null, alertClickHandler);

				obj={}
			} finally
			{	// Code that runs whether or not an error was thrown. This code can clean
				// up after the error, or take steps to keep the application running.
			}
			
			var arr: Array = (obj as Array);
			if (arr == null) {
				// object configuration > single item
				arr = [];

				if (this.type != obj.type)
					Alert.show("Configured type doesn't match with application type, continue? Yes to continue. No to exit.", "Note", 3, null, alertClickHandler);
				
				this.apiURL = obj.apiurl;
		
				this.assetsURL = obj.assetsurl;
				
				this.profile = obj.profile == "true";

				this.selectLanguage = obj.language == "true";
				if (selectLanguage)
					languages.addEventListener(LanguageListEvent.CURRENTCHANGED, handleLanguageListEventCurrentChanged);
				else if (type == Session.typeCrowdSourcing)
					languages.language = languages.getLanguage(0);
				trace(selectLanguage, languageSelected);

				this.print = obj.print == "true";
				
				this.result = obj.result == "true";
				
				if (this.result) {
					var browser:IBrowserManager = BrowserManager.getInstance(); 
					browser.init();
					var fragment:String = browser.fragment;	// after #, also base before # and
					
					if (fragment != "" && !isNaN(parseInt(fragment)))
						this.resultExperimentId = "/api/resultexperiment/" + fragment;
					else if (obj.hasOwnProperty("resultexperiment"))
						this.resultExperimentId = obj.resultexperiment;
					else
						this.resultExperimentId = "dummy";	// force fault
				}
				else if (obj.hasOwnProperty("experiment"))
					this.experimentId = obj.experiment;
				else
					this.experimentId = "dummy";			// force fault
			}
		}
		
		private function handleLanguageListEventCurrentChanged(event: LanguageListEvent): void {
			if (event.language != null) {
				Session.instance().languageSelected = true;
				
				Session.instance().serverAccess.loadExperiment(experimentId);
			}
		}
		
		private function alertClickHandler(event: CloseEvent):void {
			if (event.detail==Alert.NO) {
				closeWindow();
			}
		}
		
		[Bindable]
		public function get configurationURL(): String {
			return _configurationurl;
		}
		
		public function set configurationURL(aConfigurationURL: String): void {
			_configurationurl = aConfigurationURL;
			
			if (_configurationurl != "")
				loadConfiguration(_configurationurl);
			else
				loadConfiguration(_configurationfilename);
		}
		
		[Bindable]
		public function get application(): String {
			return _application;
		}
		
		public function set application(aName: String): void {
			_application = aName;
		}
		
		[Bindable]
		public function get type(): int {
			return _type;
		}
		
		public function set type(aType: int): void {
			if (_type != aType) {
				_type = aType;
				
				var eventobj: SessionEvent = new SessionEvent(SessionEvent.TYPECHANGED, this);
				
				dispatchEvent(eventobj);
				
				typeName = aType.toString();
			}
		}
		
		[Bindable]
		public function get typeName(): String {
			switch (type)
			{
				case Session.typeSectoral:   return "sectoral ";
				break;
				case Session.typeIndividual: return "indivual ";
				break;
				case Session.typeCrowdSourcing: return "crowd sourcing ";
				break;
			}

			return "";
		}
		
		public function set typeName(aName: String): void {
		}
		
		[Bindable]
		public function get typePages(): int {
			return _typepages;
		}
		
		public function set typePages(aPages: int): void {
			_typepages = aPages;
		}
		
		[Bindable]
		public function get searchMode(): int {
			return _searchmode;
		}
		
		public function set searchMode(aMode: int): void {
			if (_searchmode != aMode) {
				_searchmode = aMode;
				
				var eventobj: SessionEvent = new SessionEvent(SessionEvent.SEARCHMODECHANGED, this);
				
				dispatchEvent(eventobj);
			}
		}
		
		[Bindable]
		public function get searchResults(): Object {
			return _searchresults;
		}
		
		public function set searchResults(aSearchResults: Object): void {
			if (_experiment != aSearchResults) {
				_searchresults = aSearchResults;
				
				var eventobj: SessionEvent = new SessionEvent(SessionEvent.SEARCHRESULTSCHANGED, this);
				
				dispatchEvent(eventobj);
			}
		}
		
		[Bindable]
		public function get apiURL(): String {
			return _apiurl;
		}
		
		public function set apiURL(aApiURL: String): void {
			_apiurl = aApiURL;
		}

		[Bindable]
		public function get assetsURL(): String {
			return _assetsurl;
		}
		
		public function set assetsURL(aAssetsURL: String): void {
			_assetsurl = aAssetsURL;
		}
		
		[Bindable]
		public function get experimentId(): String {
			return _experimentid;
		}
		
		public function set experimentId(aExperimentId: String): void {
			if (_experimentid != aExperimentId) {
				_experimentid = aExperimentId;
				
				var eventobj: SessionEvent = new SessionEvent(SessionEvent.EXPERIMENTIDCHANGED, this);
				
				dispatchEvent(eventobj);
				trace(selectLanguage, languageSelected);
				if (selectLanguage == languageSelected)
					Session.instance().serverAccess.loadExperiment(experimentId);
				else
					Session.instance().serverAccess.loadExperimentLanguages(experimentId);
			}
		}
		
		[Bindable]
		public function get resultExperimentId(): String {
			return _resultexperimentid;
		}
		
		public function set resultExperimentId(aResultExperimentId: String): void {
			if (_resultexperimentid != aResultExperimentId) {
				_resultexperimentid = aResultExperimentId;
				if (Session.instance().apiURL == "" || Session.instance().apiURL.charAt() == '/') {
					var browser:IBrowserManager = BrowserManager.getInstance(); 
					browser.init();
					var base: String  = browser.base;			// before #
					
					posterURL = URLUtil.getProtocol(base) + "://" + URLUtil.getServerName(base) + '/poster/' + ModelUtils.getNumberFromId(aResultExperimentId) + '/MyPoster.pdf';
				}
				else
					posterURL = Session.instance().apiURL + 'poster/' + ModelUtils.getNumberFromId(aResultExperimentId) + '/MyPoster.pdf';

				var eventobj: SessionEvent = new SessionEvent(SessionEvent.RESULTEXPERIMENTIDCHANGED, this);
				
				dispatchEvent(eventobj);
				
				Session.instance().serverAccess.loadResultExperiment(resultExperimentId);
			}
		}
		
		[Bindable]
		public function get experiment(): Experiment {
			return _experiment;
		}
		
		public function set experiment(aExperiment: Experiment): void {
			if (_experiment != aExperiment) {
				if (_experiment != null) {
					_experiment.removeEventListener(ExperimentEvent.CONTENTSCHANGE, handleExperimentEventContentsChange);
				}
				
				_experiment = aExperiment;
				
				if (_experiment != null) {
					_experiment.addEventListener(ExperimentEvent.CONTENTSCHANGE, handleExperimentEventContentsChange);
				}

				var eventobj: SessionEvent = new SessionEvent(SessionEvent.EXPERIMENTCHANGED, this);
				
				dispatchEvent(eventobj);
			}
		}
		
		[Bindable]
		public function get resultExperiment(): ResultExperiment {
			return _resultexperiment;
		}
		
		public function set resultExperiment(aResultExperiment: ResultExperiment): void {
			if (_resultexperiment != aResultExperiment) {
				if (_resultexperiment != null) {
					_resultexperiment.removeEventListener(ResultExperimentEvent.CONTENTSCHANGE, handleResultExperimentEventContentsChange);
				}
				
				_resultexperiment = aResultExperiment;
				
				if (_resultexperiment != null) {
					this.experimentId = "/api/experiment/" + _resultexperiment.experiment;
					
					_resultexperiment.addEventListener(ResultExperimentEvent.CONTENTSCHANGE, handleResultExperimentEventContentsChange);
				}
				
				var eventobj: SessionEvent = new SessionEvent(SessionEvent.RESULTEXPERIMENTCHANGED, this);
				
				dispatchEvent(eventobj);
			}
		}
		
		[Bindable]
		public function get mode(): int {
			return _mode;
		}
		
		public function set mode(aMode: int): void {
			if (_mode != aMode) {
				_mode = aMode;
				
				trace("set mode", aMode);
				
				var eventobj: SessionEvent = new SessionEvent(SessionEvent.MODECHANGED, this);
				
				dispatchEvent(eventobj);
			}
		}
		
		[Bindable]
		public function get maxMode(): int {
			var result: int = Number(profile);
			result += typePages;
			
			if (experiment != null) {
				if (experiment.canvasList != null) {
					result += experiment.canvasList.numCanvasses()-1;
				}
			}
			
			return result;
		}
		
		public function set maxMode(value:int): void {
			trace("set maxMode", value);
			
			value = value;
		}
		
		private function handleExperimentEventContentsChange(event: ExperimentEvent): void {
			var eventobj: SessionEvent = new SessionEvent(SessionEvent.EXPERIMENTCONTENTSCHANGED, this);
			
			dispatchEvent(eventobj);
		}
		
		private function handleResultExperimentEventContentsChange(event: ResultExperimentEvent): void {
			var eventobj: SessionEvent = new SessionEvent(SessionEvent.RESULTEXPERIMENTCONTENTSCHANGED, this);
			
			dispatchEvent(eventobj);
		}
		
		[Bindable]
		public function get profile(): Boolean {
			return _profile;
		}
		
		public function set profile(aProfile: Boolean): void {
			_profile = aProfile;
			
			var eventobj: SessionEvent = new SessionEvent(SessionEvent.PROFILECHANGED, this);
			
			dispatchEvent(eventobj);
		}
		
		[Bindable]
		public function get print(): Boolean {
			return _print;
		}
		
		public function set print(aPrint: Boolean): void {
			_print = aPrint;
			
			var eventobj: SessionEvent = new SessionEvent(SessionEvent.PRINTCHANGED, this);
			
			dispatchEvent(eventobj);
		}
		
		[Bindable]
		public function get printing(): Boolean {
			return _printing;
		}
		
		public function set printing(aPrinting: Boolean): void {
			_printing = aPrinting;
			
			notPrinting = !aPrinting;
		}
		
		[Bindable]
		public function get result(): Boolean {
			return _result;
		}
		
		public function set result(aResult: Boolean): void {
			_result = aResult;
			
			var eventobj: SessionEvent = new SessionEvent(SessionEvent.RESULTCHANGED, this);
			
			dispatchEvent(eventobj);
		}
		
		[Bindable]
		public function get experimentSaved(): Boolean {
			return _experimentsaved;
		}
		
		public function set experimentSaved(aExperimentSaved: Boolean): void {
			if (_experimentsaved != aExperimentSaved) {
				_experimentsaved = aExperimentSaved;
				
				var eventobj: SessionEvent = new SessionEvent(SessionEvent.EXPERIMENTSAVED, this);
				
				dispatchEvent(eventobj);
			}
		}
		
		[Bindable]
		public function get enabled(): Boolean {
			return _enabled;
		}
		
		public function set enabled(aEnabled: Boolean): void {
			_enabled = aEnabled;
			
			var eventobj: SessionEvent = new SessionEvent(SessionEvent.ENABLEDCHANGED, this);
			
			dispatchEvent(eventobj);
		}
		
		private function checkEnabled(): void {
			enabled = true;
				//serverAccess.api != null;
		}
		
		[Bindable]
		public function get serverAccess(): ServerAccess {
			return _serveracces;
		}
		
		public function set serverAccess(aServerAccess: ServerAccess): void {
			_serveracces = aServerAccess;
			
			var eventobj: SessionEvent = new SessionEvent(SessionEvent.SERVERACCESSCHANGED, this);
			
			dispatchEvent(eventobj);
		}
		
		[Bindable]
		public function get messages(): ArrayCollection {
			return _messages;
		}
		
		public function set messages(aMessages: ArrayCollection): void {
			_messages = aMessages;
		}
	}
}