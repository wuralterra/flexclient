///////////////////////////////////////////////////////////////////////////////
//
// Button Scroll Lite Componenet 
// Uses tweening for smooth scrolling
//
// Loosely based on ButtonScrollingCanvas by Doug McCune
//
// Code by FlexWiz (http://flexwiz.amosl.com),  January 2010
//
///////////////////////////////////////////////////////////////////////////////
package containers
{
    import flash.display.Bitmap;
    import flash.display.DisplayObject;
    import flash.events.Event;
    import flash.events.MouseEvent;
    
	import mx.controls.Alert;

	import gs.TweenLite;
    import gs.easing.Quad;
    
    import mx.containers.Canvas;
    import spark.components.Button;
    import mx.controls.Image;
    import mx.core.ScrollPolicy;
	import mx.core.FlexGlobals;
	
    import mx.styles.CSSStyleDeclaration;
    
    //-----------------------------
    // Style Excludes
    
     [Exclude(name="horizontalLineScrollSize",kind="property")]
     [Exclude(name="horizontalPageScrollSize",kind="property")]
     [Exclude(name="horizontalScrollBar",kind="property")]
     [Exclude(name="horizontalScrollBarStyleName",kind="style")]
     [Exclude(name="horizontalScrollPolicy",kind="property")]
     [Exclude(name="horizontalScrollPosition",kind="property")]
     
     [Exclude(name="verticalLineScrollSize",kind="property")]
     [Exclude(name="verticalPageScrollSize",kind="property")]
     [Exclude(name="verticalScrollBar",kind="property")]
     [Exclude(name="verticalScrollBarStyleName",kind="style")]
     [Exclude(name="verticalScrollPolicy",kind="property")]
     [Exclude(name="verticalScrollPosition",kind="property")]
    
    //----------------------------------
    //
    // Button Styles
    
     [Style(name="upButtonStyleName",inherit="no",type="String")]
     [Style(name="downButtonStyleName",inherit="no",type="String")]


    public class ButtonScrollLite extends Canvas
    {
        private static const CLASS_NAME:String = "ButtonScrollLite"
        
        // Default Button Width
        private static const DEFAULT_BUTTON_WIDTH:Number= 16;
        private static const DEFAULT_BUTTON_HEIGHT:Number= 8;
         
        // Buttons that are used for scrolling
        private var upButton:Button;
        private var downButton:Button;      
         
        // The inner canvas 
        private var innerCanvas:Canvas;
        
        // Properties
        //
		private var _buttonWidth:Number;
        private var _buttonHeight:Number;
		
		private var _buttonGap:Number = 2;
		private var scrollSpeed:Number = 0.4; // duration in seconds
	   	private var scrollJump:Number = 10;  // size in pixels
		
		private var _childrenCreated:Boolean = false;
		private var _startScrollingEvent:String = MouseEvent.MOUSE_DOWN;
        private var _direction:String;
     
    
        public function ButtonScrollLite()
        {
            super();
            //always on no matter what
            horizontalScrollPolicy= ScrollPolicy.ON;
            verticalScrollPolicy  = ScrollPolicy.ON;
        
         	_buttonHeight= 0;
         	_buttonWidth = 0;
        }
        
        		
    //========================
    // Public properties
    //
    	public function get scrollSize():Number   { return scrollJump;  }
    	public function set scrollSize(val:Number):void { scrollJump= val; }
    	
  
        public function get scrollSpeedMove():Number  { return scrollSpeed; }      
        public function set scrollSpeedMove(value:Number):void  {  scrollSpeed= value; }
               
	    // buttonWidth and buttonHeight
	    //  
        public function get buttonWidth():Number  {  return _buttonWidth; }
        public function set buttonWidth(value:Number):void  { _buttonWidth =value; }
            
        public function get buttonHeight():Number  {  return _buttonHeight; }
        public function set buttonHeight(value:Number):void  { _buttonHeight =value; }
  
        public function get buttonGap():Number  {  return _buttonGap; }
        public function set buttonGap(value:Number):void  { _buttonGap =value; }
  
       
   		//==========================================
   		//  createChildren
   		//
        override protected function createChildren():void
        {
            super.createChildren();
            
			upButton = new Button();
			downButton = new Button();
			
			//Alert.show("set styleName " + getStyle("upButtonStyleName"), "createChildren");
			
			upButton.styleName  = getStyle("upButtonStyleName");
			downButton.styleName= getStyle("downButtonStyleName");
			
			// this is the main canvas component, we tell it to
			// never show the scrollbars since we're controlling them
			// on our own
			innerCanvas = new Canvas();
			innerCanvas.document = this.document;
			innerCanvas.horizontalScrollPolicy= ScrollPolicy.OFF;
			innerCanvas.verticalScrollPolicy  = ScrollPolicy.OFF;
			innerCanvas.clipContent = true;
			
            // Since the createChild method can get called after children have 
	        // already been added to the Canvas, we have to swap any children
	        // that have already been added into the innerCanvas	
			while(this.numChildren > 0) {
				innerCanvas.addChild(this.removeChildAt(0));
			}
			
			
			// Add the innerCanvas and all the buttons to rawChildren
			rawChildren.addChild(innerCanvas);
			rawChildren.addChild(upButton);
			rawChildren.addChild(downButton);
		
			_childrenCreated = true;
				
			addListeners(MouseEvent.MOUSE_DOWN);
			 
            upButton.visible  = true;
            downButton.visible= true;
        }
        
        // Initialize the component
        // we need to hide and exclude the scrollbars
        private var firstTime:Boolean=true;
        override public function validateDisplayList():void{
            super.validateDisplayList();
            //we really dont need to be running this everytime we invalidate so we do it only once
            //we hide all the scrolls and the boxes, reason we cant exclude them is we need information
            //about the positions and what not

			if(firstTime){
                horizontalScrollBar.visible=false;
                horizontalScrollBar.includeInLayout=false;
				
                verticalScrollBar.visible=false;
                verticalScrollBar.includeInLayout=false;
                
				whiteBox.visible=false;
                firstTime=false;
            }                  
        } 
        
        	
		/**
		 * If we have already created the innerCanvas element, then we add the child to
		 * that. If not, that means we haven't called createChildren yet. So what we do
		 * is add the child to this main Canvas, and once we call createChildren we'll
		 * remove all the children and switch them over to innerCanvas.
		 */
		override public function addChild(child:DisplayObject):DisplayObject {
			if(_childrenCreated) {
				return innerCanvas.addChild(child);
			}
			else {
				return super.addChild(child);
			}
		}
	
		override public function get maxHorizontalScrollPosition():Number {
			return innerCanvas.maxHorizontalScrollPosition;
		}
		
		override public function get maxVerticalScrollPosition():Number {
			return innerCanvas.maxVerticalScrollPosition;
		}
        
    
	    //==========================
	    // updateDisplayList
	    //
        override protected function updateDisplayList(unscaledWidth:Number, unscaledHeight:Number):void
        {
            super.updateDisplayList(unscaledWidth,unscaledHeight);
            
            var btnHeight:Number = _buttonHeight > 0? _buttonHeight : DEFAULT_BUTTON_HEIGHT;
            var btnWidth:Number = _buttonWidth > 0? _buttonWidth : DEFAULT_BUTTON_WIDTH;
            var btnSpace:Number = btnHeight + _buttonGap;
            
            innerCanvas.setActualSize(unscaledWidth, unscaledHeight - btnSpace*2);
            innerCanvas.move(0, btnSpace);
            
			positionButtons(unscaledWidth, unscaledHeight);
            
            callLater(enableOrDisableButtons);
        }
        
        // Measure is needed in case no explicit width and height are specified
        //
        override protected function measure():void
        {
        	super.measure();
        	
			var btnHeight:Number = _buttonHeight > 0? _buttonHeight : DEFAULT_BUTTON_HEIGHT;
			var btnWidth:Number = _buttonWidth > 0? _buttonWidth : DEFAULT_BUTTON_WIDTH;
            var btnSpaceX:Number = 0;
           	var btnSpaceY:Number = btnHeight + _buttonGap;
           	
        	measuredMinWidth = innerCanvas.measuredMinWidth + btnSpaceX*2;
        	measuredWidth = innerCanvas.measuredWidth + btnSpaceX*2;
        	measuredMinHeight = innerCanvas.measuredMinHeight + btnSpaceY*2;
        	measuredHeight = innerCanvas.measuredHeight + btnSpaceY*2;
        }
              
        //
        // The buttons are assumed to be skinned, and so have size, so there
        // is no resizing here
        //
        private function positionButtons(unscaledWidth:Number, unscaledHeight:Number):void 
        {
        	
        	var btnWidth:Number = _buttonWidth > 0? _buttonWidth: DEFAULT_BUTTON_WIDTH;
            var centerX:Number = (unscaledWidth - btnWidth) /2;
            
            var btnHeight:Number = _buttonHeight > 0? _buttonHeight : DEFAULT_BUTTON_HEIGHT;
            var centerY:Number = (unscaledHeight- btnHeight) /2;
                   	
			upButton.move(centerX, 0);
			downButton.move(centerX, unscaledHeight - btnHeight);
			
			upButton.setActualSize(btnWidth, btnHeight);
			downButton.setActualSize(btnWidth, btnHeight);
		}
        
    
       	// Add event listeners for buttons
       	// 
         private function addListeners(eventParam:String):void
         {
             downButton.addEventListener(eventParam, scrollDown);
             upButton.addEventListener(eventParam, scrollUp);
         }
         
       	    
	//==============================================
    //
    // Button Handlers
    //  	
		private function scrollUp(event:Event):void 
		{
	    	var dest:Number = innerCanvas.verticalScrollPosition - scrollJump;
      		TweenLite.to(innerCanvas, scrollSpeed, {verticalScrollPosition:dest, ease:Quad.easeOut, 
      										onComplete:enableOrDisableButtons });	
	    }
	    
	    private function scrollDown(event:Event):void 
	    {
			var dest:Number = innerCanvas.verticalScrollPosition + scrollJump;
      		TweenLite.to(innerCanvas, scrollSpeed, {verticalScrollPosition:dest, ease:Quad.easeOut, 
      										onComplete:enableOrDisableButtons });
		}
  
  
   		/**
	     * We check to see if the buttons should be enabled. If we can't scroll in
	     * one direction then we disable that particular button.
	     */ 
	    protected function enableOrDisableButtons():void 
	    {
    		downButton.enabled= innerCanvas.verticalScrollPosition < innerCanvas.maxVerticalScrollPosition;
		 	upButton.enabled  = innerCanvas.verticalScrollPosition > 0;
	    }
	    
        
        /**
         * Calls our Styles
         */
        private static var classConstructed:Boolean = initializeStyles();
        /**
         * The Styles that we defined that can be used for the button
         */
        private static function initializeStyles():Boolean{
           
            //we need to obtain and see if there is a CSS style Decleration
            var buttonScrollCanvasCSS:CSSStyleDeclaration = FlexGlobals.topLevelApplication.styleManager.getStyleDeclaration(CLASS_NAME);
			
            if(!buttonScrollCanvasCSS){
                //so now we need to create our own CSS
                buttonScrollCanvasCSS = new CSSStyleDeclaration();
                buttonScrollCanvasCSS.defaultFactory = function():void{
                    //what happens now is we assign the styles our own random name, that will be used as a StyleDecleration
                    this.upButtonStyleName="upButton";
                    this.downButtonStyleName="downButton";
				}
					
                //we need to install it and create our own
				FlexGlobals.topLevelApplication.styleManager.setStyleDeclaration(CLASS_NAME, buttonScrollCanvasCSS,false);
            }

            return true;
        }
    }
}
