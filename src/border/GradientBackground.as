package border
{
	import adem.view.Constants;
	
	import flash.geom.Matrix;
	
	import mx.skins.ProgrammaticSkin;
	
	public class GradientBackground extends ProgrammaticSkin
	{
        private var topCornerRadius:Number;      // top corner radius
        private var bottomCornerRadius:Number;   // bottom corner radius
        private var fillColors:Array;            // fill colors (two)
        private var fillAlphas:Array;            // fill alphas (two)
		private var gradientType:String; 
        private var setup:Boolean;
        private var angle:Number; 
        private var focalPointRatio:Number;
        
		override public function get measuredWidth():Number
		{
			return 20;
		}
		
		override public function get measuredHeight():Number
		{
			return 20;
		}
		
        // ------------------------------------------------------------------------------------- //
        
        private function setupStyles():void
        {
            fillColors = getStyle("fillColors") as Array;
            if (!fillColors) fillColors = [0xFFFFFF, 0xFFFFFF];
            
            topCornerRadius = getStyle("cornerRadius") as Number;
            if (!topCornerRadius) topCornerRadius = 0;    
            
            bottomCornerRadius = getStyle("bottomCornerRadius") as Number;
            if (!bottomCornerRadius) bottomCornerRadius = topCornerRadius;    

			fillAlphas= getStyle("fillAlphas") as Array;
			if (fillAlphas == null) fillAlphas = [1, 1];
			
            gradientType = getStyle("gradientType");
			if (gradientType == "" || gradientType == null) gradientType = "linear";
			
			angle = getStyle("angle");
			if (isNaN(angle)) angle = 90;
			
			focalPointRatio	= getStyle("focalPointRatio");
			if (isNaN(focalPointRatio)) focalPointRatio = 0.5;
        }
        
        // ------------------------------------------------------------------------------------- //
        
		override protected function updateDisplayList(unscaledWidth:Number, unscaledHeight:Number):void
		{
            super.updateDisplayList(unscaledWidth, unscaledHeight);    
            
            setupStyles();
            
			// define color and height of thin lines on bottom of canvas
			var heightLine1:Number 		= .4;
			var heightLine2:Number		= .2;
			var tabBarHeight:Number		= 8;
			var colorLine1:uint			= 0x4acde5;
			var colorLine2:uint			= 0x93d9e6;
			var paddingTabBar:uint		= 21/6;
			var colorLine1Left:uint		= 0xc2d1d5;
			var colorLine2Left:uint		= 0xd3dbdd;
			var totalHeight:uint		= unscaledHeight - (Constants.margin/7);

			var totalGradientHeight:uint= totalHeight - heightLine1 - heightLine2 - tabBarHeight ;

/*
            var g:Graphics = graphics;
            var b:EdgeMetrics = borderMetrics;
            var w:Number = unscaledWidth - b.left - b.right;
            var h:Number = unscaledHeight - b.top - b.bottom;
            var m:Matrix = verticalGradientMatrix(0, 0, w, h);
        
            g.beginGradientFill("linear", fillColors, [1, 1], [0, 255], m);
            
            var tr:Number = Math.max(topCornerRadius-2, 0);
            var br:Number = Math.max(bottomCornerRadius-2, 0);
            
            GraphicsUtil.drawRoundRectComplex(g, b.left, b.top, w, h, tr, tr, br, br);
            g.endFill();
*/

			var matrix:Matrix = new Matrix();
			matrix.createGradientBox(unscaledWidth, totalGradientHeight, angle * Math.PI / 180);
			
			
			graphics.beginGradientFill(gradientType, fillColors, fillAlphas, [0, 255] , matrix, "pad", "rgb", focalPointRatio);
			graphics.drawRect(0, 0, unscaledWidth, totalGradientHeight);
			graphics.endFill();
			
			graphics.beginFill(0x40b4ce,1);
			graphics.drawRect(0, totalGradientHeight, unscaledWidth,  tabBarHeight);
			graphics.endFill();
			
			// left of first tabbar tabbar
			graphics.beginFill(colorLine1Left,1);
			graphics.drawRect(0, totalGradientHeight + tabBarHeight, paddingTabBar, heightLine1);
			graphics.endFill();
			
			// right of leftcorner of first tabbar
			graphics.beginFill(colorLine1,1);
			graphics.drawRect(paddingTabBar, totalGradientHeight + tabBarHeight, unscaledWidth, heightLine1);
			graphics.endFill();
			
			graphics.beginFill(colorLine2Left,1);
			graphics.drawRect(0, totalGradientHeight + heightLine1 + tabBarHeight, paddingTabBar, heightLine2);
			graphics.endFill();
			
			graphics.beginFill(colorLine2,1);
			graphics.drawRect(paddingTabBar, totalGradientHeight + heightLine1 + tabBarHeight, unscaledWidth, heightLine2);
			graphics.endFill();
			
			
		}
	}
}