package border
{
	import flash.display.Graphics;
	
	public class ComplexGradientBorder extends SimpleGradientBorder
	{
        override protected function updateDisplayList(unscaledWidth:Number, unscaledHeight:Number):void
        {
            super.updateDisplayList(unscaledWidth, unscaledHeight);    
            
            var g: Graphics = graphics;
            g.beginFill(0xc1d1d5);
            g.drawRect(0, height - 5, width / 2, -2);
            g.beginFill(0x64d5ea);
            g.drawRect(width / 2, height - 5, width / 2, -2);
            g.endFill();
/*                
			// left of first tabbar tabbar
			graphics.beginFill(colorLine1Left,1);
			graphics.drawRect(0, totalGradientHeight + tabBarHeight, paddingTabBar, heightLine1);
			graphics.endFill();
			
			// right of leftcorner of first tabbar
			graphics.beginFill(colorLine1,1);
			graphics.drawRect(paddingTabBar, totalGradientHeight + tabBarHeight, unscaledWidth, heightLine1);
			graphics.endFill();
			
			graphics.beginFill(colorLine2Left,1);
			graphics.drawRect(0, totalGradientHeight + heightLine1 + tabBarHeight, paddingTabBar, heightLine2);
			graphics.endFill();
			
			graphics.beginFill(colorLine2,1);
			graphics.drawRect(paddingTabBar, totalGradientHeight + heightLine1 + tabBarHeight, unscaledWidth, heightLine2);
			graphics.endFill();
 */       }
	}
}