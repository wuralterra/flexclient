package components
{
	import spark.components.Image;

	import spark.core.IDisplayText;
	
	public class LabelImage extends Image
	{
		[SkinPart(required="false")] 
		public var labelDisplay:IDisplayText;
		
		private var _text:String = "TestText"; 
		private var bTextChanged:Boolean = false; 
		
		public function LabelImage()
		{
			super();
		}
		
		// Define the getter method.  
		public function get text():String { 
			return _text; 
		} 
		
		//Define the setter method to call invalidateProperties()  
		// when the property changes.  
		public function set text(t:String):void { 
			_text = t; 
			bTextChanged = true; 
			invalidateProperties(); 
		} 
		
		override protected function partAdded(partName:String, instance:Object):void { 
			super.partAdded(partName, instance); 
			
			if (instance == labelDisplay) { 
				labelDisplay.text= _text; 
			} 
		}

		override protected function partRemoved(partName:String, instance:Object):void { 
			super.partRemoved(partName, instance); 
			
			if (instance == labelDisplay) { 
			} 
		}
	}
}