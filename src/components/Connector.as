package components
{
	import flash.display.BitmapData;
	import flash.display.DisplayObject;
	import flash.display.Shape;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.geom.Matrix;
	
	import model.Constants;
	
	import mx.core.UIComponent;
	
	public class Connector extends UIComponent
	{
		private var connector: Shape = new Shape();
		
		private var _color:uint;
		
		public function Connector(aColor: uint = Constants.DEFAULT_COLOR):void {
			_color = aColor;

			this.toolTip = Constants.CONNECTOR;
			
			if (stage)
				init(null);
			else
				addEventListener(Event.ADDED_TO_STAGE, init);
		}
		
		private function init(event: Event):void {
			if (hasEventListener(Event.ADDED_TO_STAGE))
				removeEventListener(Event.ADDED_TO_STAGE, init);

			addChild(connector);
			
			drawConnector();
		}

		private function drawConnector(): void {
			connector.graphics.clear();

			// border
			connector.graphics.beginFill(0xffffff);
			connector.graphics.drawCircle(0,0, Constants.CONNECTOR_SIZE);
			connector.graphics.endFill();
			
			//connector.graphics.beginFill();
			connector.graphics.lineStyle(Constants.LINE_THICKNESS, color);
			connector.graphics.drawCircle(0,0, Constants.RADIUS);
			connector.graphics.endFill();
		}
		
		public function set color(value:uint):void {
			_color = value;

			drawConnector();
		}
		
		public function get color():uint {
			return _color;
		}
	}
}