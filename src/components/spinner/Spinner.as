/*  
 Spinner.as 
 ===========================================================================
 Luigi Open Source 
 ===========================================================================
 Copyright (c) 2009 The SEAMLESS association.

 For more information: www.seamlessassociation.org;
 email: info@seamless-if.org

 The contents of this file are subject to the SEAMLESS Association License
 for software infrastructure and model components  Version 1.1 (the "License");
 you may not use this file except in compliance with the License. You may 
 obtain a copy of the License at
 http://www.seamless-if.org/index.php?option=com_content&view=article&id=52&Itemid=80 
 
 Software distributed under the License is distributed on an "AS IS"  basis, 
 WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for 
 the specific  governing rights and limitations


 The Initial Developer(s) of the Original Code are: 
 Bas Vanmeulebrouk, Alterra Wageningen UR
 Inge la Rivi�re, Alterra Wageningen UR
 Hugo de Groot, Alterra Wageningen UR
 ===========================================================================
 Contributor(s): 
 ===========================================================================
 */
package components.spinner
{
	import flash.display.DisplayObject;
	import flash.events.TimerEvent;
	import flash.utils.Timer;
	
	import mx.core.FlexGlobals;
	import mx.core.UIComponent;
	import mx.events.ResizeEvent;
	import mx.styles.CSSStyleDeclaration;

	[Style(name="tickColor",type="uint",format="Color",inherit="no")]
	public class Spinner extends UIComponent
	{
		[Bindable]
		private var _numticks: int = 12;
		
		[Bindable]
		private var _tickwidth: int = 3;
		
		[Bindable]
		private var _tickColor: uint = 0x000000;
		
		[Bindable] 
		private var _fadespeed: int = 600;
		
		[Bindable] 
		private var _speed: int;
		
		[Bindable]
		private var _timer: Timer;
		
		[Bindable]
		private var _counter: int;
		
		// Define a static variable.
		private static var classConstructed:Boolean = classConstruct();
		
		// Define the flag to indicate that a style property changed.
		private var bStypePropChanged:Boolean = true;
		
		// Define a static method.
		private static function classConstruct():Boolean {
			if (!FlexGlobals.topLevelApplication.styleManager.getStyleDeclaration("components.spinner.Spinner"))
			{
				// If there is no CSS definition then create one and set the default value.
				var spinnerStyles:CSSStyleDeclaration = new CSSStyleDeclaration();
				spinnerStyles.defaultFactory = function():void {
					this._tickColor = 0x000000;
				}
				FlexGlobals.topLevelApplication.styleManager.setStyleDeclaration("components.spinner.Spinner", spinnerStyles, true);
				
			}
			return true;
		}
		
		// Constructor  
		public function Spinner() {
			super();
			
			width = 30;
			
			height = 30;
			
			addEventListener(ResizeEvent.RESIZE, handleResizeEventResize);
			
			speed = 1000;
		}
		
		override protected function measure():void {
			super.measure();
			
			measuredWidth = measuredMinWidth = 30;
			measuredHeight = measuredMinHeight = 30;
		}
		
		override public function styleChanged(styleProp:String):void {
			
			super.styleChanged(styleProp);
			
			if (styleProp=="tickColor") // || styleProp=="somethingElse") 
			{
				bStypePropChanged=true; 
				invalidateDisplayList();
				return;
			}
		}
		
		override protected function updateDisplayList(unscaledWidth:Number,
													  unscaledHeight:Number):void {
			super.updateDisplayList(unscaledWidth, unscaledHeight);
			
			// Check to see if style changed. 
			if (bStypePropChanged==true) {
				_tickColor=getStyle("tickColor");
				
				createTicks();
				
				bStypePropChanged=false;
			}
		}

		public function cleanUp(): void {
			if (timer != null) {
				timer.stop();
				
				timer = null;
			}
		}
		
		private function createTicks(): void {
			for (var j: int = numChildren - 1; j >= 0; j --) {
				removeChildAt(j);
			}
			//removeChildren(0, numChildren-1);
			var radius: Number = width / 2;
			
			var angle: Number = 2 * Math.PI / numTicks; // The angle between each tick
			
			var tickwidth: Number = (tickWidth != -1) ? tickWidth : width / 10;

			var currentangle: Number = 0;

			for (var i: int = 0; i < numTicks; i ++) {
				var xstart: Number = radius + Math.sin(currentangle) * ((numTicks + 2) * tickwidth / 2 / Math.PI);

				var ystart: Number = radius - Math.cos(currentangle) * ((numTicks + 2) * tickwidth / 2 / Math.PI);

				var xend: Number = radius + Math.sin(currentangle) * (radius - tickWidth);

				var yend: Number = radius - Math.cos(currentangle) * (radius - tickWidth);
					
				var tick: Tick = new Tick(xstart, ystart, xend, yend, tickwidth, _tickColor);
				
				tick.alpha = 1 - (1 / numTicks * i);
				
				addChild(tick);
					
				currentangle += angle;
			}
		}
		
		private function handleResizeEventResize(event: ResizeEvent): void {
			createTicks();
		}
		
		[Bindable]
		public function get numTicks(): int {
			return _numticks;
		}
		
		public function set numTicks(aNumTicks: int): void {
			_numticks = aNumTicks;
			
			createTicks();
		}
		
		[Bindable]
		public function get tickWidth(): int {
			return _tickwidth;
		}
		
		public function set tickWidth(aTickWidth: int): void {
			_tickwidth = aTickWidth;
			
			createTicks();
		}
		
		[Bindable]
		public function get fadeSpeed(): int {
			return _fadespeed;
		}
		
		public function set fadeSpeed(aFadeSpeed: int): void {
			_fadespeed = aFadeSpeed;
			
			createTicks();
		}
		
		[Bindable]
		public function get speed(): int {
			return _speed;
		} 
		
		public function set speed(aSpeed: int): void {
			if (aSpeed != _speed) {
				_speed = aSpeed;
				
				if (timer != null) {
					timer.stop();
					
					timer.delay = aSpeed / numTicks;
					
					timer.start();
				} else {
					timer = new Timer(aSpeed / numTicks);

					timer.start();
				}
			}
		}
		
		[Bindable]
		private function get timer(): Timer {
			return _timer;
		}
		
		private function set timer(aTimer: Timer): void {
			if (_timer != null) {
				_timer.removeEventListener(TimerEvent.TIMER, handleTimerEventTimer);
			}
			
			_timer = aTimer;

			if (_timer != null) {
				_timer.addEventListener(TimerEvent.TIMER, handleTimerEventTimer, false, 0, true);
			}
		}
		
		private function handleTimerEventTimer(event: TimerEvent): void {
			if (numChildren == numTicks) {
				var child: DisplayObject = getChildAt(counter);
				
				if (child is Tick) {
					var tick: Tick = child as Tick;
					
					tick.play(fadeSpeed != 1 ? fadeSpeed : speed * 6 / 10);
				}
				
				counter += 1;
			}
		}
		
		[Bindable]
		private function get counter(): int {
			return _counter;
		}
		
		private function set counter(aInt: int): void {
			if (aInt < numTicks) {
				_counter = aInt;
			} else {
				_counter = 0;
			}
		}
	}
}