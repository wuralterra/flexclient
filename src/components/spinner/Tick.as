/*  
 Tick.as 
 ===========================================================================
 Luigi Open Source 
 ===========================================================================
 Copyright (c) 2009 The SEAMLESS association.

 For more information: www.seamlessassociation.org;
 email: info@seamless-if.org

 The contents of this file are subject to the SEAMLESS Association License
 for software infrastructure and model components  Version 1.1 (the "License");
 you may not use this file except in compliance with the License. You may 
 obtain a copy of the License at
 http://www.seamless-if.org/index.php?option=com_content&view=article&id=52&Itemid=80 
 
 Software distributed under the License is distributed on an "AS IS"  basis, 
 WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for 
 the specific  governing rights and limitations


 The Initial Developer(s) of the Original Code are: 
 Bas Vanmeulebrouk, Alterra Wageningen UR
 Inge la Rivi�re, Alterra Wageningen UR
 Hugo de Groot, Alterra Wageningen UR
 ===========================================================================
 Contributor(s): 
 ===========================================================================
 */
package components.spinner
{
	import mx.containers.Canvas;
	import mx.effects.Fade;

	public class Tick extends Canvas
	{
		[Bindable]
		private var _fade: Fade;
		
		public function Tick(aFromX: Number, aFromY: Number, aToX: Number, aToY: Number, aTickWidth: int, aTickColor: uint) {
			super();
			
			percentWidth = 100;
			
			percentHeight = 100;

			graphics.lineStyle(aTickWidth, aTickColor, 1.0, false, "normal", "rounded");

			graphics.moveTo(aFromX, aFromY);

			graphics.lineTo(aToX, aToY);
			
			createFade();
		}
		
		private function createFade(): void {
			fade = new Fade(this);
			
		}
		
		public function play(aDuration: Number): void {
			fade.alphaFrom = 1.0;
			
			fade.alphaTo = 0.0;
			
			fade.duration = aDuration;
			
			fade.play();
		}
		
		[Bindable]
		private function get fade(): Fade {
			return _fade;
		}
		
		private function set fade(aFade: Fade): void {
			_fade = aFade;
		}
	}
}