package components.mapcontrol
{
	import flash.display.BitmapData;
	import flash.events.MouseEvent;
	
	import mx.controls.Alert;
	
	import org.luigios.main.control.mapcontrol.MapControl;
	import org.luigios.main.control.mapcontrol.mapcontroltool.MapControlTool;
	import org.luigios.main.geometry.Point;
	import org.luigios.main.layer.Layer;
	import org.luigios.main.layer.LayerEvent;
	import org.luigios.main.layercanvas.LayerCanvas;
	
	public class ClickMapControlTool extends MapControlTool
	{
		protected var pointLayer: PointLayer;
		
		public function ClickMapControlTool() {
			super();
		}
		
		override public function click(event: MouseEvent): void {
			// make sure it is on top ;-)
			mapControl.map.moveLayerTo(pointLayer, mapControl.map.numLayers()-1);
			
			if (checkAllowablePoint(event.localX, event.localY))
				pointLayer.point= getRealWorldCoordinate(event);
		}
		
		protected function checkAllowablePoint(aX:Number, aY:Number): Boolean {
			var result: Boolean = false;
			
			var layercanvas: LayerCanvas;
			var color:uint;
			
			for (var i: int = 0; i < mapControl.map.numLayers(); i ++) {
				var layer: Layer = mapControl.map.getLayer(i);
				
				if (layer.visible && layer != pointLayer) {
					layercanvas = mapControl.findLayerCanvas(layer);
					
					if (layercanvas != null) {
						color = getColor(layercanvas, aX, aY);
						
						result = (color != 0xffffff && color != 0);
						if (result)
							break;
					}
				}
			}
			
			return result;
		}
		
		private function getColor(aLayerCanvas: LayerCanvas, aX: int, aY: int): uint {
			var result: uint = 0xffffff;
			
			if (aLayerCanvas != null) {
				var bitmapdata: BitmapData = aLayerCanvas.createBitmapData();
				
				if (bitmapdata != null) {
					result = bitmapdata.getPixel(aX, aY);
				}
			}
			
			return result;
		}

		override public function cleanUp(): void {
			pointLayer.currentPoint = null;
			pointLayer.point = null;
			pointLayer.pointList.removeAll();
			pointLayer.pointList = null;
		}
		
		public function setObject(aObject: Object): void {
			mapControl.map.moveLayerTo(pointLayer, mapControl.map.numLayers()-1);

			pointLayer.object = aObject;
		}
		
		public function setSymbolColor(aColor: String): void {
			pointLayer.symbolColor = aColor;
		}
		
		public function setDragSymbolColor(aColor: String): void {
			pointLayer.dragSymbolColor = aColor;
		}
		
		public function setSelectedSymbolColor(aColor: String): void {
			pointLayer.selectedSymbolColor = aColor;
		}
		
		override public function set mapControl(aMapControl: MapControl): void {
			super.mapControl = aMapControl;
			
			pointLayer = new PointLayer('Volante');
			
			mapControl.map.addLayer(pointLayer);
			
			mapControl.map.layer.addEventListener(LayerEvent.ADD, handleLayerEventLayerAdd);
			
			pointLayer.visible = true;
		}

		private function handleLayerEventLayerAdd(event: LayerEvent): void {
			mapControl.map.moveLayerTo(pointLayer, mapControl.map.numLayers()-1);
		}
	}
}