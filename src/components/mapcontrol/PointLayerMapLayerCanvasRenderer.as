package components.mapcontrol
{
	import flash.display.Graphics;
	import flash.utils.getQualifiedClassName;
	
	import mx.containers.Canvas;
	import mx.controls.Image;
	import mx.core.UIComponent;
	
	import org.luigios.main.geometry.Point;
	import org.luigios.main.layer.featurelayer.styling.Fill;
	import org.luigios.main.layer.featurelayer.styling.Stroke;
	import org.luigios.main.layer.featurelayer.styling.Symbolizer;
	import org.luigios.main.layercanvas.LayerCanvas;
	import org.luigios.main.layercanvas.maplayercanvas.MapLayerCanvas;
	import org.luigios.main.map.mapcontrolsettings.MapControlSettings;
	import org.luigios.main.renderer.Renderer;
	
	public class PointLayerMapLayerCanvasRenderer extends Renderer
	{
		override public function render(aLayerCanvas: LayerCanvas, aMapControlSettings: MapControlSettings): UIComponent {
			dispatchStartEvent();
			
			var canvas: Canvas = new Canvas();
			
			var image: Image = createImage(aMapControlSettings);
			
			var graphics: Graphics = image.graphics;

			if (aLayerCanvas.layer is PointLayer) {
				var pointlayer: PointLayer = aLayerCanvas.layer as PointLayer;

				var symbolizer: Symbolizer;
				
				/*
				if (pointlayer.numPoints() > 0) {
					symbolizer = new Symbolizer(new Fill(pointlayer.symbolColor, 0.5), 
												new Stroke(pointlayer.symbolColor, 1, pointlayer.strokeWidth), 
												pointlayer.symbolSize * 0.8);

					// other points
					for (var i: int = 0; i < pointlayer.numPoints(); i ++) {
						var point:Point = pointlayer.getPoint(i);
						
						image.addChild(createPointImage(point, aMapControlSettings, symbolizer));
					} 
				}
				*/ 

				// active/selected point
				if (pointlayer.point != null) {
					symbolizer = new Symbolizer(new Fill(pointlayer.dragStarted?pointlayer.dragSymbolColor:'#ffffff', 1), 
												new Stroke(pointlayer.selectedSymbolColor, 1, pointlayer.strokeWidth*2), 
												pointlayer.symbolSize*2);
					
					image.addChild(createPointImage(pointlayer.point, aMapControlSettings, symbolizer)); 
				}
				
				// captured point
				if (pointlayer.currentPoint != null) {
					symbolizer = new Symbolizer(new Fill('#ffffff', 0.5), 
												new Stroke(pointlayer.dragSymbolColor, 1, pointlayer.strokeWidth/2), 
												pointlayer.symbolSize/2);
					
					image.addChild(createPointImage(pointlayer.currentPoint, aMapControlSettings, symbolizer)); 
				}
			}
			
			canvas.addChild(image);
			
			if (pointlayer.numPoints() > 0) {
				for (var p: int = 0; p < pointlayer.numPoints(); p ++) {
					var cspoint:CrowdSourcingPoint = (pointlayer.getPoint(p) as CrowdSourcingPoint);
					
					var pc:PointComponent = new PointComponent(cspoint.id, uint(pointlayer.symbolColor.replace("#", "0x")), pointlayer.symbolSize, pointlayer.strokeWidth);
					
					var pixelpoint: Point = aMapControlSettings.mapCalculator.realWorldToPixel(cspoint);
					
					pc.x = pixelpoint.x;
					pc.y = pixelpoint.y;
					
					// ui wrapper
					var uicomponent:UIComponent = new UIComponent();
					uicomponent.addChild(pc);
					
					canvas.addElement(uicomponent);
				}
			} 
			
			dispatchCompleteEvent(null);
			
			return canvas;
		}
			
		protected function createPointImage(aPoint: Point, aMapControlSettings: MapControlSettings, aSymbolizer: Symbolizer): Image {
			var image: Image = createImage(aMapControlSettings);
			
			var pixelpoint: Point = aMapControlSettings.mapCalculator.realWorldToPixel(aPoint);
			
			// to be save, otherwise a line might be drawn in certain flex versions
			image.graphics.moveTo(pixelpoint.x, pixelpoint.y);
			
			image.graphics.lineStyle(aSymbolizer.stroke.width, aSymbolizer.stroke.color);
			
			image.graphics.beginFill(aSymbolizer.fill.color, aSymbolizer.fill.opacity);
			
			image.graphics.drawCircle(pixelpoint.x, pixelpoint.y, aSymbolizer.size);

			return image;
		}
		
		protected function createImage(aMapControlSettings: MapControlSettings): Image {
			var image: Image = new Image();
			
			image.x = 0;
			
			image.y = 0;
			
			image.width = aMapControlSettings.width;
			
			image.height = aMapControlSettings.height;
			
			return image;			
		}
		
		override public function renders(aLayerCanvas: LayerCanvas): Boolean {
			return aLayerCanvas is MapLayerCanvas && aLayerCanvas.layer is PointLayer;
		}
	}
}