package components.mapcontrol
{
	import org.luigios.ogc.wmc.WMC;
	import org.luigios.ogc.wmc.WMC_1_0_0_Parser;
	
	public class OrderedWMC extends WMC
	{
		public function OrderedWMC(aURL:String)
		{
			super(aURL);
		}
		
		override protected function createWMCParser():WMC_1_0_0_Parser {
			return new OrderedWMC_1_0_0_Parser();
		}
	}
}