package components.mapcontrol
{
	import flash.display.BitmapData;
	import flash.events.MouseEvent;
	
	import mx.controls.Alert;
	
	import org.luigios.main.control.mapcontrol.MapControl;
	import org.luigios.main.geometry.Point;
	import org.luigios.main.layer.Layer;
	import org.luigios.main.layer.LayerEvent;
	import org.luigios.main.layercanvas.LayerCanvas;
	
	public class ClickDragMapControlTool extends ClickMapControlTool
	{
		//private var pointLayer: PointLayer;
		
		public function ClickDragMapControlTool() {
			super();
		}
		
		override public function mouseDown(event: MouseEvent): void {
			super.mouseDown(event);

			// =========================================
			// 'capture' point
			// =========================================
			if (pointLayer.currentPoint != null && pointLayer.point != null && pointLayer.currentPoint.distance(pointLayer.point) < pointLayer.snapDistance) {
				pointLayer.dragStarted = true;
			}
		}
		
		override public function mouseMove(event: MouseEvent): void {
			if (checkAllowablePoint(event.localX, event.localY)) {
				var point: Point = getRealWorldCoordinate(event);
			
				pointLayer.currentPoint = point;
			}
		}
		
		override public function mouseUp(event: MouseEvent): void {
			// =========================================
			// release point
			// =========================================
			pointLayer.dragStarted = false;
		}
*/		
		override public function click(event: MouseEvent): void {
			// make sure it is on top ;-)
			mapControl.map.moveLayerTo(pointLayer, mapControl.map.numLayers()-1);
			
			if (pointLayer.point == null) { 
				if (checkAllowablePoint(event.localX, event.localY))
					pointLayer.point= getRealWorldCoordinate(event);
			}

			pointLayer.dragStarted = false;
		}
		
		override public function cleanUp(): void {
			super.cleanUp();
			
			pointLayer.snapDistance = 0.5;

			pointLayer.dragStarted = false;
		}
		
		
		private function handleLayerEventLayerAdd(event: LayerEvent): void {
			mapControl.map.moveLayerTo(pointLayer, mapControl.map.numLayers()-1);
		}
	}
}