package components.mapcontrol
{
	import flash.display.BitmapData;
	import flash.display.DisplayObject;
	import flash.display.Shape;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.geom.Matrix;
	
	import model.Constants;
	
	import mx.core.UIComponent;
	
	public class PointComponent extends UIComponent
	{
		private var point: Shape = new Shape();
		
		private var _color:uint;
		
		private var _size:uint;
		
		private var _width:uint;
		
		private var _id:String;
		
		public function PointComponent(aID:String, aColor:uint=0xcccccc, aSize:uint=6, aWidth:uint=2):void {
			_color= aColor;
			_size = aSize;
			_width= aWidth;

			_id = aID;

			this.toolTip = aID;
			
			if (stage)
				init(null);
			else
				addEventListener(Event.ADDED_TO_STAGE, init);
		}
		
		private function init(event: Event):void {
			if (hasEventListener(Event.ADDED_TO_STAGE))
				removeEventListener(Event.ADDED_TO_STAGE, init);
			
			addChild(point);
			
			drawPoint();
		}
		
		private function drawPoint(): void {
			point.graphics.clear();
			
			// background/white border
			point.graphics.beginFill(0xffffff);
			point.graphics.drawCircle(0, 0, _size+1);
			point.graphics.endFill();
			
			//connector.graphics.beginFill();
			point.graphics.lineStyle(_width, color);
			point.graphics.drawCircle(0, 0, _size-_width/2);
			point.graphics.endFill();
		}
		
		public function set color(value:uint):void {
			_color = value;
			
			drawPoint();
		}
		
		public function get color():uint {
			return _color;
		}
	}
}