package components.mapcontrol
{
	import org.luigios.main.layer.Layer;
	import org.luigios.ogc.ows.OWSEvent;
	import org.luigios.ogc.wmc.ExtendedWMS;
	import org.luigios.ogc.wmc.LayerName;
	import org.luigios.ogc.wmc.WMC_1_0_0_Parser;
	
	public class OrderedWMC_1_0_0_Parser extends WMC_1_0_0_Parser
	{
		public function OrderedWMC_1_0_0_Parser()
		{
			super();
		}
		
		override protected function handleExtendedWMSCapabilitiesLoaded(event: OWSEvent): void {
			if (event.ows is ExtendedWMS) {
				var extendedwms: ExtendedWMS = event.ows as ExtendedWMS;
				
				if (extendedwms.layer != null) {
					for (var i: int = 0; i < extendedwms.numLayerNames(); i ++) {
						var layername: LayerName = extendedwms.getLayerName(i);

						var l: Layer = extendedwms.layer.findLayerByName(layername.layerName);
						
						if (l != null) {
							trace(i, layername.layerTitle, layername.layerName, layer.numLayers());
							
							l.layerTitle = layername.layerTitle;
							
							l.visible = ! layername.hidden;
							
							layer.addLayer(l);
						}
					}
				}
			}
		}
	}
}