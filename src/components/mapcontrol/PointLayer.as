package components.mapcontrol
{
	import mx.collections.ArrayCollection;
	import mx.utils.UIDUtil;
	
	import org.luigios.main.feature.FeatureCollection;
	import org.luigios.main.feature.FeatureCollectionEvent;
	import org.luigios.main.geometry.Point;
	import org.luigios.main.layer.Layer;
	import org.luigios.main.layer.featurelayer.styling.FeatureTypeStyle;

	/**
	 * A FeatureLayer consists of a FeatureCollection  
	 * @author vanme002
	 * 
	 */
	public class PointLayer extends Layer
	{
		[Bindable]
		private var _pointlist: ArrayCollection;
		
		[Bindable]
		private var _point: Point;
		
		private var _object: Object;
		
		[Bindable]
		private var _currentpoint: Point;
		
		[Bindable]
		public var srsCode: String;
		
		[Bindable]
		private var _dragstarted: Boolean = false;
		
		[Bindable]
		public var snapDistance: Number;

		public var symbolColor: String = "#ff0000";

		public var selectedSymbolColor: String = "#0000ff";
		
		public var dragSymbolColor: String = "#ff0000";
		
		public var symbolSize: int = 5;
		
		public var strokeWidth: int = 2;
		
		public function PointLayer(aLayerName: String) {
			super(aLayerName);
			
			pointList = new ArrayCollection();
		}
		public function addPoint(aPoint: Point): void {
			pointList.addItem(aPoint);
			
			contentsChanged();
		}
		
		public function removeAllPoints(): void {
			pointList.removeAll();
			
			contentsChanged();
		}
		
		public function getPoint(aIndex: int): Point {
			return pointList.getItemAt(aIndex) as Point;
		}
		
		public function numPoints(): int {
			return pointList.length;
		}
		
		[Bindable]
		public function get pointList(): ArrayCollection {
			return _pointlist;
		}
		
		public function set pointList(aArrayCollection: ArrayCollection): void {
			_pointlist = aArrayCollection;
			
			contentsChanged();
		}
		
		[Bindable]
		public function get object(): Object {
			return _object;
		}
		
		public function set object(aObject: Object): void {
			_object = aObject;
			
			if (aObject != null && aObject['lon'] != "" && aObject['lat'] != "") {
				point = new Point(aObject['lon'], aObject['lat']);
				point.srsCode = srsCode;
			}
			
			contentsChanged();
		}
		
		[Bindable]
		public function get point(): Point {
			return _point;
		}
		
		public function set point(aPoint: Point): void {
			_point = aPoint;
			
			if (aPoint != null) {
				object['lon'] = aPoint.x.toPrecision(aPoint.x>=10?5:4);
				object['lat'] = aPoint.y.toPrecision(aPoint.y>=10?5:4);
				
				trace("latlon afgerond:", object['lat'], object['lon']);
			}
			
			contentsChanged();
		}
		
		[Bindable]
		public function get currentPoint(): Point {
			return _currentpoint;
		}
		
		public function set currentPoint(aPoint: Point): void {
			if (dragStarted && aPoint != null && point != null && _currentpoint != null 
				&& point.x == _currentpoint.x && point.y == _currentpoint.y) {
				trace("currentPoint EQ:", dragStarted, aPoint, point, _currentpoint, point.distance(aPoint));

				modify(point, aPoint);
				object['lon'] = aPoint.x.toPrecision(aPoint.x>=10?5:4);
				object['lat'] = aPoint.y.toPrecision(aPoint.y>=10?5:4);
				
				_currentpoint = aPoint;
			}
			else {
				if (dragStarted && aPoint != null && point != null && point.distance(aPoint) < snapDistance) {
					trace("currentPoint snap:", dragStarted, aPoint, point, point.distance(aPoint));
					
					modify(aPoint, point);
					
					_currentpoint = aPoint;
				}
				else
					_currentpoint = aPoint;
			}
			
			contentsChanged();
		}
		
		protected function modify(aFromPoint: Point, aToPoint: Point): void {
			trace("modify", aFromPoint, aToPoint);
			if (aFromPoint != null && aToPoint != null) {
				aFromPoint.x = aToPoint.x;
				
				aFromPoint.y = aToPoint.y;
				
				aFromPoint.srsCode = aToPoint.srsCode;
			}
		}
		
		[Bindable]
		public function get dragStarted(): Boolean {
			return _dragstarted;
		}
		
		public function set dragStarted(aDragStarted: Boolean): void {
			_dragstarted = aDragStarted;
			
			contentsChanged();
		}
		
		// voor multipoint gebruik
/*		public function setSnapPoint(aPoint: Point): void {
			var snappoint: Point = getSnapPoint(aPoint);
			
			if (snappoint != null) {
				trace("Snap!");
				
				_currentpoint = snappoint;
			} else {
				trace("No snap!");
				
				_currentpoint = aPoint;
			}
		}
		
		public function getSnapPoint(aPoint: Point): Point {
			var result: Point = null;
			
			for (var i: int = 0; i < numPoints(); i ++) {
				var p: Point = getPoint(i);
				
				if (p.distance(aPoint) < snapDistance) {
					result = new Point(aPoint.x, aPoint.y);
					
					result.srsCode = aPoint.srsCode;

					break;
				}
			}

			return result;
		}
*/			
	}	
}