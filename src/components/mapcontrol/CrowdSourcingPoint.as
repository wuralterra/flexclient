package components.mapcontrol
{
	import org.luigios.main.geometry.Point;
	
	public class CrowdSourcingPoint extends Point
	{
		[Bindable]
		public var id: String;
		
		public function CrowdSourcingPoint(aX:Number, aY:Number, aID:String)
		{
			super(aX, aY);
			
			id = aID;
		}
	}
}