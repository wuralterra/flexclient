package components.wizard
{
	import components.JSONBox;
	
	import flash.display.BitmapData;
	import flash.display.DisplayObject;
	import flash.display.Sprite;
	import flash.events.MouseEvent;
	import flash.geom.Matrix;
	import flash.printing.PrintJob;
	import flash.printing.PrintJobOptions;
	
	import mx.containers.Canvas;
	import mx.controls.Alert;
	import mx.core.FlexGlobals;
	import mx.events.CloseEvent;
	import mx.managers.PopUpManager;
	
	import session.Session;
	import session.SessionEvent;

	public class WizardComponent extends Canvas
	{
		public var pdfBitMapData: BitmapData= null;
		
		[Bindable]
		private var _nextenabled: Boolean;
		
		[Bindable]
		private var _previousenabled: Boolean;
		
		[Bindable]
		private var _firstenabled: Boolean;
		
		[Bindable]
		private var _lastenabled: Boolean;
		
		public function WizardComponent() {
			super();
			
			percentHeight = 100;
			
			percentWidth = 100;
			
			Session.instance().addEventListener(SessionEvent.MODECHANGED, handleSessionEventModeChanged);
			
			callLater(handleSessionEventModeChanged, [null]);
		}
		
		protected function handleSessionEventModeChanged(event: SessionEvent): void {
			nextEnabled = Session.instance().mode < Session.instance().maxMode;
			
			previousEnabled = Session.instance().mode > 0;

			lastEnabled = Session.instance().mode == Session.instance().maxMode;

			firstEnabled = Session.instance().mode == 0;
		}
		
		public function executeAction(): void {
			// Override in subclasses
		}
		
		public function handlePreviousButtonClick(event: MouseEvent): void {
			executeAction();
			
			if (Session.instance().mode > 0) {
				Session.instance().mode -= 1;
			}
		}

		public function handleNextButtonClick(event: MouseEvent): void {
			executeAction();
			
			if (Session.instance().mode < Session.instance().maxMode) {
				Session.instance().mode += 1;
			}
		}
		
		public function handleFirstButtonClick(event: MouseEvent): void {
			executeAction();
			
			Session.instance().mode = 0;
		}
		
		public function handleLastButtonClick(event: MouseEvent): void {
			executeAction();
			
			if (Session.instance().mode == Session.instance().maxMode) {
				Alert.buttonWidth = 100;
				var okl:String = Alert.okLabel;
				Alert.okLabel = "JSON";
				
				if (Session.instance().print) {
					var nol:String = Alert.noLabel;
					Alert.noLabel = "Print";
				
					Alert.show("Do you want to save your results and exit the application?\nJSON to manually copy the results in JSON format.\nYes to continue with saving the results.\nPrint to start print dialog, choose PDF driver, A3 and landscape for best results.\nCancel to cancel.", "Question", 
						Alert.YES|Alert.NO|Alert.CANCEL|Alert.OK, null, alertClickHandler, null, Alert.YES);
				
					Alert.noLabel = nol;
				}
				else 
					Alert.show("Do you want to save your results and exit the application?\nJSON to manually copy the results in JSON format.\nYes to continue with saving the results.\nCancel to cancel.", "Question", Alert.YES|Alert.OK|Alert.CANCEL, null, alertClickHandler, null, Alert.YES);

				Alert.okLabel = okl;
			}
		}
		
		protected function alertClickHandler(event: CloseEvent):void {
			if (event.detail==Alert.NO) { // Print
				
				var printJob: PrintJob = new PrintJob();
				
				if (printJob.start()) {
					var _pageWidth: int = printJob.pageWidth;
					var _pageHeight: int= printJob.pageHeight;
					
					for (var i:uint=0; i < FlexGlobals.topLevelApplication.mainViewstack.numChildren; i++) {
						var wizardcomp: WizardComponent = FlexGlobals.topLevelApplication.mainViewstack.getChildAt(i);
						
						var rate: Number = _pageWidth/_pageHeight; 
						var bitmapData:BitmapData = wizardcomp.pdfBitMapData;
						var scaleX: Number = _pageWidth  / bitmapData.width;
						var scaleY: Number = _pageHeight / bitmapData.height;
						
						var matrix: Matrix = new Matrix();
						var scale:  Number = (scaleX < scaleY)?scaleX:scaleY;
						matrix.scale(scale, scale); // equal scaling
						
						var pageImage: Sprite = new Sprite();
						
						pageImage.graphics.beginBitmapFill(bitmapData, matrix, false, true);
						pageImage.graphics.drawRect(0, 0, bitmapData.width*scale, bitmapData.height*scale);
						
						pageImage.graphics.beginFill(0xffffff);
						if (rate > 1)	// landscape
							pageImage.graphics.drawRect(bitmapData.width*scale, 0,  _pageWidth-bitmapData.width*scale, _pageHeight);
						else			// portrait
							pageImage.graphics.drawRect(0, bitmapData.height*scale, _pageWidth, _pageHeight-bitmapData.height*scale);
						pageImage.graphics.endFill();
						
						try {
							printJob.addPage(pageImage, null, new PrintJobOptions(false));
						}
						catch (error:Error) {								
							trace(error.message);
						}
					}
					
					printJob.send();						
				}
			}
			if (event.detail==Alert.YES || event.detail==Alert.OK) {
				var object: Object = Session.instance().serverAccess.createExperimentDone(Session.instance().experiment, Session.instance().profilecomponent, FlexGlobals.topLevelApplication.mainViewstack);
				
				if (event.detail==Alert.YES) {
					Session.instance().serverAccess.sendExperimentDone(object);
				}
				else {
					var jsonbox:JSONBox = PopUpManager.createPopUp(FlexGlobals.topLevelApplication as DisplayObject, JSONBox, true) as JSONBox;
					jsonbox.jsonbox.text = JSON.stringify(object, null, 1);
					jsonbox.width = FlexGlobals.topLevelApplication.width/2;
					jsonbox.height= FlexGlobals.topLevelApplication.height/2;
					jsonbox.selectAll();
					PopUpManager.centerPopUp(jsonbox);
				}
			}
		}

		[Bindable]
		public function get nextEnabled(): Boolean {
			return _nextenabled;
		}
		
		public function set nextEnabled(aNextEnabled: Boolean) : void {
			_nextenabled = aNextEnabled;
		}
		
		[Bindable]
		public function get previousEnabled(): Boolean {
			return _previousenabled;
		}
		
		public function set previousEnabled(aPreviousEnabled: Boolean) : void {
			_previousenabled = aPreviousEnabled;
		}

		[Bindable]
		public function get firstEnabled(): Boolean {
			return _firstenabled;
		}
		
		public function set firstEnabled(aFirstEnabled: Boolean) : void {
			_firstenabled = aFirstEnabled;
		}
		
		[Bindable]
		public function get lastEnabled(): Boolean {
			return _lastenabled;
		}
		
		public function set lastEnabled(aLastEnabled: Boolean) : void {
			_lastenabled = aLastEnabled;
		}
	}
}