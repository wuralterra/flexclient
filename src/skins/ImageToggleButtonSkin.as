package skins
{

	import flash.display.GradientType;
	
	import mx.core.IButton;
	import mx.core.UIComponent;
	import mx.skins.halo.ButtonSkin;
	import mx.styles.StyleManager;
	import mx.utils.ColorUtil;
	
	public class ImageToggleButtonSkin extends ButtonSkin
	{
		public function ImageToggleButtonSkin()
		{
			super();
		}
		
		//--------------------------------------------------------------------------
		//
		//  Overridden methods
		//
		//--------------------------------------------------------------------------
		
		/**
		 *  @private
		 */
		override protected function updateDisplayList(w:Number, h:Number):void
		{
			super.updateDisplayList(w, h);
	
			// User-defined styles.
			var cornerRadius:Number = getStyle("cornerRadius");
			var fillAlphas:Array = getStyle("fillAlphas");
			var fillColors:Array = getStyle("fillColors");
	        styleManager.getColorNames(fillColors);
	
			var cr:Number = Math.max(0, cornerRadius);
			// it is possible to vary the radii of the individual corners
			//var radio:Object = { 'tl': cr, 'tr': cr, 'bl': cr, 'br': cr };
			var tmp:Number;
			
			graphics.clear();
													
			switch (name)
			{			
				case "downSkin":
				case "selectedDownSkin":
				case "overSkin":
				{
					// no button border/edge
													
					// button fill
					drawRoundRect(
						-2, -2, w+4, h+4, cr,
						[ fillColors[0], fillColors[1] ], 1,
						verticalGradientMatrix(0, 0, w, h));
											  				
					break;
				}
	
				case "disabledSkin":
				case "selectedDisabledSkin":
				{
					// no button border/edge
	
					// button fill
					drawRoundRect(
						-1, -1, w+2, h+2, cr,
						[ fillColors[0], fillColors[1] ], [ fillAlphas[0], fillAlphas[1] ],
						verticalGradientMatrix(0, 0, w, h));
										  
					// no top highlight
	
					break;
				}
							
				case "selectedUpSkin":
				case "selectedOverSkin":
				{
					var overFillColors:Array;
					if (fillColors.length > 2)
						overFillColors = [ fillColors[2], fillColors[3] ];
					else
						overFillColors = [ fillColors[0], fillColors[1] ];
	
					var overFillAlphas:Array;
					if (fillAlphas.length > 2)
						overFillAlphas = [ fillAlphas[2], fillAlphas[3] ];
	  				else
						overFillAlphas = [ fillAlphas[0], fillAlphas[1] ];
	
					// no button border/edge
	
					// button fill
					drawRoundRect(
						-2, -2, w+4, h+4, cr,
						overFillColors, overFillAlphas,
						verticalGradientMatrix(0, 0, w, h)); 
											  
					// no top highlight
					
					break;
				}
										
				case "upSkin":
				{
					var disFillAlphas:Array =
						[ Math.max( 0, fillAlphas[0] - 0.15),
						  Math.max( 0, fillAlphas[1] - 0.15) ];
	
					// no button border/edge
	
					// button fill
					drawRoundRect(
						0, 0, w, h, cr,
						[ fillColors[0], fillColors[1] ], disFillAlphas,
						verticalGradientMatrix(0, 0, w, h)); 
					
					break;
				}
			}
		}
	}
}
